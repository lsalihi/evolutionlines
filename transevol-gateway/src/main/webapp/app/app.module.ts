import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { LdgatewaySharedModule } from 'app/shared/shared.module';
import { LdgatewayCoreModule } from 'app/core/core.module';
import { LdgatewayAppRoutingModule } from './app-routing.module';
import { LdgatewayHomeModule } from './home/home.module';
import { LdgatewayEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    LdgatewaySharedModule,
    LdgatewayCoreModule,
    LdgatewayHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    LdgatewayEntityModule,
    LdgatewayAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent],
})
export class LdgatewayAppModule {}
