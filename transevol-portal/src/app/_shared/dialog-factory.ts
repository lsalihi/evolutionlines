export const openDialog = (dialog, component, data, width?: string, height?: string): void => {
  const defautWidth = '640px';
  const defaultHeight = '420px';
  const dialogRef = dialog.open(component, {
    height: height ? height : defaultHeight,
    width: width ? width : defautWidth,
    data: data
  });

  dialogRef.afterClosed().subscribe(result => {
  });
};

export const openDialogWithExecuteFonction = (dialog, component, data, execute, width?: string, height?: string): void => {
  const defautWidth = '250px';
  const defaultHeight = '250px';
  const dialogRef = dialog.open(component, {
    height: height ? height : defaultHeight,
    width: width ? width : defautWidth,
    data: data
  });

  dialogRef.afterClosed().subscribe(result => {
    if (result === true) {
      execute();
    }
  });
};
