import { AfterContentInit, ContentChild, Directive, Host, HostListener, Input } from '@angular/core';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { SortDirective } from './sort.directive';
import { ConfigService } from './config.service';

@Directive({
    selector: '[sortBy]'
})
export class SortByDirective implements AfterContentInit {
    @Input() sortBy: string;
    @ContentChild(FaIconComponent, { static: true }) iconComponent: FaIconComponent;

    sortIcon: IconDefinition;
    sortAscIcon: IconDefinition;
    sortDescIcon: IconDefinition;

    constructor(@Host() private sort: SortDirective, configService: ConfigService) {
        this.sort = sort;
        const config = configService.getConfig();
        this.sortIcon = config.sortIcon;
        this.sortAscIcon = config.sortAscIcon;
        this.sortDescIcon = config.sortDescIcon;
    }

    ngAfterContentInit(): void {
        if (this.sort.predicate && this.sort.predicate !== '_score' && this.sort.predicate === this.sortBy) {
            this.updateIconDefinition(this.iconComponent, this.sort.ascending ? this.sortAscIcon : this.sortDescIcon);
            this.sort.activeIconComponent = this.iconComponent;
        }
    }

    @HostListener('click')
    onClick() {
        if (this.sort.predicate && this.sort.predicate !== '_score') {
            this.sort.sort(this.sortBy);
            this.updateIconDefinition(this.sort.activeIconComponent, this.sortIcon);
            this.updateIconDefinition(this.iconComponent, this.sort.ascending ? this.sortAscIcon : this.sortDescIcon);
            this.sort.activeIconComponent = this.iconComponent;
        }
    }

    private updateIconDefinition(iconComponent: FaIconComponent, icon: IconDefinition) {
        if (iconComponent) {
            iconComponent.iconProp = icon;
            iconComponent.ngOnChanges({});
        }
    }
}