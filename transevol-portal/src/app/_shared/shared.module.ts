import { NgModule } from '@angular/core';
import { ItemCountComponent } from './components/item-count.component';

@NgModule({
  imports: [],
  declarations: [ItemCountComponent],
  entryComponents: [],
  exports: [ItemCountComponent]
})
export class LandlinesSharedModule {}
