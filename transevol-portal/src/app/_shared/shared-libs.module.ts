import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { LandlinesCoreModule } from './core.module';
import { DIRECTIVES, PIPES } from './util/directive-pipe';

@NgModule({
  declarations:[...DIRECTIVES, ...PIPES],
  exports: [LandlinesCoreModule,
    FormsModule, CommonModule, NgbModule, InfiniteScrollModule, FontAwesomeModule, ReactiveFormsModule]
})
export class LandlinesSharedLibsModule {}
