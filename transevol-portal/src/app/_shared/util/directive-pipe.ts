import { SortDirective } from '../directive/sort.directive';
import { SortByDirective } from '../directive/sort-by.directive';
import { OrderByPipe } from '../pipe/order-by.pipe';
import { CapitalizePipe, FilterPipe, KeysPipe, PureFilterPipe, TruncateWordsPipe, TruncateCharactersPipe } from '../pipe';


export const DIRECTIVES = [
    SortDirective,
    SortByDirective
];

export const PIPES = [
    CapitalizePipe,
    FilterPipe,
    KeysPipe,
    OrderByPipe,
    PureFilterPipe,
    TruncateWordsPipe,
    TruncateCharactersPipe
];