import  {Shorthand} from '@app/_models/_shared_models/shorthand.model';


export interface ICategorieAge {

	 id?: number;
     libelle?: string;
     description?: string;
     minAge?: number;
     maxAge?: number;
     societe?: Shorthand;
     activiteMetier?: Shorthand;
}


export class CategorieAge implements  ICategorieAge{

	constructor(
				public id?: number,
				public libelle?: string,
				public abreviation?:string,
				public description?:string,
				public minAge?:number,
				public maxAge?:number,
				public societe?: Shorthand,
				public activiteMetier?: Shorthand
		){}
}