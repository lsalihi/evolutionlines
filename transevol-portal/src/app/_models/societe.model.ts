import { Shorthand } from '@app/_models/_shared_models/shorthand.model';
import { IContact } from '@app/_models/contact.model';

export interface ISociete {
  id?: number;
  libelle?: string;
  actif?: boolean;
  //contacts?: IContact[];
  pays?: Shorthand;
}

export class Societe implements ISociete {
  constructor(public id?: number, public libelle?: string, public actif?: boolean,public pays?: Shorthand) {
    this.actif = this.actif || false;
  }
}
