export interface IDevise {
  id?: number;
  abreviationCourte?: string;
  abreviationLongue?: string;
  code?: string;
  libelle?: string;
  nombreApresVirgule?: number;
  actif?: boolean;
}

export class Devise implements IDevise {
  constructor(
    public id?: number,
    public abreviationCourte?: string,
    public abreviationLongue?: string,
    public code?: string,
    public libelle?: string,
    public nombreApresVirgule?: number,
    public actif?: boolean
  ) {
    this.actif = this.actif || false;
  }
}
