//import { ICircuit } from 'app/shared/model/circuit.model';

export interface IGare {
  id?: number;
  libelle?: string;
  correspondance?: boolean;
  gareAgence?: boolean;
  actif?: boolean;
  villeId?: number;
  agenceId?: number;
  villeLibelle?: string;
  agenceLibelle?: string;
  // circuits?: ICircuit[];
}

export class Gare implements IGare {
  constructor(
    public id?: number,
    public libelle?: string,
    public correspondance?: boolean,
    public gareAgence?: boolean,
    public actif?: boolean,
    public villeId?: number,
    public agenceId?: number,
    public villeLibelle?: string,
    public agenceLibelle?: string
  ) //public circuits?: ICircuit[]
  {
    this.correspondance = this.correspondance || false;
    this.gareAgence = this.gareAgence || false;
    this.actif = this.actif || false;
  }
}
