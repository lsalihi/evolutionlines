import { Moment } from 'moment';
import { IPasserelle } from './passerelle.model';

export interface ILigne {
  id?: number;
  libelle?: string;
  code?: string;
  dateCreation?: Moment;
  suspendu?: boolean;
  dateSuspension?: Moment;
  actif?: boolean;
  passerelles?: IPasserelle[];
  gareDepartId?: number;
  gareDestinationId?: number;
  classeId?: number;
}

export class Ligne implements ILigne {
  constructor(
    public id?: number,
    public libelle?: string,
    public code?: string,
    public dateCreation?: Moment,
    public suspendu?: boolean,
    public dateSuspension?: Moment,
    public actif?: boolean,
    public passerelles?: IPasserelle[],
    public gareDepartId?: number,
    public gareDestinationId?: number,
    public classeId?: number
  ) {
    this.suspendu = this.suspendu || false;
    this.actif = this.actif || false;
  }
}
