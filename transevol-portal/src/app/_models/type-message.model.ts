import { Shorthand } from '@app/_models/_shared_models/shorthand.model';

export interface ITypeMessage {
    id?: number;
    titre?: string;
    description?: string;
    type?: string;
    codeFonction?: string;
    criticite?: Shorthand;
}

export class TypeMessage implements ITypeMessage {
  constructor(
    public id?: number,
    public titre?: string,
    public description?: string,
    public type?: string,
    public codeFonction?: string,
    public criticite?: Shorthand
  ) {
  }
}
