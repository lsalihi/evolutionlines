export interface ICodification {
  id?: number;
  codeTable?: string;
  codeCodification?: string;
  codeLibelle?: string;
  codeActif?: boolean;
  societeId?: number;
}

export class Codification implements ICodification {
  constructor(
    public id?: number,
    public codeTable?: string,
    public codeCodification?: string,
    public codeLibelle?: string,
    public codeActif?: boolean,
    public societeId?: number
  ) {
    this.codeActif = this.codeActif || false;
  }
}
