import { Shorthand } from '@app/_models/_shared_models/shorthand.model';

export interface IMessage {
  id?: number;
  code?: string;
  libelle?: string;
  typeMessage?: Shorthand;
}

export class Message implements IMessage {
  constructor(
    public id?: number,
    public code?: string,
    public libelle?: string,
    public typeMessage?: Shorthand
  ) {
    
  }
}
