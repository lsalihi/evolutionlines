export interface IBanque {

	 id?: number;
     libelle?: string;
     abreviation?: string;
     description?: string;
     actif?: boolean;
}


export class Banque implements  IBanque{

	constructor(
				public id?: number,
				public libelle?: string,
				public abreviation?:string,
				public description?:string,
				public actif?:boolean
		) {}
}