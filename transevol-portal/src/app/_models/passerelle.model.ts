import { Moment } from 'moment';

export interface IPasserelle {
  id?: number;
  code?: string;
  libelle?: string;
  dateCreation?: Moment;
  suspendu?: boolean;
  dateSuspension?: Moment;
  circuitAllerId?: number;
  circuitRetourId?: number;
  ligneId?: number;
}

export class Passerelle implements IPasserelle {
  constructor(
    public id?: number,
    public code?: string,
    public libelle?: string,
    public dateCreation?: Moment,
    public suspendu?: boolean,
    public dateSuspension?: Moment,
    public circuitAllerId?: number,
    public circuitRetourId?: number,
    public ligneId?: number
  ) {
    this.suspendu = this.suspendu || false;
  }
}
