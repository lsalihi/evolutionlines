import { ITronconOrdre } from '@app/_models/troncon-ordre.model';
import { IGare } from '@app/_models/gare.model';

export interface ICircuit {
  id?: number;
  tronconsOrdres?: ITronconOrdre[];
  gareChangements?: IGare[];
}

export class Circuit implements ICircuit {
  constructor(public id?: number, public tronconsOrdres?: ITronconOrdre[], public gareChangements?: IGare[]) {}
}
