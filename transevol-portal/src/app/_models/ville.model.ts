export interface IVille {
  id?: number;
  libelle?: string;
  actif?: boolean;
  paysId?: number;
}

export class Ville implements IVille {
  constructor(public id?: number, public libelle?: string, public actif?: boolean, public paysId?: number) {
    this.actif = this.actif || false;
  }
}
