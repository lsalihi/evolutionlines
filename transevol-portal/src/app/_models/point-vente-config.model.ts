export interface IPointVenteConfig {
  id?: number;
}

export class PointVenteConfig implements IPointVenteConfig {
  constructor(public id?: number) {}
}
