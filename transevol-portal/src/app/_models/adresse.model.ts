export interface IAdresse {
  id?: number;
  adresse?: string;
  complementAdresse?: string;
  codePostal?: string;
}

export class Adresse implements IAdresse {
  constructor(public id?: number, public adresse?: string, public complementAdresse?: string, public codePostal?: string) {}
}
