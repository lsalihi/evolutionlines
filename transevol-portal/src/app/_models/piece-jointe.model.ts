import { Moment } from 'moment';

export interface IPieceJointe {
  id?: number;
  libelle?: string;
  nomPj?: string;
  dateAjout?: Moment;
  path?: string;
  fileType?: string;
  description?: string;
  statutPieceJointeId?: number;
  agenceId?: number;
}

export class PieceJointe implements IPieceJointe {
  constructor(
    public id?: number,
    public libelle?: string,
    public nomPj?: string,
    public dateAjout?: Moment,
    public path?: string,
    public fileType?: string,
    public description?: string,
    public statutPieceJointeId?: number,
    public agenceId?: number
  ) {}
}
