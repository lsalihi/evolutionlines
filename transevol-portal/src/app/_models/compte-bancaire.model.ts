export interface ICompteBancaire {
  id?: number;
  rib?: string;
  banqueId?: number;
  adresseAgenceId?: number;
  contactAgenceId?: number;
  villeAgenceId?: number;
}

export class CompteBancaire implements ICompteBancaire {
  constructor(
    public id?: number,
    public rib?: string,
    public banqueId?: number,
    public adresseAgenceId?: number,
    public contactAgenceId?: number,
    public villeAgenceId?: number
  ) {}
}
