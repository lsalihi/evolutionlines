import { Moment } from 'moment';

export interface IPays {
  id?: number;
  abreviation?: string;
  codeAlphanum?: string;
  codeNumerique?: string;
  dateDebutHeureEte?: Moment;
  dateFinHeureEte?: Moment;
  heureEte?: boolean;
  decalageHoraire?: string;
  libelle?: string;
  actif?: boolean;
  deviseId?: number;
}

export class Pays implements IPays {
  constructor(
    public id?: number,
    public abreviation?: string,
    public codeAlphanum?: string,
    public codeNumerique?: string,
    public dateDebutHeureEte?: Moment,
    public dateFinHeureEte?: Moment,
    public heureEte?: boolean,
    public decalageHoraire?: string,
    public libelle?: string,
    public actif?: boolean,
    public deviseId?: number
  ) {
    this.heureEte = this.heureEte || false;
    this.actif = this.actif || false;
  }
}
