import { Moment } from 'moment';

export interface IHoraireService {
  id?: number;
  ouverture?: Moment;
  fermeture?: Moment;
  agenceId?: number;
}

export class HoraireService implements IHoraireService {
  constructor(public id?: number, public ouverture?: Moment, public fermeture?: Moment, public agenceId?: number) {}
}
