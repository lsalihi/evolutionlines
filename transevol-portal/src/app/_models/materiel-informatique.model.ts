export interface IMaterielInformatique {
  id?: number;
  reference?: string;
  commentaire?: string;
  statut?: boolean;
  typeEquipementId?: number;
  marqueEquipementId?: number;
  agenceId?: number;
}

export class MaterielInformatique implements IMaterielInformatique {
  constructor(
    public id?: number,
    public reference?: string,
    public commentaire?: string,
    public statut?: boolean,
    public typeEquipementId?: number,
    public marqueEquipementId?: number,
    public agenceId?: number
  ) {
    this.statut = this.statut || false;
  }
}
