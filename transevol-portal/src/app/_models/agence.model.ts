import { Moment } from 'moment';
import { IContact } from '@app/_models/contact.model';
import { IMaterielInformatique } from '@app/_models/materiel-informatique.model';
import { IPieceJointe } from '@app/_models/piece-jointe.model';
import { IHoraireService } from '@app/_models/horaire-service.model';

export interface IAgence {
  id?: number;
  libelle?: string;
  code?: string;
  codeComptable?: string;
  identificationFiscale?: string;
  dateCreation?: Moment;
  debutService?: Moment;
  finService?: Moment;
  dateOuverture?: Moment;
  dateMiseEnProd?: Moment;
  taxeProfessionnelles?: number;
  fondDepense?: number;
  seuilVersementIntermediaire?: number;
  informatise?: boolean;
  dateDesactivation?: Moment;
  motifDesactivation?: string;
  motifFermeture?: string;
  motifReouverture?: string;
  motifStatutMetier?: string;
  dateFermeture?: Moment;
  actif?: boolean;
  adresseId?: number;
  pointVenteId?: number;
  compteBancaireId?: number;
  contacts?: IContact[];
  materielsInformatiques?: IMaterielInformatique[];
  piecesJointes?: IPieceJointe[];
  horairesServices?: IHoraireService[];
  villeId?: number;
  societeId?: number;
  agenceBusinessStatusId?: number;
}

export class Agence implements IAgence {
  constructor(
    public id?: number,
    public libelle?: string,
    public code?: string,
    public codeComptable?: string,
    public identificationFiscale?: string,
    public dateCreation?: Moment,
    public debutService?: Moment,
    public finService?: Moment,
    public dateOuverture?: Moment,
    public dateMiseEnProd?: Moment,
    public taxeProfessionnelles?: number,
    public fondDepense?: number,
    public seuilVersementIntermediaire?: number,
    public informatise?: boolean,
    public dateDesactivation?: Moment,
    public motifDesactivation?: string,
    public motifFermeture?: string,
    public motifReouverture?: string,
    public motifStatutMetier?: string,
    public dateFermeture?: Moment,
    public actif?: boolean,
    public adresseId?: number,
    public pointVenteId?: number,
    public compteBancaireId?: number,
    public contacts?: IContact[],
    public materielsInformatiques?: IMaterielInformatique[],
    public piecesJointes?: IPieceJointe[],
    public horairesServices?: IHoraireService[],
    public villeId?: number,
    public societeId?: number,
    public agenceBusinessStatusId?: number
  ) {
    this.informatise = this.informatise || false;
    this.actif = this.actif || false;
  }
}
