export interface ITronconOrdre {
  id?: number;
  ordre?: number;
  tronconId?: number;
  circuitId?: number;
}

export class TronconOrdre implements ITronconOrdre {
  constructor(public id?: number, public ordre?: number, public tronconId?: number, public circuitId?: number) {}
}
