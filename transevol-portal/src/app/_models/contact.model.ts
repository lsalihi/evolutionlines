export interface IContact {
  id?: number;
  email?: string;
  commentaire?: string;
  nom?: string;
  prenom?: string;
  principal?: boolean;
  telFixe?: string;
  telPortable?: string;
  societeId?: number;
  agenceId?: number;
}

export class Contact implements IContact {
  constructor(
    public id?: number,
    public email?: string,
    public commentaire?: string,
    public nom?: string,
    public prenom?: string,
    public principal?: boolean,
    public telFixe?: string,
    public telPortable?: string,
    public societeId?: number,
    public agenceId?: number
  ) {
    this.principal = this.principal || false;
  }
}
