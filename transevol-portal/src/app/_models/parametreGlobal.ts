export  class ParametreGlobal{

    titreMinistere? : string;

	/** The titre moa. */
	titreMoa? : string;

	/** The titre moa coreal. */
	titreMoaCoreal? : string;

	/** The titre moe. */
	titreMoe? : string;

	/** The message accueil. */
	messageAccueil? : string;

	/** The faq. */
	faq? : string;

	/** The aide generale. */
	aideGenerale? : string;
}