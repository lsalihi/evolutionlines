import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IGare, Gare } from '@app/_models/gare.model';
import { GareService } from '@app/_services/gare.service';
import { IVille } from '@app/_models/ville.model';
import { VilleService } from '@app/_services/ville.service';
import { IAgence } from '@app/_models/agence.model';
import { AgenceService } from '@app/_services/agence.service';
import { ICircuit } from '@app/_models/circuit.model';
import { CircuitService } from '@app/_services/circuit.service';

@Component({
  selector: 'jhi-gare-update',
  templateUrl: './gare-update.component.html'
})
export class GareUpdateComponent implements OnInit {
  isSaving: boolean;

  villes: IVille[];

  agences: IAgence[];

  circuits: ICircuit[];

  editForm = this.fb.group({
    id: [],
    libelle: [],
    correspondance: [],
    gareAgence: [],
    actif: [],
    villeId: [],
    agenceId: []
  });

  constructor(
    protected gareService: GareService,
    protected villeService: VilleService,
    protected agenceService: AgenceService,
    protected circuitService: CircuitService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ gare }) => {
      this.updateForm(gare);
    });
    this.villeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IVille[]>) => mayBeOk.ok),
        map((response: HttpResponse<IVille[]>) => response.body)
      )
      .subscribe((res: IVille[]) => (this.villes = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.agenceService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IAgence[]>) => mayBeOk.ok),
        map((response: HttpResponse<IAgence[]>) => response.body)
      )
      .subscribe((res: IAgence[]) => (this.agences = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.circuitService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICircuit[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICircuit[]>) => response.body)
      )
      .subscribe((res: ICircuit[]) => (this.circuits = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(gare: IGare) {
    this.editForm.patchValue({
      id: gare.id,
      libelle: gare.libelle,
      correspondance: gare.correspondance,
      gareAgence: gare.gareAgence,
      actif: gare.actif,
      villeId: gare.villeId,
      agenceId: gare.agenceId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const gare = this.createFromForm();
    if (gare.id !== undefined) {
      this.subscribeToSaveResponse(this.gareService.update(gare));
    } else {
      this.subscribeToSaveResponse(this.gareService.create(gare));
    }
  }

  private createFromForm(): IGare {
    return {
      ...new Gare(),
      id: this.editForm.get(['id']).value,
      libelle: this.editForm.get(['libelle']).value,
      correspondance: this.editForm.get(['correspondance']).value,
      gareAgence: this.editForm.get(['gareAgence']).value,
      actif: this.editForm.get(['actif']).value,
      villeId: this.editForm.get(['villeId']).value,
      agenceId: this.editForm.get(['agenceId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IGare>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
  //  this.jhiAlertService.error(errorMessage, null, null);
  }

  trackVilleById(index: number, item: IVille) {
    return item.id;
  }

  trackAgenceById(index: number, item: IAgence) {
    return item.id;
  }

  trackCircuitById(index: number, item: ICircuit) {
    return item.id;
  }

  getSelected(selectedVals: any[], option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
