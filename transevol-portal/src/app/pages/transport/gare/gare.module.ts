import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LandlinesSharedLibsModule } from '@app/_shared/shared-libs.module';
import { LandlinesSharedModule } from '@app/_shared/shared.module';
import { GareComponent } from './gare.component';
import { GareDetailComponent } from './gare-detail.component';
import { GareUpdateComponent } from './gare-update.component';
import { GareDeletePopupComponent, GareDeleteDialogComponent } from './gare-delete-dialog.component';
import { gareRoute, garePopupRoute } from './gare.route';

const ENTITY_STATES = [...gareRoute, ...garePopupRoute];

@NgModule({
  imports: [LandlinesSharedModule,LandlinesSharedLibsModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [GareComponent, GareDetailComponent, GareUpdateComponent, GareDeleteDialogComponent, GareDeletePopupComponent],
  entryComponents: [GareDeleteDialogComponent]
})
export class LandlinesGareModule {}
