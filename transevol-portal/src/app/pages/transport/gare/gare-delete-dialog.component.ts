import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { IGare } from '@app/_models/gare.model';
import { GareService } from '@app/_services/gare.service';

@Component({
  selector: 'jhi-gare-delete-dialog',
  templateUrl: './gare-delete-dialog.component.html'
})
export class GareDeleteDialogComponent {
  gare: IGare;

  constructor(protected gareService: GareService, public activeModal: NgbActiveModal) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.gareService.delete(id).subscribe(response => {
   //   this.eventManager.broadcast({
     //   name: 'gareListModification',
     //   content: 'Deleted an gare'
     // });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-gare-delete-popup',
  template: ''
})
export class GareDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ gare }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(GareDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.gare = gare;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/gare', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/gare', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
