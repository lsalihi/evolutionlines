import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IGare } from '@app/_models/gare.model';

@Component({
  selector: 'jhi-gare-detail',
  templateUrl: './gare-detail.component.html'
})
export class GareDetailComponent implements OnInit {
  gare: IGare;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ gare }) => {
      this.gare = gare;
    });
  }

  previousState() {
    window.history.back();
  }
}
