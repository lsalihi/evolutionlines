import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { ResolvePagingParams } from '@app/_shared/util/resolve-paging-params.service';
import { AuthGuard } from '@app/_helpers';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Gare } from '@app/_models/gare.model';
import { GareService } from '@app/_services/gare.service';
import { GareComponent } from './gare.component';
import { GareDetailComponent } from './gare-detail.component';
import { GareUpdateComponent } from './gare-update.component';
import { GareDeletePopupComponent } from './gare-delete-dialog.component';
import { IGare } from '@app/_models/gare.model';

@Injectable({ providedIn: 'root' })
export class GareResolve implements Resolve<IGare> {
  constructor(private service: GareService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IGare> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Gare>) => response.ok),
        map((gare: HttpResponse<Gare>) => gare.body)
      );
    }
    return of(new Gare());
  }
}

export const gareRoute: Routes = [
  {
    path: '',
    component: GareComponent,
    resolve: {
      pagingParams: ResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'Gares'
    },
    canActivate: [AuthGuard]
  },
  {
    path: ':id/view',
    component: GareDetailComponent,
    resolve: {
      gare: GareResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Gares'
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'new',
    component: GareUpdateComponent,
    resolve: {
      gare: GareResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Gares'
    },
    canActivate: [AuthGuard]
  },
  {
    path: ':id/edit',
    component: GareUpdateComponent,
    resolve: {
      gare: GareResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Gares'
    },
    canActivate: [AuthGuard]
  }
];

export const garePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: GareDeletePopupComponent,
    resolve: {
      gare: GareResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Gares'
    },
    canActivate: [AuthGuard],
    outlet: 'popup'
  }
];
