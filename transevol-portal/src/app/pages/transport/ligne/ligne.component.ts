import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ILigne } from '@app/_models/ligne.model';
import { LigneService } from '@app/_services/ligne.service';
import { ParseLinks } from '@app/_shared/util/ParseLinks';
import { ITEMS_PER_PAGE } from '@app/_shared/constants/pagination.constants';

@Component({
  selector: 'jhi-ligne',
  templateUrl: './ligne.component.html'
})
export class LigneComponent implements OnInit, OnDestroy {
  currentAccount: any;
  lignes: ILigne[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;

  constructor(
    protected ligneService: LigneService,
    protected parseLinks: ParseLinks,
    protected activatedRoute: ActivatedRoute,
    protected router: Router
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
  }

  loadAll() {
    this.ligneService
      .query({
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<ILigne[]>) => this.paginateLignes(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(['/ligne'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    this.loadAll();
  }

  clear() {
    this.page = 0;
    this.router.navigate([
      '/ligne',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
    /*this.accountService.identity().then(account => {
      this.currentAccount = account;
    });*/
    this.registerChangeInLignes();
  }

  ngOnDestroy() {
   // this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ILigne) {
    return item.id;
  }

  registerChangeInLignes() {
    //this.eventSubscriber = this.eventManager.subscribe('ligneListModification', response => this.loadAll());
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateLignes(data: ILigne[], headers: HttpHeaders) {
    //this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems =10;// parseInt(headers.get('X-Total-Count'), 10);
    this.lignes = data;
  }

  protected onError(errorMessage: string) {
    //this.jhiAlertService.error(errorMessage, null, null);
  }
}
