import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { ILigne } from '@app/_models/ligne.model';
import { LigneService } from '@app/_services/ligne.service';

@Component({
  selector: 'jhi-ligne-delete-dialog',
  templateUrl: './ligne-delete-dialog.component.html'
})
export class LigneDeleteDialogComponent {
  ligne: ILigne;

  constructor(protected ligneService: LigneService, public activeModal: NgbActiveModal) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.ligneService.delete(id).subscribe(response => {
     /* this.eventManager.broadcast({
        name: 'ligneListModification',
        content: 'Deleted an ligne'
      });*/
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-ligne-delete-popup',
  template: ''
})
export class LigneDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ ligne }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(LigneDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.ligne = ligne;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/ligne', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/ligne', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
