import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { LigneComponent } from './ligne.component';
import { LigneDetailComponent } from './ligne-detail.component';
import { LigneUpdateComponent } from './ligne-update.component';
import { LigneDeletePopupComponent } from './ligne-delete-dialog.component';
import { ILigne, Ligne } from '@app/_models/ligne.model';
import { LigneService } from '@app/_services/ligne.service';
import { ResolvePagingParams } from '@app/_shared/util/resolve-paging-params.service';
import { AuthGuard } from '@app/_helpers';

@Injectable({ providedIn: 'root' })
export class LigneResolve implements Resolve<ILigne> {
  constructor(private service: LigneService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ILigne> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Ligne>) => response.ok),
        map((ligne: HttpResponse<Ligne>) => ligne.body)
      );
    }
    return of(new Ligne());
  }
}

export const ligneRoute: Routes = [
  {
    path: '',
    component: LigneComponent,
    resolve: {
      pagingParams: ResolvePagingParams
    },
    /*data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'Lignes'
    },*/
    canActivate: [AuthGuard]
  },
  {
    path: ':id/view',
    component: LigneDetailComponent,
    resolve: {
      ligne: LigneResolve
    },
    /*data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Lignes'
    },*/
    canActivate: [AuthGuard]
  },
  {
    path: 'new',
    component: LigneUpdateComponent,
    resolve: {
      ligne: LigneResolve
    },
    /*data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Lignes'
    },*/
    canActivate: [AuthGuard]
  },
  {
    path: ':id/edit',
    component: LigneUpdateComponent,
    resolve: {
      ligne: LigneResolve
    },
    /*data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Lignes'
    },*/
    canActivate: [AuthGuard]
  }
];

export const lignePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: LigneDeletePopupComponent,
    resolve: {
      ligne: LigneResolve
    },
    /*data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Lignes'
    },*/
    canActivate: [AuthGuard],
    outlet: 'popup'
  }
];
