import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ILigne } from '@app/_models/ligne.model';


@Component({
  selector: 'jhi-ligne-detail',
  templateUrl: './ligne-detail.component.html'
})
export class LigneDetailComponent implements OnInit {
  ligne: ILigne;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ ligne }) => {
      this.ligne = ligne;
    });
  }

  previousState() {
    window.history.back();
  }
}
