import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LigneComponent } from './ligne.component';
import { LigneDetailComponent } from './ligne-detail.component';
import { LigneUpdateComponent } from './ligne-update.component';
import { LigneDeletePopupComponent, LigneDeleteDialogComponent } from './ligne-delete-dialog.component';
import { ligneRoute, lignePopupRoute } from './ligne.route';
import { LandlinesSharedLibsModule } from '@app/_shared/shared-libs.module';
import { LandlinesSharedModule } from '@app/_shared/shared.module';
import { LigneService } from '@app/_services/ligne.service';

const ENTITY_STATES = [...ligneRoute, ...lignePopupRoute];
//declaration update component passerelle
@NgModule({
  imports: [LandlinesSharedLibsModule,
    LandlinesSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [LigneComponent, LigneDetailComponent, LigneUpdateComponent, LigneDeleteDialogComponent, LigneDeletePopupComponent],
  entryComponents: [LigneDeleteDialogComponent],
  providers : [LigneService]
})
export class LandlinesLigneModule {}
