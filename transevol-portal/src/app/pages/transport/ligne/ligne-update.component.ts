import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { ICodification } from '@app/_models/codification.model';
import { IPasserelle } from '@app/_models/passerelle.model';
import { LigneService } from '@app/_services/ligne.service';
import { CodificationService } from '@app/_services/codification.service';
import { ILigne, Ligne } from '@app/_models/ligne.model';
import { IGare } from '@app/_models/gare.model';
import { GareService } from '@app/_services/gare.service';
import { CodificationEnum } from '@app/_shared/constants/codification-enum';
import { MatDialog } from '@angular/material';
@Component({
  selector: 'jhi-ligne-update',
  templateUrl: './ligne-update.component.html'
})
export class LigneUpdateComponent implements OnInit {
  isSaving: boolean;
  hideInfoLigne: boolean = false;
  gares: IGare[];

  naturesDepart: ICodification[];
  dateCreationDp: any;
  dateSuspensionDp: any;
  passerelles: IPasserelle[];
  passerelle: IPasserelle;

  editForm = this.fb.group({
    id: [],
    libelle: [],
    code: [],
    dateCreation: [],
    suspendu: [],
    dateSuspension: [],
    actif: [],
    gareDepartId: [],
    gareDestinationId: [],
    classeId: [],
    passerelles: this.fb.array([])
  });

  constructor(
    protected ligneService: LigneService,
    protected gareService: GareService,
    protected codificationService: CodificationService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.isSaving = false;
    //this.passerelles = [];
    this.activatedRoute.data.subscribe(({ ligne }) => {
      this.updateForm(ligne);
    });
    this.gareService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IGare[]>) => mayBeOk.ok),
        map((response: HttpResponse<IGare[]>) => response.body)
      )
      .subscribe((res: IGare[]) => (this.gares = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.codificationService
      .findByCode(CodificationEnum.NATURE_DEPART)
      .pipe(
        filter((mayBeOk: HttpResponse<ICodification[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICodification[]>) => response.body)
      )
      .subscribe((res: ICodification[]) => (this.naturesDepart = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(ligne: ILigne) {
    if (ligne.id === undefined) {
      ligne.dateCreation = moment();
    }
    this.editForm.patchValue({
      id: ligne.id,
      libelle: ligne.libelle,
      code: ligne.code,
      dateCreation: ligne.dateCreation,
      suspendu: ligne.suspendu,
      dateSuspension: ligne.dateSuspension,
      actif: ligne.actif,
      gareDepartId: ligne.gareDepartId,
      gareDestinationId: ligne.gareDestinationId,
      classeId: ligne.classeId,
      passerelles: this.fb.array([])
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const ligne = this.createFromForm();
    if (ligne.id !== undefined) {
      this.subscribeToSaveResponse(this.ligneService.update(ligne));
    } else {
      this.subscribeToSaveResponse(this.ligneService.create(ligne));
    }
  }

  private createFromForm(): ILigne {
    return {
      ...new Ligne(),
      id: this.editForm.get(['id']).value,
      libelle: this.editForm.get(['libelle']).value,
      code: this.editForm.get(['code']).value,
      dateCreation: this.editForm.get(['dateCreation']).value,
      suspendu: this.editForm.get(['suspendu']).value,
      dateSuspension: this.editForm.get(['dateSuspension']).value,
      actif: this.editForm.get(['actif']).value,
      gareDepartId: this.editForm.get(['gareDepartId']).value,
      gareDestinationId: this.editForm.get(['gareDestinationId']).value,
      classeId: this.editForm.get(['classeId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ILigne>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    //this.jhiAlertService.error(errorMessage, null, null);
  }

  trackGareById(index: number, item: IGare) {
    return item.id;
  }

  trackCodificationById(index: number, item: ICodification) {
    return item.id;
  }

  /** passrelles*/
  getPasserelles(): FormArray {
    return this.editForm.get('passerelles') as FormArray;
  }
/*  onAddPasserelle() {
    const newPasserelleControl = this.fb.control(null, Validators.required);
    alert('onAddPasserelle' + JSON.stringify(newPasserelleControl));
    this.getPasserelles().push(newPasserelleControl);
  }
*/
  addPasserelle() {
    const creds = this.editForm.controls.passerelles as FormArray;
    creds.push(this.fb.group({
      libelle: '',
    }));
  }

  aonAddPasserelle(passerelle: IPasserelle) {
    console.log("onAddPasserelle", passerelle);
  }

  onAddPasserelle() {
    console.log("onAddPasserelle");
  }

}
