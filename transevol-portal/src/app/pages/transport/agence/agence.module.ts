import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LandlinesSharedLibsModule } from '@app/_shared/shared-libs.module';
import { LandlinesSharedModule } from '@app/_shared/shared.module';
import { AgenceComponent } from './agence.component';
import { AgenceDetailComponent } from './agence-detail.component';
import { AgenceUpdateComponent } from './agence-update.component';
import { AgenceDeletePopupComponent, AgenceDeleteDialogComponent } from './agence-delete-dialog.component';
import { agenceRoute, agencePopupRoute } from './agence.route';

const ENTITY_STATES = [...agenceRoute, ...agencePopupRoute];

@NgModule({
  imports: [LandlinesSharedLibsModule,LandlinesSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [AgenceComponent, AgenceDetailComponent, AgenceUpdateComponent, AgenceDeleteDialogComponent, AgenceDeletePopupComponent],
  entryComponents: [AgenceDeleteDialogComponent]
})
export class LandlinesAgenceModule {}
