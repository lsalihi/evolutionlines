import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { filter, map } from 'rxjs/operators';

import { IAgence } from '@app/_models/agence.model';

import { ITEMS_PER_PAGE } from '@app/_shared/constants/pagination.constants';
import { AgenceService } from '@app/_services/agence.service';

import { ParseLinks } from '@app/_shared/util/ParseLinks';

@Component({
  selector: 'jhi-agence',
  templateUrl: './agence.component.html'
})


export class AgenceComponent implements OnInit, OnDestroy {
  currentAccount: any;
  agences: IAgence[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;


  constructor(
    protected agenceService: AgenceService,
      protected parseLinks: ParseLinks,
    protected activatedRoute: ActivatedRoute,
    protected router: Router
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
  }

  loadAll() {

   console.log("loadAgences");
    this.agenceService
      .query({
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IAgence[]>) => this.paginateAgences(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(['/agence'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    this.loadAll();
  }

  clear() {
    this.page = 0;
    this.router.navigate([
      '/agence',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
   // this.accountService.identity().then(account => {
   //   this.currentAccount = account;
  //  });
    this.registerChangeInAgences();
  }

  ngOnDestroy() {
//    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IAgence) {
    return item.id;
  }

  registerChangeInAgences() {
   // this.eventSubscriber = this.eventManager.subscribe('agenceListModification', response => this.loadAll());
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateAgences(data: IAgence[], headers: HttpHeaders) {
 
    this.agences = data;
    this.totalItems = 10;

 
  }

  protected onError(errorMessage: string) {
   // this.jhiAlertService.error(errorMessage, null, null);
  }
}
