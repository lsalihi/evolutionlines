import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { IAgence, Agence } from '@app/_models/agence.model';
import { AgenceService } from '@app/_services/agence.service';
import { IAdresse } from '@app/_models/adresse.model';
import { AdresseService } from '@app/_services/adresse.service';
import { IPointVenteConfig } from '@app/_models/point-vente-config.model';
import { PointVenteConfigService } from '@app/_services/point-vente-config.service';
import { ICompteBancaire } from '@app/_models/compte-bancaire.model';
import { CompteBancaireService } from '@app/_services/compte-bancaire.service';
import { IVille } from '@app/_models/ville.model';
import { VilleService } from '@app/_services/ville.service';
import { ISociete } from '@app/_models/societe.model';
import { SocieteService } from '@app/_services/societe.service';
import { ICodification } from '@app/_models/codification.model';
import { CodificationService } from '@app/_services/codification.service';

@Component({
  selector: 'jhi-agence-update',
  templateUrl: './agence-update.component.html'
})
export class AgenceUpdateComponent implements OnInit {
  isSaving: boolean;

affichage: number;

  adresses: IAdresse[];

  pointventes: IPointVenteConfig[];

  comptebancaires: ICompteBancaire[];

  villes: IVille[];

  societes: ISociete[];

  codifications: ICodification[];
  dateCreationDp: any;
  debutServiceDp: any;
  finServiceDp: any;
  dateOuvertureDp: any;
  dateMiseEnProdDp: any;
  dateDesactivationDp: any;
  dateFermetureDp: any;

  editForm = this.fb.group({
    id: [],
    libelle: [],
    code: [],
    codeComptable: [],
    identificationFiscale: [],
    dateCreation: [],
    debutService: [],
    finService: [],
    dateOuverture: [],
    dateMiseEnProd: [],
    taxeProfessionnelles: [],
    fondDepense: [],
    seuilVersementIntermediaire: [],
    informatise: [],
    dateDesactivation: [],
    motifDesactivation: [],
    motifFermeture: [],
    motifReouverture: [],
    motifStatutMetier: [],
    dateFermeture: [],
    actif: [],
    adresseId: [],
    pointVenteId: [],
    compteBancaireId: [],
    villeId: [],
    societeId: [],
    agenceBusinessStatusId: []
  });

  constructor(
    protected agenceService: AgenceService,
    protected adresseService: AdresseService,
    protected pointVenteConfigService: PointVenteConfigService,
    protected compteBancaireService: CompteBancaireService,
    protected villeService: VilleService,
    protected societeService: SocieteService,
    protected codificationService: CodificationService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {
this.affichage=1;
}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ agence }) => {
      this.updateForm(agence);
    });
    this.adresseService
      .query({ filter: 'agence-is-null' })
      .pipe(
        filter((mayBeOk: HttpResponse<IAdresse[]>) => mayBeOk.ok),
        map((response: HttpResponse<IAdresse[]>) => response.body)
      )
      .subscribe(
        (res: IAdresse[]) => {
          if (!this.editForm.get('adresseId').value) {
            this.adresses = res;
          } else {
            this.adresseService
              .find(this.editForm.get('adresseId').value)
              .pipe(
                filter((subResMayBeOk: HttpResponse<IAdresse>) => subResMayBeOk.ok),
                map((subResponse: HttpResponse<IAdresse>) => subResponse.body)
              )
              .subscribe(
                (subRes: IAdresse) => (this.adresses = [subRes].concat(res)),
                (subRes: HttpErrorResponse) => this.onError(subRes.message)
              );
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.pointVenteConfigService
      .query({ filter: 'agence-is-null' })
      .pipe(
        filter((mayBeOk: HttpResponse<IPointVenteConfig[]>) => mayBeOk.ok),
        map((response: HttpResponse<IPointVenteConfig[]>) => response.body)
      )
      .subscribe(
        (res: IPointVenteConfig[]) => {
          if (!this.editForm.get('pointVenteId').value) {
            this.pointventes = res;
          } else {
            this.pointVenteConfigService
              .find(this.editForm.get('pointVenteId').value)
              .pipe(
                filter((subResMayBeOk: HttpResponse<IPointVenteConfig>) => subResMayBeOk.ok),
                map((subResponse: HttpResponse<IPointVenteConfig>) => subResponse.body)
              )
              .subscribe(
                (subRes: IPointVenteConfig) => (this.pointventes = [subRes].concat(res)),
                (subRes: HttpErrorResponse) => this.onError(subRes.message)
              );
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.compteBancaireService
      .query({ filter: 'agence-is-null' })
      .pipe(
        filter((mayBeOk: HttpResponse<ICompteBancaire[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICompteBancaire[]>) => response.body)
      )
      .subscribe(
        (res: ICompteBancaire[]) => {
          if (!this.editForm.get('compteBancaireId').value) {
            this.comptebancaires = res;
          } else {
            this.compteBancaireService
              .find(this.editForm.get('compteBancaireId').value)
              .pipe(
                filter((subResMayBeOk: HttpResponse<ICompteBancaire>) => subResMayBeOk.ok),
                map((subResponse: HttpResponse<ICompteBancaire>) => subResponse.body)
              )
              .subscribe(
                (subRes: ICompteBancaire) => (this.comptebancaires = [subRes].concat(res)),
                (subRes: HttpErrorResponse) => this.onError(subRes.message)
              );
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.villeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IVille[]>) => mayBeOk.ok),
        map((response: HttpResponse<IVille[]>) => response.body)
      )
      .subscribe((res: IVille[]) => (this.villes = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.societeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ISociete[]>) => mayBeOk.ok),
        map((response: HttpResponse<ISociete[]>) => response.body)
      )
      .subscribe((res: ISociete[]) => (this.societes = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.codificationService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICodification[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICodification[]>) => response.body)
      )
      .subscribe((res: ICodification[]) => (this.codifications = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(agence: IAgence) {
    this.editForm.patchValue({
      id: agence.id,
      libelle: agence.libelle,
      code: agence.code,
      codeComptable: agence.codeComptable,
      identificationFiscale: agence.identificationFiscale,
      dateCreation: agence.dateCreation,
      debutService: agence.debutService,
      finService: agence.finService,
      dateOuverture: agence.dateOuverture,
      dateMiseEnProd: agence.dateMiseEnProd,
      taxeProfessionnelles: agence.taxeProfessionnelles,
      fondDepense: agence.fondDepense,
      seuilVersementIntermediaire: agence.seuilVersementIntermediaire,
      informatise: agence.informatise,
      dateDesactivation: agence.dateDesactivation,
      motifDesactivation: agence.motifDesactivation,
      motifFermeture: agence.motifFermeture,
      motifReouverture: agence.motifReouverture,
      motifStatutMetier: agence.motifStatutMetier,
      dateFermeture: agence.dateFermeture,
      actif: agence.actif,
      adresseId: agence.adresseId,
      pointVenteId: agence.pointVenteId,
      compteBancaireId: agence.compteBancaireId,
      villeId: agence.villeId,
      societeId: agence.societeId,
      agenceBusinessStatusId: agence.agenceBusinessStatusId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const agence = this.createFromForm();
    if (agence.id !== undefined) {
      this.subscribeToSaveResponse(this.agenceService.update(agence));
    } else {
      this.subscribeToSaveResponse(this.agenceService.create(agence));
    }
  }

  private createFromForm(): IAgence {
    return {
      ...new Agence(),
      id: this.editForm.get(['id']).value,
      libelle: this.editForm.get(['libelle']).value,
      code: this.editForm.get(['code']).value,
      codeComptable: this.editForm.get(['codeComptable']).value,
      identificationFiscale: this.editForm.get(['identificationFiscale']).value,
      dateCreation: this.editForm.get(['dateCreation']).value,
      debutService: this.editForm.get(['debutService']).value,
      finService: this.editForm.get(['finService']).value,
      dateOuverture: this.editForm.get(['dateOuverture']).value,
      dateMiseEnProd: this.editForm.get(['dateMiseEnProd']).value,
      taxeProfessionnelles: this.editForm.get(['taxeProfessionnelles']).value,
      fondDepense: this.editForm.get(['fondDepense']).value,
      seuilVersementIntermediaire: this.editForm.get(['seuilVersementIntermediaire']).value,
      informatise: this.editForm.get(['informatise']).value,
      dateDesactivation: this.editForm.get(['dateDesactivation']).value,
      motifDesactivation: this.editForm.get(['motifDesactivation']).value,
      motifFermeture: this.editForm.get(['motifFermeture']).value,
      motifReouverture: this.editForm.get(['motifReouverture']).value,
      motifStatutMetier: this.editForm.get(['motifStatutMetier']).value,
      dateFermeture: this.editForm.get(['dateFermeture']).value,
      actif: this.editForm.get(['actif']).value,
      adresseId: this.editForm.get(['adresseId']).value,
      pointVenteId: this.editForm.get(['pointVenteId']).value,
      compteBancaireId: this.editForm.get(['compteBancaireId']).value,
      villeId: this.editForm.get(['villeId']).value,
      societeId: this.editForm.get(['societeId']).value,
      agenceBusinessStatusId: this.editForm.get(['agenceBusinessStatusId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAgence>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
 //   this.jhiAlertService.error(errorMessage, null, null);
  }

  trackAdresseById(index: number, item: IAdresse) {
    return item.id;
  }

  trackPointVenteConfigById(index: number, item: IPointVenteConfig) {
    return item.id;
  }

  trackCompteBancaireById(index: number, item: ICompteBancaire) {
    return item.id;
  }

  trackVilleById(index: number, item: IVille) {
    return item.id;
  }

  trackSocieteById(index: number, item: ISociete) {
    return item.id;
  }

  trackCodificationById(index: number, item: ICodification) {
    return item.id;
  }
changeAffichage(index: number) {
  this.affichage=index;
  }

}
