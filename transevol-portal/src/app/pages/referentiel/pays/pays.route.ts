import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Pays } from '@app/_models/pays.model';
import { PaysService } from '@app/_services/pays.service';
import { PaysComponent } from './pays.component';
import { PaysDetailComponent } from './pays-detail.component';
import { PaysUpdateComponent } from './pays-update.component';
import { PaysDeletePopupComponent } from './pays-delete-dialog.component';
import { IPays } from '@app/_models/pays.model';
import { AuthGuard } from '@app/_helpers';
import { ResolvePagingParams } from '@app/_shared/util/resolve-paging-params.service';

@Injectable({ providedIn: 'root' })
export class PaysResolve implements Resolve<IPays> {
  constructor(private service: PaysService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IPays> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Pays>) => response.ok),
        map((pays: HttpResponse<Pays>) => pays.body)
      );
    }
    return of(new Pays());
  }
}

export const paysRoute: Routes = [
  {
    path: '',
    component: PaysComponent,
    resolve: {
      pagingParams: ResolvePagingParams
    },
  
    canActivate: [AuthGuard]
  },
  {
    path: ':id/view',
    component: PaysDetailComponent,
    resolve: {
      pays: PaysResolve
    },
 
    canActivate: [AuthGuard]
  },
  {
    path: 'new',
    component: PaysUpdateComponent,
    resolve: {
      pays: PaysResolve
    },
  
    canActivate: [AuthGuard]
  },
  {
    path: ':id/edit',
    component: PaysUpdateComponent,
    resolve: {
      pays: PaysResolve
    },
   
    canActivate: [AuthGuard]
  }
];

export const paysPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: PaysDeletePopupComponent,
    resolve: {
      pays: PaysResolve
    },

    canActivate: [AuthGuard],
    outlet: 'popup'
  }
];
