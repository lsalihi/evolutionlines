import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LandlinesSharedModule } from '@app/_shared/shared.module';
import { PaysComponent } from './pays.component';
import { PaysDetailComponent } from './pays-detail.component';
import { PaysUpdateComponent } from './pays-update.component';
import { PaysDeletePopupComponent, PaysDeleteDialogComponent } from './pays-delete-dialog.component';
import { paysRoute, paysPopupRoute } from './pays.route';
import { LandlinesSharedLibsModule } from '@app/_shared/shared-libs.module';

const ENTITY_STATES = [...paysRoute, ...paysPopupRoute];

@NgModule({
  imports: [LandlinesSharedModule,LandlinesSharedLibsModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [PaysComponent, PaysDetailComponent, PaysUpdateComponent, PaysDeleteDialogComponent, PaysDeletePopupComponent],
  entryComponents: [PaysDeleteDialogComponent]
})
export class LandlinesPaysModule {}
