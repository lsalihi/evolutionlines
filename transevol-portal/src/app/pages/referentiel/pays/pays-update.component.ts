import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { IPays, Pays } from '@app/_models/pays.model';
import { PaysService } from '@app/_services/pays.service';
import { IDevise } from '@app/_models/devise.model';
import { DeviseService } from '@app/_services/devise.service';

@Component({
  selector: 'jhi-pays-update',
  templateUrl: './pays-update.component.html'
})
export class PaysUpdateComponent implements OnInit {
  isSaving: boolean;
  devises: IDevise[];
  dateDebutHeureEteDp: any;
  dateFinHeureEteDp: any;

  editForm = this.fb.group({
    id: [],
    abreviation: [],
    codeAlphanum: [],
    codeNumerique: [],
    dateDebutHeureEte: [],
    dateFinHeureEte: [],
    heureEte: [],
    decalageHoraire: [],
    libelle: [],
    actif: [],
    deviseId: []
  });

  constructor(

    protected paysService: PaysService,
    protected deviseService: DeviseService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ pays }) => {
      this.updateForm(pays);
    });
    this.deviseService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IDevise[]>) => mayBeOk.ok),
        map((response: HttpResponse<IDevise[]>) => response.body)
      )
      .subscribe((res: IDevise[]) => (this.devises = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(pays: IPays) {
    this.editForm.patchValue({
      id: pays.id,
      abreviation: pays.abreviation,
      codeAlphanum: pays.codeAlphanum,
      codeNumerique: pays.codeNumerique,
      dateDebutHeureEte: pays.dateDebutHeureEte,
      dateFinHeureEte: pays.dateFinHeureEte,
      heureEte: pays.heureEte,
      decalageHoraire: pays.decalageHoraire,
      libelle: pays.libelle,
      actif: pays.actif,
      deviseId: pays.deviseId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const pays = this.createFromForm();
    if (pays.id !== undefined) {
      this.subscribeToSaveResponse(this.paysService.update(pays));
    } else {
      this.subscribeToSaveResponse(this.paysService.create(pays));
    }
  }

  private createFromForm(): IPays {
    return {
      ...new Pays(),
      id: this.editForm.get(['id']).value,
      abreviation: this.editForm.get(['abreviation']).value,
      codeAlphanum: this.editForm.get(['codeAlphanum']).value,
      codeNumerique: this.editForm.get(['codeNumerique']).value,
      dateDebutHeureEte: this.editForm.get(['dateDebutHeureEte']).value,
      dateFinHeureEte: this.editForm.get(['dateFinHeureEte']).value,
      heureEte: this.editForm.get(['heureEte']).value,
      decalageHoraire: this.editForm.get(['decalageHoraire']).value,
      libelle: this.editForm.get(['libelle']).value,
      actif: this.editForm.get(['actif']).value,
      deviseId: this.editForm.get(['deviseId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPays>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
   // this.jhiAlertService.error(errorMessage, null, null);
  }

  trackDeviseById(index: number, item: IDevise) {
    return item.id;
  }
}
