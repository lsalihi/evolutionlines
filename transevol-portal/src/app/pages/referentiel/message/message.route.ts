import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { MessageComponent } from './message.component';
import { MessageDetailComponent } from './message-detail.component';
import { MessageUpdateComponent } from './message-update.component';
import { MessageDeletePopupComponent } from './message-delete-dialog.component';
import { ResolvePagingParams } from '@app/_shared/util/resolve-paging-params.service';
import { IMessage, Message } from '@app/_models/message.model';
import { Shorthand } from '@app/_models/_shared_models/shorthand.model';
import { MessageService } from '@app/_services';
import { UserRouteAccessService } from '@app/_services/user-route-access-service';
import { AuthGuard } from '@app/_helpers';

@Injectable({ providedIn: 'root' })
export class MessageResolve implements Resolve<IMessage> {
  constructor(private service: MessageService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IMessage> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Message>) => response.ok),
        map((message: HttpResponse<Message>) => message.body)
      );
    }
    
    return of(new Message(undefined,undefined,undefined,new Shorthand()));
  }
}

export const messageRoute: Routes = [
  {
    path: '',
    component: MessageComponent,
    resolve: {
      pagingParams: ResolvePagingParams
    },
    /*data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'Messages'
    },*/
    canActivate: [AuthGuard]
  },
  {
    path: ':id/view',
    component: MessageDetailComponent,
    resolve: {
      message: MessageResolve
    },
   /* data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Messages'
    },*/
    canActivate: [AuthGuard]
  },
  {
    path: 'new',
    component: MessageUpdateComponent,
    resolve: {
      message: MessageResolve
    },
    /*data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Messages'
    },*/
    canActivate: [AuthGuard]
  },
  {
    path: ':id/edit',
    component: MessageUpdateComponent,
    resolve: {
      message: MessageResolve
    },
    /*data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Messages'
    },*/
    canActivate: [AuthGuard]
  }
];

export const messagePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: MessageDeletePopupComponent,
    resolve: {
      message: MessageResolve
    },
    /*data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Messages'
    },*/
    canActivate: [AuthGuard /*UserRouteAccessService*/],
    outlet: 'popup'
  }
];
