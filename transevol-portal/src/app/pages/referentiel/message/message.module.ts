import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';


import { MessageComponent } from './message.component';
import { MessageDetailComponent } from './message-detail.component';
import { MessageUpdateComponent } from './message-update.component';
import { MessageDeletePopupComponent, MessageDeleteDialogComponent } from './message-delete-dialog.component';
import { messageRoute, messagePopupRoute } from './message.route';
import { LandlinesSharedLibsModule } from '@app/_shared/shared-libs.module';
import { LandlinesSharedModule } from '@app/_shared/shared.module';
import { MessageService } from '@app/_services';

const ENTITY_STATES = [...messageRoute, ...messagePopupRoute];

@NgModule({
  imports: [LandlinesSharedLibsModule,
    LandlinesSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [MessageComponent, MessageDetailComponent, MessageUpdateComponent, MessageDeleteDialogComponent, MessageDeletePopupComponent],
  entryComponents: [MessageDeleteDialogComponent],
  providers : [MessageService]
})
export class LandlinesMessageModule { }
