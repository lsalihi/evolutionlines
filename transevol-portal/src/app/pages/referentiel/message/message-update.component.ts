import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { MessageService, TypeMessageService } from '@app/_services';
import { IMessage, Message } from '@app/_models/message.model';
import { Shorthand } from '@app/_models/_shared_models/shorthand.model';

@Component({
  selector: 'jhi-message-update',
  templateUrl: './message-update.component.html'
})
export class MessageUpdateComponent implements OnInit {
  isSaving: boolean;

  typeMessages : Shorthand[];

  editForm = this.fb.group({
    id: [],
    code: [],
    libelle: [],
    typeMessage: []
  });

  constructor(protected messageService: MessageService,
              protected activatedRoute: ActivatedRoute,
              protected typeMessageService: TypeMessageService,
              private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;

    this.typeMessageService
      .queryShorthandTypeMessage()
      .subscribe(
        (res: HttpResponse<Shorthand[]>) => {this.typeMessages=res.body},
        (res: HttpErrorResponse) => this.onError(res.message)
      );

    this.activatedRoute.data.subscribe(({ message }) => {
      this.updateForm(message);
    });
  }

  updateForm(message: IMessage) {
    this.editForm.patchValue({
      id: message.id,
      code: message.code,
      libelle: message.libelle,
      typeMessage: message.typeMessage.id
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const message = this.createFromForm();
    if (message.id !== undefined) {
      this.subscribeToSaveResponse(this.messageService.update(message));
    } else {
      this.subscribeToSaveResponse(this.messageService.create(message));
    }
  }

  private createFromForm(): IMessage {
    return {
      ...new Message(),
      id: this.editForm.get(['id']).value,
      code: this.editForm.get(['code']).value,
      libelle: this.editForm.get(['libelle']).value,
      typeMessage: this.editForm.get(['typeMessage']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMessage>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  trackTypeMessage(index: number, item: Shorthand) {
    return item.id;
  }

  protected onError(errorMessage: string) {
    
  }

}
