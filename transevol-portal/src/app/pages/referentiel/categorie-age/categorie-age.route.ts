import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CategorieAgeComponent } from './categorie-age.component';
import { CategorieAgeDetailComponent } from './categorie-age-detail.component';
import { CategorieAgeUpdateComponent } from './categorie-age-update.component';
import { CategorieAgeDeletePopupComponent } from './categorie-age-delete-dialog.component';
import { ResolvePagingParams } from '@app/_shared/util/resolve-paging-params.service';
import { ICategorieAge, CategorieAge } from '@app/_models/categorie-age.model';
import { Shorthand } from '@app/_models/_shared_models/shorthand.model';
import { CategorieAgeService } from '@app/_services';
import { UserRouteAccessService } from '@app/_services/user-route-access-service';
import { AuthGuard } from '@app/_helpers';

@Injectable({ providedIn: 'root' })
export class CategorieAgeResolve implements Resolve<ICategorieAge> {
  constructor(private service: CategorieAgeService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICategorieAge> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<CategorieAge>) => response.ok),
        map((message: HttpResponse<CategorieAge>) => message.body)
      );
    }
    
    return of(new CategorieAge(undefined,undefined,undefined,undefined,undefined,undefined,new Shorthand(),new Shorthand()));
  }
}

export const categorieAgeRoute: Routes = [
  {
    path: '',
    component: CategorieAgeComponent,
    resolve: {
      pagingParams: ResolvePagingParams
    },
    /*data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'Messages'
    },*/
    canActivate: [AuthGuard]
  },
  {
    path: ':id/view',
    component: CategorieAgeDetailComponent,
    resolve: {
      categorieAge: CategorieAgeResolve
    },
   /* data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Messages'
    },*/
    canActivate: [AuthGuard]
  },
  {
    path: 'new',
    component: CategorieAgeUpdateComponent,
    resolve: {
      categorieAge: CategorieAgeResolve
    },
    /*data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Messages'
    },*/
    canActivate: [AuthGuard]
  },
  {
    path: ':id/edit',
    component: CategorieAgeUpdateComponent,
    resolve: {
      categorieAge: CategorieAgeResolve
    },
    /*data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Messages'
    },*/
    canActivate: [AuthGuard]
  }
];

export const categorieAgePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: CategorieAgeDeletePopupComponent,
    resolve: {
      categorieAge: CategorieAgeResolve
    },
    /*data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Messages'
    },*/
    canActivate: [AuthGuard /*UserRouteAccessService*/],
    outlet: 'popup'
  }
];
