import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { CategorieAgeService, SocieteService, CodificationService } from '@app/_services';
import { ICategorieAge, CategorieAge } from '@app/_models/categorie-age.model';
import { Shorthand } from '@app/_models/_shared_models/shorthand.model';

@Component({
  selector: 'jhi-categorie-age-update',
  templateUrl: './categorie-age-update.component.html'
})
export class CategorieAgeUpdateComponent implements OnInit {
  isSaving: boolean;

  societes : Shorthand[];
  activitesMetier : Shorthand[];

  editForm = this.fb.group({
    id: [],
    libelle: [],
    description: [],
    minAge: [],
    maxAge: [],
    societe: [],
    activiteMetier: []
  });

  constructor(protected categorieAgeService: CategorieAgeService,
              protected activatedRoute: ActivatedRoute,
              private societeService: SocieteService,
              private codificationService: CodificationService,
              private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;

    this.societeService
      .queryShorthandSociete()
      .subscribe(
        (res: HttpResponse<Shorthand[]>) => {this.societes=res.body},
        (res: HttpErrorResponse) => this.onError(res.message)
      );

      this.codificationService
      .queryShorthandCodifications()
      .subscribe(
        (res: HttpResponse<Shorthand[]>) => {this.activitesMetier=res.body},
        (res: HttpErrorResponse) => this.onError(res.message)
      );

    this.activatedRoute.data.subscribe(({ categorieAge }) => {
      this.updateForm(categorieAge);
    });
  }

  updateForm(categorieAge: ICategorieAge) {
    this.editForm.patchValue({
      id: categorieAge.id,
      libelle: categorieAge.libelle,
      description: categorieAge.description,
      minAge: categorieAge.minAge,
      maxAge: categorieAge.maxAge,
      societe: categorieAge.societe.id,
      activiteMetier: categorieAge.activiteMetier.id
      
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const categorieAge = this.createFromForm();
    if (categorieAge.id !== undefined) {
      this.subscribeToSaveResponse(this.categorieAgeService.update(categorieAge));
    } else {
      this.subscribeToSaveResponse(this.categorieAgeService.create(categorieAge));
    }
  }

  private createFromForm(): ICategorieAge {
    return {
      ...new CategorieAge(),
      id: this.editForm.get(['id']).value,
      libelle: this.editForm.get(['libelle']).value,
      description: this.editForm.get(['description']).value,
      minAge: this.editForm.get(['minAge']).value,
      maxAge: this.editForm.get(['maxAge']).value,
      societe: this.editForm.get(['societe']).value,
      activiteMetier: this.editForm.get(['activiteMetier']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICategorieAge>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  trackShorthand(index: number, item: Shorthand) {
    return item.id;
  }
  

  protected onError(errorMessage: string) {
    
  }

}
