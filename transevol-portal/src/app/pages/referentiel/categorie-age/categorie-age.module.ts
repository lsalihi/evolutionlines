import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';


import { CategorieAgeComponent } from './categorie-age.component';
import { CategorieAgeDetailComponent } from './categorie-age-detail.component';
import { CategorieAgeUpdateComponent } from './categorie-age-update.component';
import { CategorieAgeDeletePopupComponent, CategorieAgeDeleteDialogComponent } from './categorie-age-delete-dialog.component';
import { categorieAgeRoute, categorieAgePopupRoute } from './categorie-age.route';
import { LandlinesSharedLibsModule } from '@app/_shared/shared-libs.module';
import { LandlinesSharedModule } from '@app/_shared/shared.module';
import { CategorieAgeService } from '@app/_services';

const ENTITY_STATES = [...categorieAgeRoute, ...categorieAgePopupRoute];

@NgModule({
  imports: [LandlinesSharedLibsModule,
    LandlinesSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [CategorieAgeComponent, CategorieAgeDetailComponent, CategorieAgeUpdateComponent, CategorieAgeDeleteDialogComponent, CategorieAgeDeletePopupComponent],
  entryComponents: [CategorieAgeDeleteDialogComponent],
  providers : [CategorieAgeService]
})
export class LandlinesCategorieAgeModule { }
