import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ICategorieAge } from '@app/_models/categorie-age.model';

@Component({
  selector: 'jhi-categorie-age-detail',
  templateUrl: './categorie-age-detail.component.html'
})
export class CategorieAgeDetailComponent implements OnInit {
  categorieAge: ICategorieAge;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ categorieAge }) => {
      this.categorieAge = categorieAge;
    });
  }

  previousState() {
    window.history.back();
  }
}
