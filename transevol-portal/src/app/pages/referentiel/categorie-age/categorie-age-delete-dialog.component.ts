import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ICategorieAge } from '@app/_models/categorie-age.model';
import { CategorieAgeService } from '@app/_services';

@Component({
  selector: 'jhi-categorie-age-delete-dialog',
  templateUrl: './categorie-age-delete-dialog.component.html'
})
export class CategorieAgeDeleteDialogComponent {
  categorieAge: ICategorieAge;

  constructor(protected categorieAgeService: CategorieAgeService, public activeModal: NgbActiveModal) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.categorieAgeService.delete(id).subscribe(response => {
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-categorieAge-delete-popup',
  template: ''
})
export class CategorieAgeDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ categorieAge }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(CategorieAgeDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.categorieAge = categorieAge;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/categorieAge', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/categorieAge', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
