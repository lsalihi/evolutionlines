import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { DeviseComponent } from './devise.component';
import { DeviseDetailComponent } from './devise-detail.component';
import { DeviseUpdateComponent } from './devise-update.component';
import { DeviseDeletePopupComponent } from './devise-delete-dialog.component';
import { ResolvePagingParams } from '@app/_shared/util/resolve-paging-params.service';
import { IDevise, Devise } from '@app/_models/devise.model';
import { DeviseService } from '@app/_services';
import { UserRouteAccessService } from '@app/_services/user-route-access-service';
import { AuthGuard } from '@app/_helpers';

@Injectable({ providedIn: 'root' })
export class DeviseResolve implements Resolve<IDevise> {
  constructor(private service: DeviseService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IDevise> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Devise>) => response.ok),
        map((devise: HttpResponse<Devise>) => devise.body)
      );
    }
    return of(new Devise());
  }
}

export const deviseRoute: Routes = [
  {
    path: '',
    component: DeviseComponent,
    resolve: {
      pagingParams: ResolvePagingParams
    },
    /*data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'Devises'
    },*/
    canActivate: [AuthGuard]
  },
  {
    path: ':id/view',
    component: DeviseDetailComponent,
    resolve: {
      devise: DeviseResolve
    },
   /* data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Devises'
    },*/
    canActivate: [AuthGuard]
  },
  {
    path: 'new',
    component: DeviseUpdateComponent,
    resolve: {
      devise: DeviseResolve
    },
    /*data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Devises'
    },*/
    canActivate: [AuthGuard]
  },
  {
    path: ':id/edit',
    component: DeviseUpdateComponent,
    resolve: {
      devise: DeviseResolve
    },
    /*data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Devises'
    },*/
    canActivate: [AuthGuard]
  }
];

export const devisePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: DeviseDeletePopupComponent,
    resolve: {
      devise: DeviseResolve
    },
    /*data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Devises'
    },*/
    canActivate: [AuthGuard /*UserRouteAccessService*/],
    outlet: 'popup'
  }
];
