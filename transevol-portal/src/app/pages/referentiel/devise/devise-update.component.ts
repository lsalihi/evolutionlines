import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { DeviseService } from '@app/_services';
import { IDevise, Devise } from '@app/_models/devise.model';

@Component({
  selector: 'jhi-devise-update',
  templateUrl: './devise-update.component.html'
})
export class DeviseUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    abreviationCourte: [],
    abreviationLongue: [],
    code: [],
    libelle: [],
    nombreApresVirgule: [],
    actif: []
  });

  constructor(protected deviseService: DeviseService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ devise }) => {
      this.updateForm(devise);
    });
  }

  updateForm(devise: IDevise) {
    this.editForm.patchValue({
      id: devise.id,
      abreviationCourte: devise.abreviationCourte,
      abreviationLongue: devise.abreviationLongue,
      code: devise.code,
      libelle: devise.libelle,
      nombreApresVirgule: devise.nombreApresVirgule,
      actif: devise.actif
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const devise = this.createFromForm();
    if (devise.id !== undefined) {
      this.subscribeToSaveResponse(this.deviseService.update(devise));
    } else {
      this.subscribeToSaveResponse(this.deviseService.create(devise));
    }
  }

  private createFromForm(): IDevise {
    return {
      ...new Devise(),
      id: this.editForm.get(['id']).value,
      abreviationCourte: this.editForm.get(['abreviationCourte']).value,
      abreviationLongue: this.editForm.get(['abreviationLongue']).value,
      code: this.editForm.get(['code']).value,
      libelle: this.editForm.get(['libelle']).value,
      nombreApresVirgule: this.editForm.get(['nombreApresVirgule']).value,
      actif: this.editForm.get(['actif']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDevise>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
