import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { IDevise } from '@app/_models/devise.model';
import { DeviseService } from '@app/_services';

@Component({
  selector: 'jhi-devise-delete-dialog',
  templateUrl: './devise-delete-dialog.component.html'
})
export class DeviseDeleteDialogComponent {
  devise: IDevise;

  constructor(protected deviseService: DeviseService, public activeModal: NgbActiveModal) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.deviseService.delete(id).subscribe(response => {
      /*this.eventManager.broadcast({
        name: 'deviseListModification',
        content: 'Deleted an devise'
      });*/
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-devise-delete-popup',
  template: ''
})
export class DeviseDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ devise }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(DeviseDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.devise = devise;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/devise', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/devise', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
