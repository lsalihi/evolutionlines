import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CodificationComponent } from './codification.component';
import { CodificationDetailComponent } from './codification-detail.component';
import { CodificationUpdateComponent } from './codification-update.component';
import { CodificationDeletePopupComponent } from './codification-delete-dialog.component';
import { ICodification, Codification } from '@app/_models/codification.model';
import { ResolvePagingParams } from '@app/_shared/util/resolve-paging-params.service';
import { AuthGuard } from '@app/_helpers';
import { CodificationService } from '@app/_services/codification.service';


@Injectable({ providedIn: 'root' })
export class CodificationResolve implements Resolve<ICodification> {
  constructor(private service: CodificationService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICodification> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Codification>) => response.ok),
        map((codification: HttpResponse<Codification>) => codification.body)
      );
    }
    return of(new Codification());
  }
}

export const codificationRoute: Routes = [
  {
    path: '',
    component: CodificationComponent,
    resolve: {
      pagingParams: ResolvePagingParams
    },
    /*data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'Codifications'
    },*/
    canActivate: [AuthGuard]
  },
  {
    path: ':id/view',
    component: CodificationDetailComponent,
    resolve: {
      codification: CodificationResolve
    },
   /* data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Codifications'
    },*/
    canActivate: [AuthGuard]
  },
  {
    path: 'new',
    component: CodificationUpdateComponent,
    resolve: {
      codification: CodificationResolve
    },
    /*data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Codifications'
    },*/
    canActivate: [AuthGuard]
  },
  {
    path: ':id/edit',
    component: CodificationUpdateComponent,
    resolve: {
      codification: CodificationResolve
    },
    /*data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Codifications'
    },*/
    canActivate: [AuthGuard]
  }
];

export const codificationPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: CodificationDeletePopupComponent,
    resolve: {
      codification: CodificationResolve
    },
    /*data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Codifications'
    },*/
    canActivate: [AuthGuard],
    outlet: 'popup'
  }
];
