import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';


import { DeviseComponent } from './devise.component';
import { DeviseDetailComponent } from './devise-detail.component';
import { DeviseUpdateComponent } from './devise-update.component';
import { DeviseDeletePopupComponent, DeviseDeleteDialogComponent } from './devise-delete-dialog.component';
import { deviseRoute, devisePopupRoute } from './devise.route';
import { LandlinesSharedLibsModule } from '@app/_shared/shared-libs.module';
import { LandlinesSharedModule } from '@app/_shared/shared.module';
import { DeviseService } from '@app/_services';

const ENTITY_STATES = [...deviseRoute, ...devisePopupRoute];

@NgModule({
  imports: [LandlinesSharedLibsModule,
    LandlinesSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [DeviseComponent, DeviseDetailComponent, DeviseUpdateComponent, DeviseDeleteDialogComponent, DeviseDeletePopupComponent],
  entryComponents: [DeviseDeleteDialogComponent],
  providers : [DeviseService]
})
export class LandlinesDeviseModule { }
