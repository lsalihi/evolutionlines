import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IDevise } from '@app/_models/devise.model';

@Component({
  selector: 'jhi-devise-detail',
  templateUrl: './devise-detail.component.html'
})
export class DeviseDetailComponent implements OnInit {
  devise: IDevise;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ devise }) => {
      this.devise = devise;
    });
  }

  previousState() {
    window.history.back();
  }
}
