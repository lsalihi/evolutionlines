import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { IDevise } from '@app/_models/devise.model';
import { DeviseService } from '@app/_services';
import { ITEMS_PER_PAGE } from '@app/_shared/constants/pagination.constants';
import { AccountService } from '@app/_services/account.service';
import { ParseLinks } from '@app/_shared/util/ParseLinks';


@Component({
  selector: 'jhi-devise',
  templateUrl: './devise.component.html'
})
export class DeviseComponent implements OnInit, OnDestroy {
  currentAccount: any;
  devises: IDevise[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;

  constructor(
    protected deviseService: DeviseService,
    protected parseLinks: ParseLinks,
    //protected accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
  }

  loadAll() {
    this.deviseService
      .query({
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IDevise[]>) => this.paginateDevises(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(['/devise'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    this.loadAll();
  }

  clear() {
    this.page = 0;
    this.router.navigate([
      '/devise',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
   /* this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    */
    this.registerChangeInDevises();
  }

  ngOnDestroy() {
    //this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IDevise) {
    return item.id;
  }

  registerChangeInDevises() {
    //this.eventSubscriber = this.eventManager.subscribe('deviseListModification', response => this.loadAll());
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateDevises(data: IDevise[], headers: HttpHeaders) {
    this.devises = data;
    //this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = 10;// parseInt(headers.get('X-Total-Count'), 10);
    console.log(data);
  }

  protected onError(errorMessage: string) {
    //this.jhiAlertService.error(errorMessage, null, null);
  }
}
