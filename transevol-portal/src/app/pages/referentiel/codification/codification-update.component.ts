import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ICodification, Codification } from '@app/_models/codification.model';
import { CodificationService } from '@app/_services/codification.service';

@Component({
  selector: 'jhi-codification-update',
  templateUrl: './codification-update.component.html'
})
export class CodificationUpdateComponent implements OnInit {
  isSaving: boolean;

  societes: any[];

  editForm = this.fb.group({
    id: [],
    codeTable: [],
    codeCodification: [],
    codeLibelle: [],
    codeActif: [],
    societeId: []
  });

  constructor(
    protected codificationService: CodificationService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ codification }) => {
      this.updateForm(codification);
    });
   }

  updateForm(codification: ICodification) {
    this.editForm.patchValue({
      id: codification.id,
      codeTable: codification.codeTable,
      codeCodification: codification.codeCodification,
      codeLibelle: codification.codeLibelle,
      codeActif: codification.codeActif,
      societeId: codification.societeId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const codification = this.createFromForm();

    if (codification.id !== undefined) {
      this.subscribeToSaveResponse(this.codificationService.update(codification));
    } else {
      this.subscribeToSaveResponse(this.codificationService.create(codification));
    }
  }

  private createFromForm(): ICodification {
    return {
      ...new Codification(),
      id: this.editForm.get(['id']).value,
      codeTable: this.editForm.get(['codeTable']).value,
      codeCodification: this.editForm.get(['codeCodification']).value,
      codeLibelle: this.editForm.get(['codeLibelle']).value,
      codeActif: this.editForm.get(['codeActif']).value,
      societeId: this.editForm.get(['societeId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICodification>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    //this.jhiAlertService.error(errorMessage, null, null);
  }

  trackSocieteById(index: number, item: any) {
    return item.id;
  }
}
