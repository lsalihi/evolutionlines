import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ICodification } from '@app/_models/codification.model';

@Component({
  selector: 'jhi-codification-detail',
  templateUrl: './codification-detail.component.html'
})
export class CodificationDetailComponent implements OnInit {
  codification: ICodification;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ codification }) => {
      this.codification = codification;
    });
  }

  previousState() {
    window.history.back();
  }
}
