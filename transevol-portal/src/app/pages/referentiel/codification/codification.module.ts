import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';


import { CodificationComponent } from './codification.component';
import { CodificationDetailComponent } from './codification-detail.component';
import { CodificationUpdateComponent } from './codification-update.component';
import { CodificationDeletePopupComponent, CodificationDeleteDialogComponent } from './codification-delete-dialog.component';
import { codificationRoute, codificationPopupRoute } from './codification.route';
import { LandlinesSharedLibsModule } from '@app/_shared/shared-libs.module';
import { LandlinesSharedModule } from '@app/_shared/shared.module';
import { CodificationService } from '@app/_services/codification.service';

const ENTITY_STATES = [...codificationRoute, ...codificationPopupRoute];

@NgModule({
  imports: [LandlinesSharedLibsModule,
    LandlinesSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [CodificationComponent, CodificationDetailComponent, 
    CodificationUpdateComponent, CodificationDeleteDialogComponent,
    CodificationDeletePopupComponent],
  entryComponents: [CodificationDeleteDialogComponent],
  providers : [CodificationService]
})
export class LandlinesCodificationModule {}
