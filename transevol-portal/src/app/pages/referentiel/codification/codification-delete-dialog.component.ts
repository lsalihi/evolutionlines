import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ICodification } from '@app/_models/codification.model';
import { CodificationService } from '@app/_services/codification.service';

@Component({
  selector: 'jhi-codification-delete-dialog',
  templateUrl: './codification-delete-dialog.component.html'
})
export class CodificationDeleteDialogComponent {
  codification: ICodification;

  constructor(
    protected codificationService: CodificationService,
    public activeModal: NgbActiveModal
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.codificationService.delete(id).subscribe(response => {
      /*this.eventManager.broadcast({
        name: 'codificationListModification',
        content: 'Deleted an codification'
      });*/
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-codification-delete-popup',
  template: ''
})
export class CodificationDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ codification }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(CodificationDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.codification = codification;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/codification', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/codification', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
