import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { SocieteComponent } from './societe.component';
import { SocieteDetailComponent } from './societe-detail.component';
import { SocieteUpdateComponent } from './societe-update.component';
import { SocieteDeletePopupComponent } from './societe-delete-dialog.component';
import { ResolvePagingParams } from '@app/_shared/util/resolve-paging-params.service';
import { ISociete, Societe } from '@app/_models/societe.model';
import { Shorthand } from '@app/_models/_shared_models/shorthand.model';
import { SocieteService } from '@app/_services';
import { UserRouteAccessService } from '@app/_services/user-route-access-service';
import { AuthGuard } from '@app/_helpers';

@Injectable({ providedIn: 'root' })
export class SocieteResolve implements Resolve<ISociete> {
  constructor(private service: SocieteService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ISociete> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Societe>) => response.ok),
        map((message: HttpResponse<Societe>) => message.body)
      );
    }

    return of(new Societe(undefined,undefined,undefined,new Shorthand()));
  }
}

export const societeRoute: Routes = [
  {
    path: '',
    component: SocieteComponent,
    resolve: {
      pagingParams: ResolvePagingParams
    },
    /*data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'Messages'
    },*/
    canActivate: [AuthGuard]
  },
  {
    path: ':id/view',
    component: SocieteDetailComponent,
    resolve: {
      societe: SocieteResolve
    },
   /* data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Messages'
    },*/
    canActivate: [AuthGuard]
  },
  {
    path: 'new',
    component: SocieteUpdateComponent,
    resolve: {
      societe: SocieteResolve
    },
    /*data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Messages'
    },*/
    canActivate: [AuthGuard]
  },
  {
    path: ':id/edit',
    component: SocieteUpdateComponent,
    resolve: {
      societe: SocieteResolve
    },
    /*data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Messages'
    },*/
    canActivate: [AuthGuard]
  }
];

export const societePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: SocieteDeletePopupComponent,
    resolve: {
      societe: SocieteResolve
    },
    /*data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Messages'
    },*/
    canActivate: [AuthGuard /*UserRouteAccessService*/],
    outlet: 'popup'
  }
];
