import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';


import { SocieteComponent } from './societe.component';
import { SocieteDetailComponent } from './societe-detail.component';
import { SocieteUpdateComponent } from './societe-update.component';
import { SocieteDeletePopupComponent, SocieteDeleteDialogComponent } from './societe-delete-dialog.component';
import { societeRoute, societePopupRoute } from './societe.route';
import { LandlinesSharedLibsModule } from '@app/_shared/shared-libs.module';
import { LandlinesSharedModule } from '@app/_shared/shared.module';
import { SocieteService } from '@app/_services';

const ENTITY_STATES = [...societeRoute, ...societePopupRoute];

@NgModule({
  imports: [LandlinesSharedLibsModule,
    LandlinesSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [SocieteComponent, SocieteDetailComponent, SocieteUpdateComponent, SocieteDeleteDialogComponent, SocieteDeletePopupComponent],
  entryComponents: [SocieteDeleteDialogComponent],
  providers : [SocieteService]
})
export class LandlinesSocieteModule { }
