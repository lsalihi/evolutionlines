import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { SocieteService, PaysService } from '@app/_services';
import { ISociete, Societe } from '@app/_models/societe.model';
import { Shorthand } from '@app/_models/_shared_models/shorthand.model';

@Component({
  selector: 'jhi-societe-update',
  templateUrl: './societe-update.component.html'
})
export class SocieteUpdateComponent implements OnInit {
  isSaving: boolean;

  paysList : Shorthand[];
  

  editForm = this.fb.group({
    id: [],
    libelle: [],
    actif: [],
    pays: []
  });

  constructor(protected societeService: SocieteService,
              protected activatedRoute: ActivatedRoute,
              private paysService: PaysService,
              private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;

    this.paysService
      .queryShorthandPays()
      .subscribe(
        (res: HttpResponse<Shorthand[]>) => {this.paysList=res.body},
        (res: HttpErrorResponse) => this.onError(res.message)
      );


    this.activatedRoute.data.subscribe(({ societe }) => {
      this.updateForm(societe);
    });
  }

  updateForm(societe: ISociete) {
    this.editForm.patchValue({
      id: societe.id,
      libelle: societe.libelle,
      actif: societe.actif,
      pays: societe.pays.id
      
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const societe = this.createFromForm();

    if (societe.id !== undefined) {
      this.subscribeToSaveResponse(this.societeService.update(societe));
    } else {
      this.subscribeToSaveResponse(this.societeService.create(societe));
    }
  }

  private createFromForm(): ISociete {
    return {
      ...new Societe(),
      id: this.editForm.get(['id']).value,
      libelle: this.editForm.get(['libelle']).value,
      actif: this.editForm.get(['actif']).value,
      pays: this.editForm.get(['pays']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISociete>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  trackPays(index: number, item: Shorthand) {
    return item.id;
  }
  

  protected onError(errorMessage: string) {
    
  }

}
