import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { BanqueService, SocieteService } from '@app/_services';
import { IBanque, Banque } from '@app/_models/banque.model';

@Component({
  selector: 'jhi-banque-update',
  templateUrl: './banque-update.component.html'
})
export class BanqueUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    libelle: [],
    abreviation: [],
    description: [],
    actif: []
  });

  constructor(protected banqueService: BanqueService,
              protected activatedRoute: ActivatedRoute,
              private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;


    this.activatedRoute.data.subscribe(({ banque }) => {
      this.updateForm(banque);
    });
  }

  updateForm(banque: IBanque) {
    this.editForm.patchValue({
      id: banque.id,
      libelle: banque.libelle,
      abreviation: banque.abreviation,
      description: banque.description,
      actif: banque.actif
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const banque = this.createFromForm();
    if (banque.id !== undefined) {
      this.subscribeToSaveResponse(this.banqueService.update(banque));
    } else {
      this.subscribeToSaveResponse(this.banqueService.create(banque));
    }
  }

  private createFromForm(): IBanque {
    return {
      ...new Banque(),
      id: this.editForm.get(['id']).value,
      libelle: this.editForm.get(['libelle']).value,
      abreviation: this.editForm.get(['abreviation']).value,
      description: this.editForm.get(['description']).value,
      actif: this.editForm.get(['actif']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBanque>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  protected onError(errorMessage: string) {
    
  }

}
