import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';


import { BanqueComponent } from './banque.component';
import { BanqueDetailComponent } from './banque-detail.component';
import { BanqueUpdateComponent } from './banque-update.component';
import { BanqueDeletePopupComponent, BanqueDeleteDialogComponent } from './banque-delete-dialog.component';
import { banqueRoute, banquePopupRoute } from './banque.route';
import { LandlinesSharedLibsModule } from '@app/_shared/shared-libs.module';
import { LandlinesSharedModule } from '@app/_shared/shared.module';
import { BanqueService } from '@app/_services';

const ENTITY_STATES = [...banqueRoute, ...banquePopupRoute];

@NgModule({
  imports: [LandlinesSharedLibsModule,
    LandlinesSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [BanqueComponent, BanqueDetailComponent, BanqueUpdateComponent, BanqueDeleteDialogComponent, BanqueDeletePopupComponent],
  entryComponents: [BanqueDeleteDialogComponent],
  providers : [BanqueService]
})
export class LandlinesBanqueModule { }
