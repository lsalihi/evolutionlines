import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { BanqueComponent } from './banque.component';
import { BanqueDetailComponent } from './banque-detail.component';
import { BanqueUpdateComponent } from './banque-update.component';
import { BanqueDeletePopupComponent } from './banque-delete-dialog.component';
import { ResolvePagingParams } from '@app/_shared/util/resolve-paging-params.service';
import { IBanque, Banque } from '@app/_models/banque.model';
import { BanqueService } from '@app/_services';
import { UserRouteAccessService } from '@app/_services/user-route-access-service';
import { AuthGuard } from '@app/_helpers';

@Injectable({ providedIn: 'root' })
export class BanqueResolve implements Resolve<IBanque> {
  constructor(private service: BanqueService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IBanque> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Banque>) => response.ok),
        map((message: HttpResponse<Banque>) => message.body)
      );
    }
    return of(new Banque());
  }
}

export const banqueRoute: Routes = [
  {
    path: '',
    component: BanqueComponent,
    resolve: {
      pagingParams: ResolvePagingParams
    },
    /*data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'Messages'
    },*/
    canActivate: [AuthGuard]
  },
  {
    path: ':id/view',
    component: BanqueDetailComponent,
    resolve: {
      banque: BanqueResolve
    },
   /* data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Messages'
    },*/
    canActivate: [AuthGuard]
  },
  {
    path: 'new',
    component: BanqueUpdateComponent,
    resolve: {
      banque: BanqueResolve
    },
    /*data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Messages'
    },*/
    canActivate: [AuthGuard]
  },
  {
    path: ':id/edit',
    component: BanqueUpdateComponent,
    resolve: {
      banque: BanqueResolve
    },
    /*data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Messages'
    },*/
    canActivate: [AuthGuard]
  }
];

export const banquePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: BanqueDeletePopupComponent,
    resolve: {
      banque: BanqueResolve
    },
    /*data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Messages'
    },*/
    canActivate: [AuthGuard /*UserRouteAccessService*/],
    outlet: 'popup'
  }
];
