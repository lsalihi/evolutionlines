import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { IBanque } from '@app/_models/banque.model';
import { BanqueService } from '@app/_services';

@Component({
  selector: 'jhi-banque-delete-dialog',
  templateUrl: './banque-delete-dialog.component.html'
})
export class BanqueDeleteDialogComponent {
  banque: IBanque;

  constructor(protected banqueService: BanqueService, public activeModal: NgbActiveModal) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.banqueService.delete(id).subscribe(response => {
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-banque-delete-popup',
  template: ''
})
export class BanqueDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ banque }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(BanqueDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.banque = banque;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/banque', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/banque', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
