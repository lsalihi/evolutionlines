import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TypeMessage } from '@app/_models/type-message.model';

@Component({
  selector: 'jhi-type-message-detail',
  templateUrl: './type-message-detail.component.html'
})
export class TypeMessageDetailComponent implements OnInit {
  typeMessage: TypeMessage;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ typeMessage }) => {
      this.typeMessage = typeMessage;
    });
  }

  previousState() {
    window.history.back();
  }
}
