import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { TypeMessageService, CodificationService } from '@app/_services';
import { ITypeMessage, TypeMessage } from '@app/_models/type-message.model';
import { Shorthand } from '@app/_models/_shared_models/shorthand.model';

@Component({
  selector: 'jhi-type-message-update',
  templateUrl: './type-message-update.component.html'
})
export class TypeMessageUpdateComponent implements OnInit {
  isSaving: boolean;

  criticites: Shorthand[];

  editForm = this.fb.group({
    id: [],
    titre: [],
    description: [],
    criticite: [],
    type: [],
    fonction: []
  });

  constructor(protected typeMessageService: TypeMessageService,
              protected activatedRoute: ActivatedRoute,
              private codificationService: CodificationService,
              private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;

    this.codificationService
      .queryShorthandCodifications()
      .subscribe(
        (res: HttpResponse<Shorthand[]>) => {this.criticites=res.body},
        (res: HttpErrorResponse) => this.onError(res.message)
      );

    this.activatedRoute.data.subscribe(({ typeMessage }) => {
      this.updateForm(typeMessage);
    });
  }

  updateForm(typeMessage: ITypeMessage) {
    this.editForm.patchValue({
      id: typeMessage.id,
      titre: typeMessage.titre,
      description: typeMessage.description,
      criticite: typeMessage.criticite.id,
      type: typeMessage.type,
      fonction: typeMessage.codeFonction
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const typeMessage = this.createFromForm();
    if (typeMessage.id !== undefined) {
      this.subscribeToSaveResponse(this.typeMessageService.update(typeMessage));
    } else {
      this.subscribeToSaveResponse(this.typeMessageService.create(typeMessage));
    }
  }

  private createFromForm(): ITypeMessage {
    return {
      ...new TypeMessage(),
      id: this.editForm.get(['id']).value,
      titre: this.editForm.get(['titre']).value,
      description: this.editForm.get(['description']).value,
      criticite: this.editForm.get(['criticite']).value,
      type: this.editForm.get(['type']).value,
      codeFonction: this.editForm.get(['fonction']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITypeMessage>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  trackCriticite(index: number, item: Shorthand) {
    return item.id;
  }

  protected onError(errorMessage: string) {
    
  }

}
