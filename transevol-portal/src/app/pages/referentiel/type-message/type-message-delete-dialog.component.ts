import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TypeMessage } from '@app/_models/type-message.model';
import { TypeMessageService } from '@app/_services';

@Component({
  selector: 'jhi-type-message-delete-dialog',
  templateUrl: './type-message-delete-dialog.component.html'
})
export class TypeMessageDeleteDialogComponent {
  typeMessage: TypeMessage;

  constructor(protected typeMessageService: TypeMessageService, public activeModal: NgbActiveModal) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.typeMessageService.delete(id).subscribe(response => {
      /*this.eventManager.broadcast({
        name: 'deviseListModification',
        content: 'Deleted an devise'
      });*/
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-type-message-delete-popup',
  template: ''
})
export class TypeMessageDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ typeMessage }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(TypeMessageDeletePopupComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.typeMessage = typeMessage;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/typeMessage', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/typeMessage', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
