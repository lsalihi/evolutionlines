import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';


import { TypeMessageComponent } from './type-message.component';
import { TypeMessageDetailComponent } from './type-message-detail.component';
import { TypeMessageUpdateComponent } from './type-message-update.component';
import { TypeMessageDeletePopupComponent, TypeMessageDeleteDialogComponent } from './type-message-delete-dialog.component';
import { typeMessageRoute, typeMessagePopupRoute } from './type-message.route';
import { LandlinesSharedLibsModule } from '@app/_shared/shared-libs.module';
import { LandlinesSharedModule } from '@app/_shared/shared.module';
import { TypeMessageService } from '@app/_services';

const ENTITY_STATES = [...typeMessageRoute, ...typeMessagePopupRoute];

@NgModule({
  imports: [LandlinesSharedLibsModule,
    LandlinesSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [TypeMessageComponent, TypeMessageDetailComponent, TypeMessageUpdateComponent, TypeMessageDeletePopupComponent, TypeMessageDeleteDialogComponent],
  entryComponents: [TypeMessageDeleteDialogComponent],
  providers : [TypeMessageService]
})
export class LandlinesTypeMessageModule { }
