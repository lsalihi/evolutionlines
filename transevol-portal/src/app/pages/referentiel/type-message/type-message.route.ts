import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { TypeMessageComponent } from './type-message.component';
import { TypeMessageDetailComponent } from './type-message-detail.component';
import { TypeMessageUpdateComponent } from './type-message-update.component';
import { TypeMessageDeletePopupComponent } from './type-message-delete-dialog.component';
import { ResolvePagingParams } from '@app/_shared/util/resolve-paging-params.service';
import { ITypeMessage, TypeMessage } from '@app/_models/type-message.model';
import { Shorthand } from '@app/_models/_shared_models/shorthand.model';
import { TypeMessageService } from '@app/_services';
import { UserRouteAccessService } from '@app/_services/user-route-access-service';
import { AuthGuard } from '@app/_helpers';

@Injectable({ providedIn: 'root' })
export class TypeMessageResolve implements Resolve<ITypeMessage> {
  constructor(private service: TypeMessageService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ITypeMessage> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<TypeMessage>) => response.ok),
        map((typeMessage: HttpResponse<TypeMessage>) => typeMessage.body)
      );
    }

    return of(new TypeMessage(undefined,undefined,undefined,undefined,undefined,new Shorthand()));
  }
}

export const typeMessageRoute: Routes = [
  {
    path: '',
    component: TypeMessageComponent,
    resolve: {
      pagingParams: ResolvePagingParams
    },
    /*data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'TypeMessages'
    },*/
    canActivate: [AuthGuard]
  },
  {
    path: ':id/view',
    component: TypeMessageDetailComponent,
    resolve: {
      typeMessage: TypeMessageResolve
    },
   /* data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TypeMessages'
    },*/
    canActivate: [AuthGuard]
  },
  {
    path: 'new',
    component: TypeMessageUpdateComponent,
    resolve: {
      typeMessage: TypeMessageResolve
    },
    /*data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TypeMessages'
    },*/
    canActivate: [AuthGuard]
  },
  {
    path: ':id/edit',
    component: TypeMessageUpdateComponent,
    resolve: {
      typeMessage: TypeMessageResolve
    },
    /*data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TypeMessages'
    },*/
    canActivate: [AuthGuard]
  }
];

export const typeMessagePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: TypeMessageDeletePopupComponent,
    resolve: {
      typeMessage: TypeMessageResolve
    },
    /*data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TypeMessages'
    },*/
    canActivate: [AuthGuard /*UserRouteAccessService*/],
    outlet: 'popup'
  }
];
