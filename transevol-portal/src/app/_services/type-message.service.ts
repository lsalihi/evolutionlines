import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ITypeMessage} from '@app/_models/type-message.model';
import { Shorthand } from '@app/_models/_shared_models/shorthand.model';
import { createRequestOption } from '@app/_shared/util/request-util';
import { environment } from '@environments/environment';

type EntityResponseType = HttpResponse<ITypeMessage>;
type EntityArrayResponseType = HttpResponse<ITypeMessage[]>;
type ShorthandEntityArrayResponseType = HttpResponse<Shorthand[]>;

@Injectable({ providedIn: 'root' })
export class TypeMessageService {
  public resourceUrl = `${environment.apiUrl}api/typesMessage`;
  public shorthandResourceUrl = `${environment.apiUrl}api/typesMessageShrt`;

  constructor(protected http: HttpClient) {}

  create(typeMessage: ITypeMessage): Observable<EntityResponseType> {
    return this.http.post<ITypeMessage>(this.resourceUrl, typeMessage, { observe: 'response' });
  }

  update(typeMessage: ITypeMessage): Observable<EntityResponseType> {
    return this.http.put<ITypeMessage>(this.resourceUrl, typeMessage, { observe: 'response' });
  }


  find(id: string): Observable<EntityResponseType> {
    return this.http.get<ITypeMessage>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  queryShorthandTypeMessage(): Observable<ShorthandEntityArrayResponseType> {
    return this.http.get<Shorthand[]>(this.shorthandResourceUrl, { observe: 'response' });
  }

  queryByTypeTypeMessage(type: string): Observable<EntityArrayResponseType> {
    return this.http.get<ITypeMessage[]>(`${this.resourceUrl}/search?type=${type}`, { observe: 'response' });
  }


  findByCodeFn(codeFn: string): Observable<EntityArrayResponseType> {
    return this.http.get<ITypeMessage[]>(`${this.resourceUrl}/search/codeFunction=${codeFn}`, { observe: 'response' });
  }


  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITypeMessage[]>(this.resourceUrl, { params: options, observe: 'response' });
  }
  
  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
