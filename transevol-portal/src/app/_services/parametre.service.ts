import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ParametreGlobal } from '@app/_models/parametreGlobal';
import { environment } from '@environments/environment';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ParametreService {

  private parametreGlobalSubject: BehaviorSubject<ParametreGlobal>;
  public parametreGlobal: Observable<ParametreGlobal>;

  constructor(private http: HttpClient) { }

  public get parametreGlobalValue(): ParametreGlobal {
    return this.parametreGlobalSubject.value;
  }

  getParametreGlobal() {
    return this.http.get<ParametreGlobal>(`${environment.apiUrl}/parametres/globalsParams`);
  }
}
