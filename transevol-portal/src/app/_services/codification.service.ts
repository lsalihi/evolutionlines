import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ICodification } from '@app/_models/codification.model';
import { Shorthand } from '@app/_models/_shared_models/shorthand.model';
import { SERVER_API_URL } from '@app/app.constants';
import { createRequestOption } from '@app/_shared/util/request-util';


type EntityResponseType = HttpResponse<ICodification>;
type EntityArrayResponseType = HttpResponse<ICodification[]>;
type ShorthandEntityArrayResponseType = HttpResponse<Shorthand[]>;

@Injectable({ providedIn: 'root' })
export class CodificationService {
  public resourceUrl = SERVER_API_URL + 'api/codifications';
  public shorthandResourceUrl = SERVER_API_URL + 'api/codificationsShrt';

  constructor(protected http: HttpClient) {}

  create(codification: ICodification): Observable<EntityResponseType> {
    return this.http.post<ICodification>(this.resourceUrl, codification, { observe: 'response' });
  }

  update(codification: ICodification): Observable<EntityResponseType> {
    return this.http.put<ICodification>(this.resourceUrl, codification, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICodification>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICodification[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  findByCode(codeTable: string): Observable<EntityArrayResponseType> {
    return this.http.get<ICodification[]>(`${this.resourceUrl}/codes/${codeTable}`, { observe: 'response' });
  }

  queryShorthandCodifications(): Observable<ShorthandEntityArrayResponseType> {
    return this.http.get<Shorthand[]>(this.shorthandResourceUrl, { observe: 'response' });
  }

  
}
