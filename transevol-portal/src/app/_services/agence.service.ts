import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from '@app/_shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from '@app/app.constants';
import { createRequestOption } from '@app/_shared/util/request-util';
import { IAgence } from '@app/_models/agence.model';

type EntityResponseType = HttpResponse<IAgence>;
type EntityArrayResponseType = HttpResponse<IAgence[]>;

@Injectable({ providedIn: 'root' })
export class AgenceService {
  public resourceUrl = SERVER_API_URL + 'api/agences';

  constructor(protected http: HttpClient) {}

  create(agence: IAgence): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(agence);
    return this.http
      .post<IAgence>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(agence: IAgence): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(agence);
    return this.http
      .put<IAgence>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IAgence>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

   query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IAgence[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(agence: IAgence): IAgence {
    const copy: IAgence = Object.assign({}, agence, {
      dateCreation: agence.dateCreation != null && agence.dateCreation.isValid() ? agence.dateCreation.format(DATE_FORMAT) : null,
      debutService: agence.debutService != null && agence.debutService.isValid() ? agence.debutService.format(DATE_FORMAT) : null,
      finService: agence.finService != null && agence.finService.isValid() ? agence.finService.format(DATE_FORMAT) : null,
      dateOuverture: agence.dateOuverture != null && agence.dateOuverture.isValid() ? agence.dateOuverture.format(DATE_FORMAT) : null,
      dateMiseEnProd: agence.dateMiseEnProd != null && agence.dateMiseEnProd.isValid() ? agence.dateMiseEnProd.format(DATE_FORMAT) : null,
      dateDesactivation:
        agence.dateDesactivation != null && agence.dateDesactivation.isValid() ? agence.dateDesactivation.format(DATE_FORMAT) : null,
      dateFermeture: agence.dateFermeture != null && agence.dateFermeture.isValid() ? agence.dateFermeture.format(DATE_FORMAT) : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateCreation = res.body.dateCreation != null ? moment(res.body.dateCreation) : null;
      res.body.debutService = res.body.debutService != null ? moment(res.body.debutService) : null;
      res.body.finService = res.body.finService != null ? moment(res.body.finService) : null;
      res.body.dateOuverture = res.body.dateOuverture != null ? moment(res.body.dateOuverture) : null;
      res.body.dateMiseEnProd = res.body.dateMiseEnProd != null ? moment(res.body.dateMiseEnProd) : null;
      res.body.dateDesactivation = res.body.dateDesactivation != null ? moment(res.body.dateDesactivation) : null;
      res.body.dateFermeture = res.body.dateFermeture != null ? moment(res.body.dateFermeture) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((agence: IAgence) => {
        agence.dateCreation = agence.dateCreation != null ? moment(agence.dateCreation) : null;
        agence.debutService = agence.debutService != null ? moment(agence.debutService) : null;
        agence.finService = agence.finService != null ? moment(agence.finService) : null;
        agence.dateOuverture = agence.dateOuverture != null ? moment(agence.dateOuverture) : null;
        agence.dateMiseEnProd = agence.dateMiseEnProd != null ? moment(agence.dateMiseEnProd) : null;
        agence.dateDesactivation = agence.dateDesactivation != null ? moment(agence.dateDesactivation) : null;
        agence.dateFermeture = agence.dateFermeture != null ? moment(agence.dateFermeture) : null;
      });
    }
    return res;
  }
}
