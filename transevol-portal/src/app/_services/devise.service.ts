import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IDevise } from '@app/_models/devise.model';
import { createRequestOption } from '@app/_shared/util/request-util';
import { environment } from '@environments/environment';

type EntityResponseType = HttpResponse<IDevise>;
type EntityArrayResponseType = HttpResponse<IDevise[]>;

@Injectable({ providedIn: 'root' })
export class DeviseService {
  public resourceUrl = `${environment.apiUrl}api/devises`;// SERVER_API_URL + 'api/devises';

  constructor(protected http: HttpClient) {}

  create(devise: IDevise): Observable<EntityResponseType> {
    return this.http.post<IDevise>(this.resourceUrl, devise, { observe: 'response' });
  }

  update(devise: IDevise): Observable<EntityResponseType> {
    return this.http.put<IDevise>(this.resourceUrl, devise, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDevise>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDevise[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
