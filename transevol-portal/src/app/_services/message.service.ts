import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IMessage } from '@app/_models/message.model';
import { createRequestOption } from '@app/_shared/util/request-util';
import { environment } from '@environments/environment';

type EntityResponseType = HttpResponse<IMessage>;
type EntityArrayResponseType = HttpResponse<IMessage[]>;

@Injectable({ providedIn: 'root' })
export class MessageService {
  public resourceUrl = `${environment.apiUrl}api/messages`;
  

  constructor(protected http: HttpClient) {}

  create(message: IMessage): Observable<EntityResponseType> {
    return this.http.post<IMessage>(this.resourceUrl, message, { observe: 'response' });
  }

  update(message: IMessage): Observable<EntityResponseType> {
    return this.http.put<IMessage>(this.resourceUrl, message, { observe: 'response' });
  }


  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMessage>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  findByCode(code: string): Observable<EntityResponseType> {
    return this.http.get<IMessage>(`${this.resourceUrl}/code/${code}`, { observe: 'response' });
  }

  queryByTypeMessage(id: number): Observable<EntityArrayResponseType> {
    return this.http.get<IMessage[]>(`${this.resourceUrl}/typeMessage/${id}`, { observe: 'response' });
  }


  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMessage[]>(this.resourceUrl, { params: options, observe: 'response' });
  }
  

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
