import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from '@app/app.constants';
import { createRequestOption } from '@app/_shared/util/request-util';
import { IPointVenteConfig } from '@app/_models/point-vente-config.model';

type EntityResponseType = HttpResponse<IPointVenteConfig>;
type EntityArrayResponseType = HttpResponse<IPointVenteConfig[]>;

@Injectable({ providedIn: 'root' })
export class PointVenteConfigService {
  public resourceUrl = SERVER_API_URL + 'api/point-vente-configs';

  constructor(protected http: HttpClient) {}

  create(pointVenteConfig: IPointVenteConfig): Observable<EntityResponseType> {
    return this.http.post<IPointVenteConfig>(this.resourceUrl, pointVenteConfig, { observe: 'response' });
  }

  update(pointVenteConfig: IPointVenteConfig): Observable<EntityResponseType> {
    return this.http.put<IPointVenteConfig>(this.resourceUrl, pointVenteConfig, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPointVenteConfig>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPointVenteConfig[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
