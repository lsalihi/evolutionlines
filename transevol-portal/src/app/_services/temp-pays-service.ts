import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Shorthand } from '@app/_models/_shared_models/shorthand.model';
import { createRequestOption } from '@app/_shared/util/request-util';
import { environment } from '@environments/environment';

type ShorthandEntityArrayResponseType = HttpResponse<Shorthand[]>;

@Injectable({ providedIn: 'root' })
export class PaysService {
  public shorthandResourceUrl = `${environment.apiUrl}api/paysShrt`;

  constructor(protected http: HttpClient) {}

  queryShorthandPays(): Observable<ShorthandEntityArrayResponseType> {
    return this.http.get<Shorthand[]>(this.shorthandResourceUrl, { observe: 'response' });
  }

}