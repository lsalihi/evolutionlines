import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SERVER_API_URL } from '@app/app.constants';
import { createRequestOption } from '@app/_shared/util/request-util';
import { ICategorieAge } from '@app/_models/categorie-age.model';
import { environment } from '@environments/environment';

type result = HttpResponse<ICategorieAge>;
type resultSet = HttpResponse<ICategorieAge[]>;

@Injectable({ providedIn: 'root' })
export class CategorieAgeService {
	public resourceUrl = `${environment.apiUrl}api/categoriesAge`;

	constructor(protected http: HttpClient) {}


	query(req?: any): Observable<resultSet> {
		const options = createRequestOption(req);
		return this.http.get<ICategorieAge[]>(this.resourceUrl, {params : options, observe : 'response'})
	}

	create (banque: ICategorieAge): Observable<result> {
		return this.http.post<ICategorieAge>(this.resourceUrl, banque, {observe: 'response'});
	}

	update(banque: ICategorieAge): Observable<result> {
		return this.http.put<ICategorieAge>(this.resourceUrl, banque, {observe : 'response'});
	}

	delete(id: number): Observable<HttpResponse<any>> {
		return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe : 'response'});
	}

	find(id: number): Observable<result> {
		return this.http.get<ICategorieAge>(`${this.resourceUrl}/id/${id}` , {observe : 'response'})
	}

	findByLibelle(libelle: string): Observable<result> {
		return this.http.get<ICategorieAge>(`${this.resourceUrl}/libelle/${libelle}`, {observe : 'response'});
	}

	findByMaxMinAge(max: number , min: number): Observable<resultSet> {
		return this.http.get<ICategorieAge[]>(`${this.resourceUrl}/mx/${max}/mn/${min}`, {observe : 'response'});
	}

}
