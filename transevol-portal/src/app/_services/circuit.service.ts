import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SERVER_API_URL } from '@app/app.constants';
import { createRequestOption } from '@app/_shared/util/request-util';
import { ICircuit } from '@app/_models/circuit.model';

type EntityResponseType = HttpResponse<ICircuit>;
type EntityArrayResponseType = HttpResponse<ICircuit[]>;

@Injectable({ providedIn: 'root' })
export class CircuitService {
  public resourceUrl = SERVER_API_URL + 'api/circuits';

  constructor(protected http: HttpClient) {}

  create(circuit: ICircuit): Observable<EntityResponseType> {
    return this.http.post<ICircuit>(this.resourceUrl, circuit, { observe: 'response' });
  }

  update(circuit: ICircuit): Observable<EntityResponseType> {
    return this.http.put<ICircuit>(this.resourceUrl, circuit, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICircuit>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICircuit[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
