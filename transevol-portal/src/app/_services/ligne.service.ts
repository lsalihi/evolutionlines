import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';

import { environment } from '@environments/environment';
import { ILigne } from '@app/_models/ligne.model';
import { map } from 'rxjs/operators';
import { createRequestOption } from '@app/_shared/util/request-util';
import { DATE_FORMAT } from '@app/_shared/constants/input.constants';

type EntityResponseType = HttpResponse<ILigne>;
type EntityArrayResponseType = HttpResponse<ILigne[]>;

@Injectable({ providedIn: 'root' })
export class LigneService {

  public resourceUrl = `${environment.apiUrl}api/lignes`;

  constructor(protected http: HttpClient) {}

  create(ligne: ILigne): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(ligne);
    return this.http
      .post<ILigne>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(ligne: ILigne): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(ligne);
    return this.http
      .put<ILigne>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ILigne>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ILigne[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(ligne: ILigne): ILigne {
    const copy: ILigne = Object.assign({}, ligne, {
      dateCreation: ligne.dateCreation != null && ligne.dateCreation.isValid() ? ligne.dateCreation.format(DATE_FORMAT) : null,
      dateSuspension: ligne.dateSuspension != null && ligne.dateSuspension.isValid() ? ligne.dateSuspension.format(DATE_FORMAT) : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateCreation = res.body.dateCreation != null ? moment(res.body.dateCreation) : null;
      res.body.dateSuspension = res.body.dateSuspension != null ? moment(res.body.dateSuspension) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((ligne: ILigne) => {
        ligne.dateCreation = ligne.dateCreation != null ? moment(ligne.dateCreation) : null;
        ligne.dateSuspension = ligne.dateSuspension != null ? moment(ligne.dateSuspension) : null;
      });
    }
    return res;
  }
}
