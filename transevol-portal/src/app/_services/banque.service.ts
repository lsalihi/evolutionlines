import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SERVER_API_URL } from '@app/app.constants';
import { createRequestOption } from '@app/_shared/util/request-util';
import { IBanque } from '@app/_models/banque.model';
import { environment } from '@environments/environment';

type result = HttpResponse<IBanque>;
type resultSet = HttpResponse<IBanque[]>;

@Injectable({ providedIn: 'root' })
export class BanqueService {
	public resourceUrl = `${environment.apiUrl}api/banques`;

	constructor(protected http: HttpClient) {}


	query(req?: any): Observable<resultSet> {
		const options = createRequestOption(req);
		return this.http.get<IBanque[]>(this.resourceUrl, {params : options, observe : 'response'})
	}

	create (banque: IBanque): Observable<result> {
		return this.http.post<IBanque>(this.resourceUrl, banque, {observe: 'response'});
	}

	update(banque: IBanque): Observable<result> {
		return this.http.put<IBanque>(this.resourceUrl, banque, {observe : 'response'});
	}

	delete(id: number): Observable<HttpResponse<any>> {
		return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe : 'response'});
	}

	find(id: number): Observable<result> {
		return this.http.get<IBanque>(`${this.resourceUrl}/id/${id}` , {observe : 'response'})
	}

	findByLibelle(libelle: string): Observable<result> {
		return this.http.get<IBanque>(`${this.resourceUrl}/libelle/${libelle}`, {observe : 'response'});
	}

	findByState(state: boolean): Observable<resultSet> {
		return this.http.get<IBanque[]>(`${this.resourceUrl}/state/${state}`, {observe : 'response'});
	}


}
