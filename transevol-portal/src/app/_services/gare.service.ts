import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SERVER_API_URL } from '@app/app.constants';
import { IGare } from '@app/_models/gare.model';
import { createRequestOption } from '@app/_shared/util/request-util';
import { environment } from '@environments/environment';


type EntityResponseType = HttpResponse<IGare>;
type EntityArrayResponseType = HttpResponse<IGare[]>;

@Injectable({ providedIn: 'root' })
export class GareService {
  public resourceUrl = `${environment.apiUrl}api/gares`;

  constructor(protected http: HttpClient) {}

  create(gare: IGare): Observable<EntityResponseType> {
    return this.http.post<IGare>(this.resourceUrl, gare, { observe: 'response' });
  }

  update(gare: IGare): Observable<EntityResponseType> {
    return this.http.put<IGare>(this.resourceUrl, gare, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IGare>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IGare[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
