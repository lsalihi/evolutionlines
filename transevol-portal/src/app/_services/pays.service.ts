import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from '@app/_shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from '@app/app.constants';
import { createRequestOption } from '@app/_shared/util/request-util';
import { IPays } from '@app/_models/pays.model';

type EntityResponseType = HttpResponse<IPays>;
type EntityArrayResponseType = HttpResponse<IPays[]>;

@Injectable({ providedIn: 'root' })
export class PaysService {
  public resourceUrl = SERVER_API_URL + 'api/pays';

  constructor(protected http: HttpClient) {}

  create(pays: IPays): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(pays);
    return this.http
      .post<IPays>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(pays: IPays): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(pays);
    return this.http
      .put<IPays>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IPays>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IPays[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(pays: IPays): IPays {
    const copy: IPays = Object.assign({}, pays, {
      dateDebutHeureEte:
        pays.dateDebutHeureEte != null && pays.dateDebutHeureEte.isValid() ? pays.dateDebutHeureEte.format(DATE_FORMAT) : null,
      dateFinHeureEte: pays.dateFinHeureEte != null && pays.dateFinHeureEte.isValid() ? pays.dateFinHeureEte.format(DATE_FORMAT) : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateDebutHeureEte = res.body.dateDebutHeureEte != null ? moment(res.body.dateDebutHeureEte) : null;
      res.body.dateFinHeureEte = res.body.dateFinHeureEte != null ? moment(res.body.dateFinHeureEte) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((pays: IPays) => {
        pays.dateDebutHeureEte = pays.dateDebutHeureEte != null ? moment(pays.dateDebutHeureEte) : null;
        pays.dateFinHeureEte = pays.dateFinHeureEte != null ? moment(pays.dateFinHeureEte) : null;
      });
    }
    return res;
  }
}
