﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// used to create fake backend
import { fakeBackendProvider } from './_helpers';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing-routing';

import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { HomeModule } from './home/home.module';
import { LoginModule } from './login/login.module';
import { NavbarComponent } from './layouts/navbar/navbar.component'
    ;
import { FooterComponent } from './layouts/footer/footer.component'
import { LandlinesSharedLibsModule } from './_shared/shared-libs.module';
//import { MaterialModule } from './_shared/material.module';
@NgModule({
    imports: [
        LandlinesSharedLibsModule,
        //MaterialModule,
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        HomeModule,
        LoginModule
    ],
    exports: [LandlinesSharedLibsModule, NavbarComponent],
    declarations: [
        AppComponent,
        NavbarComponent,
        FooterComponent],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        // provider used to create fake backend
        fakeBackendProvider
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }