import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '@app/_services';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['navbar.scss']
})
export class NavbarComponent implements OnInit {

  inProduction: boolean = false;
  isNavbarCollapsed: boolean;
  languages: any[];
  swaggerEnabled: boolean = false;
  version: string;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    this.version = '';
    this.isNavbarCollapsed = true;
  }

  ngOnInit() {

  }

  collapseNavbar() {
    this.isNavbarCollapsed = true;
  }

  isAuthenticated() {
    return true;
  }

  login() {
  }

   toggleNavbar() {
    this.isNavbarCollapsed = !this.isNavbarCollapsed;
  }

  getImageUrl() {
    return null;
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
}
}
