﻿import { Routes, RouterModule } from '@angular/router';

import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { navbarRoute } from './layouts/navbar/navbar.route';
const LAYOUT_ROUTES = [navbarRoute];

const routes: Routes = [
    {
      path : '',
      loadChildren: () => import('@app/home/home.module').then(m => m.HomeModule)
    },
    {
        path : 'login**',
        loadChildren: () => import('@app/login/login.module').then(m => m.LoginModule)

    },
    {
      path: 'codification',
      loadChildren: () => import('@app/pages/referentiel/codification/codification.module').then(m => m.LandlinesCodificationModule)
    },
    {
      path: 'devise',
      loadChildren: () => import('@app/pages/referentiel/devise/devise.module').then(m => m.LandlinesDeviseModule)
    },
    {
      path: 'ligne',
      loadChildren: () => import('@app/pages/transport/ligne/ligne.module').then(m => m.LandlinesLigneModule)
    },

    {
      path: 'message',
      loadChildren: () => import('@app/pages/referentiel/message/message.module').then(m => m.LandlinesMessageModule)
    },
      {
          path: 'agence',
          loadChildren: () => import('@app/pages/transport/agence/agence.module').then(m => m.LandlinesAgenceModule)
        },

         {
      path: 'pays',
      loadChildren: () => import('@app/pages/referentiel/pays/pays.module').then(m => m.LandlinesPaysModule)
    },

    ...LAYOUT_ROUTES
  ];
  
  @NgModule({
    imports: [
      RouterModule.forRoot(routes, {initialNavigation: 'enabled'}),
      CommonModule,
      ReactiveFormsModule
    ],
    exports: [
      RouterModule,
      ReactiveFormsModule
    ],
    providers: []
  })
  export class AppRoutingModule { }