import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@app/_helpers';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent, canActivate: [AuthGuard]
  }  
]; 

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports : [RouterModule]
})
export class HomeRoutingModule { }
