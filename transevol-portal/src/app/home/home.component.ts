﻿import { Component } from '@angular/core';
import { first } from 'rxjs/operators';

import { User } from '@app/_models';
import { UserService, AuthenticationService } from '@app/_services';
import { Router } from '@angular/router';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent {
    loading = false;

    currentUser: User;

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) {
        //load habilitations
        this.loading = true;
        this.authenticationService.currentUser.subscribe(x => {
        this.currentUser = x;
            this.loading = false;
        });
        
        if(!this.currentUser || !this.currentUser.parametreGlobal){
            this.authenticationService.logout();
        }
    }


}