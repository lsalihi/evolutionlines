package com.transport.landlines.repository;

import com.transport.landlines.domain.ModeleMail;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ModeleMail entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ModeleMailRepository extends JpaRepository<ModeleMail, Long> {
}
