package com.transport.landlines.repository;

import com.transport.landlines.domain.HoraireService;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the HoraireService entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HoraireServiceRepository extends JpaRepository<HoraireService, Long> {
}
