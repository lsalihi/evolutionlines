package com.transport.landlines.repository;

import com.transport.landlines.domain.KmInterVille;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the KmInterVille entity.
 */
@SuppressWarnings("unused")
@Repository
public interface KmInterVilleRepository extends JpaRepository<KmInterVille, Long> {
}
