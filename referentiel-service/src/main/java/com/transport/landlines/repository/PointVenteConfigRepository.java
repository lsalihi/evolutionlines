package com.transport.landlines.repository;

import com.transport.landlines.domain.PointVenteConfig;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the PointVenteConfig entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PointVenteConfigRepository extends JpaRepository<PointVenteConfig, Long> {
}
