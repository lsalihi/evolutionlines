package com.transport.landlines.repository;

import com.transport.landlines.domain.CategorieAge;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CategorieAge entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CategorieAgeRepository extends JpaRepository<CategorieAge, Long> {
}
