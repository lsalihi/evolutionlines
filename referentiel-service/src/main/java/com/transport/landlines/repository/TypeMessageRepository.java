package com.transport.landlines.repository;

import com.transport.landlines.domain.TypeMessage;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the TypeMessage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TypeMessageRepository extends JpaRepository<TypeMessage, Long> {
}
