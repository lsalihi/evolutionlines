package com.transport.landlines.repository;

import com.transport.landlines.domain.ModeReglement;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ModeReglement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ModeReglementRepository extends JpaRepository<ModeReglement, Long> {
}
