package com.transport.landlines.repository;

import com.transport.landlines.domain.Gare;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Gare entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GareRepository extends JpaRepository<Gare, Long> {
}
