package com.transport.landlines.repository;

import com.transport.landlines.domain.Codification;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Codification entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CodificationRepository extends JpaRepository<Codification, Long> {
}
