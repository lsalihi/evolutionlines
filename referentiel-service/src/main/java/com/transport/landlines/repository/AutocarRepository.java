package com.transport.landlines.repository;

import com.transport.landlines.domain.Autocar;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Autocar entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AutocarRepository extends JpaRepository<Autocar, Long> {
}
