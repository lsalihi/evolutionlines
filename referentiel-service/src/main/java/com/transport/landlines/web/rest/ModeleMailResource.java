package com.transport.landlines.web.rest;

import com.transport.landlines.service.ModeleMailService;
import com.transport.landlines.web.rest.errors.BadRequestAlertException;
import com.transport.landlines.service.dto.ModeleMailDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.transport.landlines.domain.ModeleMail}.
 */
@RestController
@RequestMapping("/api")
public class ModeleMailResource {

    private final Logger log = LoggerFactory.getLogger(ModeleMailResource.class);

    private static final String ENTITY_NAME = "referentielModeleMail";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ModeleMailService modeleMailService;

    public ModeleMailResource(ModeleMailService modeleMailService) {
        this.modeleMailService = modeleMailService;
    }

    /**
     * {@code POST  /modele-mails} : Create a new modeleMail.
     *
     * @param modeleMailDTO the modeleMailDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new modeleMailDTO, or with status {@code 400 (Bad Request)} if the modeleMail has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/modele-mails")
    public ResponseEntity<ModeleMailDTO> createModeleMail(@RequestBody ModeleMailDTO modeleMailDTO) throws URISyntaxException {
        log.debug("REST request to save ModeleMail : {}", modeleMailDTO);
        if (modeleMailDTO.getId() != null) {
            throw new BadRequestAlertException("A new modeleMail cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ModeleMailDTO result = modeleMailService.save(modeleMailDTO);
        return ResponseEntity.created(new URI("/api/modele-mails/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /modele-mails} : Updates an existing modeleMail.
     *
     * @param modeleMailDTO the modeleMailDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated modeleMailDTO,
     * or with status {@code 400 (Bad Request)} if the modeleMailDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the modeleMailDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/modele-mails")
    public ResponseEntity<ModeleMailDTO> updateModeleMail(@RequestBody ModeleMailDTO modeleMailDTO) throws URISyntaxException {
        log.debug("REST request to update ModeleMail : {}", modeleMailDTO);
        if (modeleMailDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ModeleMailDTO result = modeleMailService.save(modeleMailDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, modeleMailDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /modele-mails} : get all the modeleMails.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of modeleMails in body.
     */
    @GetMapping("/modele-mails")
    public ResponseEntity<List<ModeleMailDTO>> getAllModeleMails(Pageable pageable) {
        log.debug("REST request to get a page of ModeleMails");
        Page<ModeleMailDTO> page = modeleMailService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /modele-mails/:id} : get the "id" modeleMail.
     *
     * @param id the id of the modeleMailDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the modeleMailDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/modele-mails/{id}")
    public ResponseEntity<ModeleMailDTO> getModeleMail(@PathVariable Long id) {
        log.debug("REST request to get ModeleMail : {}", id);
        Optional<ModeleMailDTO> modeleMailDTO = modeleMailService.findOne(id);
        return ResponseUtil.wrapOrNotFound(modeleMailDTO);
    }

    /**
     * {@code DELETE  /modele-mails/:id} : delete the "id" modeleMail.
     *
     * @param id the id of the modeleMailDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/modele-mails/{id}")
    public ResponseEntity<Void> deleteModeleMail(@PathVariable Long id) {
        log.debug("REST request to delete ModeleMail : {}", id);
        modeleMailService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
