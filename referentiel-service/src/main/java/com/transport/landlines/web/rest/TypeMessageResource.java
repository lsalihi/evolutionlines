package com.transport.landlines.web.rest;

import com.transport.landlines.service.TypeMessageService;
import com.transport.landlines.web.rest.errors.BadRequestAlertException;
import com.transport.landlines.service.dto.TypeMessageDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.transport.landlines.domain.TypeMessage}.
 */
@RestController
@RequestMapping("/api")
public class TypeMessageResource {

    private final Logger log = LoggerFactory.getLogger(TypeMessageResource.class);

    private static final String ENTITY_NAME = "referentielTypeMessage";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TypeMessageService typeMessageService;

    public TypeMessageResource(TypeMessageService typeMessageService) {
        this.typeMessageService = typeMessageService;
    }

    /**
     * {@code POST  /type-messages} : Create a new typeMessage.
     *
     * @param typeMessageDTO the typeMessageDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new typeMessageDTO, or with status {@code 400 (Bad Request)} if the typeMessage has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/type-messages")
    public ResponseEntity<TypeMessageDTO> createTypeMessage(@RequestBody TypeMessageDTO typeMessageDTO) throws URISyntaxException {
        log.debug("REST request to save TypeMessage : {}", typeMessageDTO);
        if (typeMessageDTO.getId() != null) {
            throw new BadRequestAlertException("A new typeMessage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TypeMessageDTO result = typeMessageService.save(typeMessageDTO);
        return ResponseEntity.created(new URI("/api/type-messages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /type-messages} : Updates an existing typeMessage.
     *
     * @param typeMessageDTO the typeMessageDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated typeMessageDTO,
     * or with status {@code 400 (Bad Request)} if the typeMessageDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the typeMessageDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/type-messages")
    public ResponseEntity<TypeMessageDTO> updateTypeMessage(@RequestBody TypeMessageDTO typeMessageDTO) throws URISyntaxException {
        log.debug("REST request to update TypeMessage : {}", typeMessageDTO);
        if (typeMessageDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TypeMessageDTO result = typeMessageService.save(typeMessageDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, typeMessageDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /type-messages} : get all the typeMessages.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of typeMessages in body.
     */
    @GetMapping("/type-messages")
    public ResponseEntity<List<TypeMessageDTO>> getAllTypeMessages(Pageable pageable) {
        log.debug("REST request to get a page of TypeMessages");
        Page<TypeMessageDTO> page = typeMessageService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /type-messages/:id} : get the "id" typeMessage.
     *
     * @param id the id of the typeMessageDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the typeMessageDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/type-messages/{id}")
    public ResponseEntity<TypeMessageDTO> getTypeMessage(@PathVariable Long id) {
        log.debug("REST request to get TypeMessage : {}", id);
        Optional<TypeMessageDTO> typeMessageDTO = typeMessageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(typeMessageDTO);
    }

    /**
     * {@code DELETE  /type-messages/:id} : delete the "id" typeMessage.
     *
     * @param id the id of the typeMessageDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/type-messages/{id}")
    public ResponseEntity<Void> deleteTypeMessage(@PathVariable Long id) {
        log.debug("REST request to delete TypeMessage : {}", id);
        typeMessageService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
