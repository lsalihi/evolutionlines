package com.transport.landlines.web.rest;

import com.transport.landlines.service.PointVenteConfigService;
import com.transport.landlines.web.rest.errors.BadRequestAlertException;
import com.transport.landlines.service.dto.PointVenteConfigDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.transport.landlines.domain.PointVenteConfig}.
 */
@RestController
@RequestMapping("/api")
public class PointVenteConfigResource {

    private final Logger log = LoggerFactory.getLogger(PointVenteConfigResource.class);

    private static final String ENTITY_NAME = "referentielPointVenteConfig";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PointVenteConfigService pointVenteConfigService;

    public PointVenteConfigResource(PointVenteConfigService pointVenteConfigService) {
        this.pointVenteConfigService = pointVenteConfigService;
    }

    /**
     * {@code POST  /point-vente-configs} : Create a new pointVenteConfig.
     *
     * @param pointVenteConfigDTO the pointVenteConfigDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new pointVenteConfigDTO, or with status {@code 400 (Bad Request)} if the pointVenteConfig has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/point-vente-configs")
    public ResponseEntity<PointVenteConfigDTO> createPointVenteConfig(@RequestBody PointVenteConfigDTO pointVenteConfigDTO) throws URISyntaxException {
        log.debug("REST request to save PointVenteConfig : {}", pointVenteConfigDTO);
        if (pointVenteConfigDTO.getId() != null) {
            throw new BadRequestAlertException("A new pointVenteConfig cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PointVenteConfigDTO result = pointVenteConfigService.save(pointVenteConfigDTO);
        return ResponseEntity.created(new URI("/api/point-vente-configs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /point-vente-configs} : Updates an existing pointVenteConfig.
     *
     * @param pointVenteConfigDTO the pointVenteConfigDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated pointVenteConfigDTO,
     * or with status {@code 400 (Bad Request)} if the pointVenteConfigDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the pointVenteConfigDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/point-vente-configs")
    public ResponseEntity<PointVenteConfigDTO> updatePointVenteConfig(@RequestBody PointVenteConfigDTO pointVenteConfigDTO) throws URISyntaxException {
        log.debug("REST request to update PointVenteConfig : {}", pointVenteConfigDTO);
        if (pointVenteConfigDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PointVenteConfigDTO result = pointVenteConfigService.save(pointVenteConfigDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, pointVenteConfigDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /point-vente-configs} : get all the pointVenteConfigs.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of pointVenteConfigs in body.
     */
    @GetMapping("/point-vente-configs")
    public ResponseEntity<List<PointVenteConfigDTO>> getAllPointVenteConfigs(Pageable pageable) {
        log.debug("REST request to get a page of PointVenteConfigs");
        Page<PointVenteConfigDTO> page = pointVenteConfigService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /point-vente-configs/:id} : get the "id" pointVenteConfig.
     *
     * @param id the id of the pointVenteConfigDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the pointVenteConfigDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/point-vente-configs/{id}")
    public ResponseEntity<PointVenteConfigDTO> getPointVenteConfig(@PathVariable Long id) {
        log.debug("REST request to get PointVenteConfig : {}", id);
        Optional<PointVenteConfigDTO> pointVenteConfigDTO = pointVenteConfigService.findOne(id);
        return ResponseUtil.wrapOrNotFound(pointVenteConfigDTO);
    }

    /**
     * {@code DELETE  /point-vente-configs/:id} : delete the "id" pointVenteConfig.
     *
     * @param id the id of the pointVenteConfigDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/point-vente-configs/{id}")
    public ResponseEntity<Void> deletePointVenteConfig(@PathVariable Long id) {
        log.debug("REST request to delete PointVenteConfig : {}", id);
        pointVenteConfigService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
