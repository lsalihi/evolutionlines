package com.transport.landlines.web.rest;

import com.transport.landlines.service.ModeReglementService;
import com.transport.landlines.web.rest.errors.BadRequestAlertException;
import com.transport.landlines.service.dto.ModeReglementDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.transport.landlines.domain.ModeReglement}.
 */
@RestController
@RequestMapping("/api")
public class ModeReglementResource {

    private final Logger log = LoggerFactory.getLogger(ModeReglementResource.class);

    private static final String ENTITY_NAME = "referentielModeReglement";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ModeReglementService modeReglementService;

    public ModeReglementResource(ModeReglementService modeReglementService) {
        this.modeReglementService = modeReglementService;
    }

    /**
     * {@code POST  /mode-reglements} : Create a new modeReglement.
     *
     * @param modeReglementDTO the modeReglementDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new modeReglementDTO, or with status {@code 400 (Bad Request)} if the modeReglement has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/mode-reglements")
    public ResponseEntity<ModeReglementDTO> createModeReglement(@RequestBody ModeReglementDTO modeReglementDTO) throws URISyntaxException {
        log.debug("REST request to save ModeReglement : {}", modeReglementDTO);
        if (modeReglementDTO.getId() != null) {
            throw new BadRequestAlertException("A new modeReglement cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ModeReglementDTO result = modeReglementService.save(modeReglementDTO);
        return ResponseEntity.created(new URI("/api/mode-reglements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /mode-reglements} : Updates an existing modeReglement.
     *
     * @param modeReglementDTO the modeReglementDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated modeReglementDTO,
     * or with status {@code 400 (Bad Request)} if the modeReglementDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the modeReglementDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/mode-reglements")
    public ResponseEntity<ModeReglementDTO> updateModeReglement(@RequestBody ModeReglementDTO modeReglementDTO) throws URISyntaxException {
        log.debug("REST request to update ModeReglement : {}", modeReglementDTO);
        if (modeReglementDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ModeReglementDTO result = modeReglementService.save(modeReglementDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, modeReglementDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /mode-reglements} : get all the modeReglements.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of modeReglements in body.
     */
    @GetMapping("/mode-reglements")
    public ResponseEntity<List<ModeReglementDTO>> getAllModeReglements(Pageable pageable) {
        log.debug("REST request to get a page of ModeReglements");
        Page<ModeReglementDTO> page = modeReglementService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /mode-reglements/:id} : get the "id" modeReglement.
     *
     * @param id the id of the modeReglementDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the modeReglementDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/mode-reglements/{id}")
    public ResponseEntity<ModeReglementDTO> getModeReglement(@PathVariable Long id) {
        log.debug("REST request to get ModeReglement : {}", id);
        Optional<ModeReglementDTO> modeReglementDTO = modeReglementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(modeReglementDTO);
    }

    /**
     * {@code DELETE  /mode-reglements/:id} : delete the "id" modeReglement.
     *
     * @param id the id of the modeReglementDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/mode-reglements/{id}")
    public ResponseEntity<Void> deleteModeReglement(@PathVariable Long id) {
        log.debug("REST request to delete ModeReglement : {}", id);
        modeReglementService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
