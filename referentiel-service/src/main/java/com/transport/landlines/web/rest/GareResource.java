package com.transport.landlines.web.rest;

import com.transport.landlines.service.GareService;
import com.transport.landlines.web.rest.errors.BadRequestAlertException;
import com.transport.landlines.service.dto.GareDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.transport.landlines.domain.Gare}.
 */
@RestController
@RequestMapping("/api")
public class GareResource {

    private final Logger log = LoggerFactory.getLogger(GareResource.class);

    private static final String ENTITY_NAME = "referentielGare";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final GareService gareService;

    public GareResource(GareService gareService) {
        this.gareService = gareService;
    }

    /**
     * {@code POST  /gares} : Create a new gare.
     *
     * @param gareDTO the gareDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new gareDTO, or with status {@code 400 (Bad Request)} if the gare has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/gares")
    public ResponseEntity<GareDTO> createGare(@RequestBody GareDTO gareDTO) throws URISyntaxException {
        log.debug("REST request to save Gare : {}", gareDTO);
        if (gareDTO.getId() != null) {
            throw new BadRequestAlertException("A new gare cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GareDTO result = gareService.save(gareDTO);
        return ResponseEntity.created(new URI("/api/gares/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /gares} : Updates an existing gare.
     *
     * @param gareDTO the gareDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated gareDTO,
     * or with status {@code 400 (Bad Request)} if the gareDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the gareDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/gares")
    public ResponseEntity<GareDTO> updateGare(@RequestBody GareDTO gareDTO) throws URISyntaxException {
        log.debug("REST request to update Gare : {}", gareDTO);
        if (gareDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        GareDTO result = gareService.save(gareDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, gareDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /gares} : get all the gares.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of gares in body.
     */
    @GetMapping("/gares")
    public ResponseEntity<List<GareDTO>> getAllGares(Pageable pageable) {
        log.debug("REST request to get a page of Gares");
        Page<GareDTO> page = gareService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /gares/:id} : get the "id" gare.
     *
     * @param id the id of the gareDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the gareDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/gares/{id}")
    public ResponseEntity<GareDTO> getGare(@PathVariable Long id) {
        log.debug("REST request to get Gare : {}", id);
        Optional<GareDTO> gareDTO = gareService.findOne(id);
        return ResponseUtil.wrapOrNotFound(gareDTO);
    }

    /**
     * {@code DELETE  /gares/:id} : delete the "id" gare.
     *
     * @param id the id of the gareDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/gares/{id}")
    public ResponseEntity<Void> deleteGare(@PathVariable Long id) {
        log.debug("REST request to delete Gare : {}", id);
        gareService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
