package com.transport.landlines.web.rest;

import com.transport.landlines.service.KmInterVilleService;
import com.transport.landlines.web.rest.errors.BadRequestAlertException;
import com.transport.landlines.service.dto.KmInterVilleDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.transport.landlines.domain.KmInterVille}.
 */
@RestController
@RequestMapping("/api")
public class KmInterVilleResource {

    private final Logger log = LoggerFactory.getLogger(KmInterVilleResource.class);

    private static final String ENTITY_NAME = "referentielKmInterVille";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final KmInterVilleService kmInterVilleService;

    public KmInterVilleResource(KmInterVilleService kmInterVilleService) {
        this.kmInterVilleService = kmInterVilleService;
    }

    /**
     * {@code POST  /km-inter-villes} : Create a new kmInterVille.
     *
     * @param kmInterVilleDTO the kmInterVilleDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new kmInterVilleDTO, or with status {@code 400 (Bad Request)} if the kmInterVille has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/km-inter-villes")
    public ResponseEntity<KmInterVilleDTO> createKmInterVille(@RequestBody KmInterVilleDTO kmInterVilleDTO) throws URISyntaxException {
        log.debug("REST request to save KmInterVille : {}", kmInterVilleDTO);
        if (kmInterVilleDTO.getId() != null) {
            throw new BadRequestAlertException("A new kmInterVille cannot already have an ID", ENTITY_NAME, "idexists");
        }
        KmInterVilleDTO result = kmInterVilleService.save(kmInterVilleDTO);
        return ResponseEntity.created(new URI("/api/km-inter-villes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /km-inter-villes} : Updates an existing kmInterVille.
     *
     * @param kmInterVilleDTO the kmInterVilleDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated kmInterVilleDTO,
     * or with status {@code 400 (Bad Request)} if the kmInterVilleDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the kmInterVilleDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/km-inter-villes")
    public ResponseEntity<KmInterVilleDTO> updateKmInterVille(@RequestBody KmInterVilleDTO kmInterVilleDTO) throws URISyntaxException {
        log.debug("REST request to update KmInterVille : {}", kmInterVilleDTO);
        if (kmInterVilleDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        KmInterVilleDTO result = kmInterVilleService.save(kmInterVilleDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, kmInterVilleDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /km-inter-villes} : get all the kmInterVilles.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of kmInterVilles in body.
     */
    @GetMapping("/km-inter-villes")
    public ResponseEntity<List<KmInterVilleDTO>> getAllKmInterVilles(Pageable pageable) {
        log.debug("REST request to get a page of KmInterVilles");
        Page<KmInterVilleDTO> page = kmInterVilleService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /km-inter-villes/:id} : get the "id" kmInterVille.
     *
     * @param id the id of the kmInterVilleDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the kmInterVilleDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/km-inter-villes/{id}")
    public ResponseEntity<KmInterVilleDTO> getKmInterVille(@PathVariable Long id) {
        log.debug("REST request to get KmInterVille : {}", id);
        Optional<KmInterVilleDTO> kmInterVilleDTO = kmInterVilleService.findOne(id);
        return ResponseUtil.wrapOrNotFound(kmInterVilleDTO);
    }

    /**
     * {@code DELETE  /km-inter-villes/:id} : delete the "id" kmInterVille.
     *
     * @param id the id of the kmInterVilleDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/km-inter-villes/{id}")
    public ResponseEntity<Void> deleteKmInterVille(@PathVariable Long id) {
        log.debug("REST request to delete KmInterVille : {}", id);
        kmInterVilleService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
