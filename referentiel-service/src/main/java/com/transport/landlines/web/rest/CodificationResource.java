package com.transport.landlines.web.rest;

import com.transport.landlines.service.CodificationService;
import com.transport.landlines.web.rest.errors.BadRequestAlertException;
import com.transport.landlines.service.dto.CodificationDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.transport.landlines.domain.Codification}.
 */
@RestController
@RequestMapping("/api")
public class CodificationResource {

    private final Logger log = LoggerFactory.getLogger(CodificationResource.class);

    private static final String ENTITY_NAME = "referentielCodification";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CodificationService codificationService;

    public CodificationResource(CodificationService codificationService) {
        this.codificationService = codificationService;
    }

    /**
     * {@code POST  /codifications} : Create a new codification.
     *
     * @param codificationDTO the codificationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new codificationDTO, or with status {@code 400 (Bad Request)} if the codification has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/codifications")
    public ResponseEntity<CodificationDTO> createCodification(@RequestBody CodificationDTO codificationDTO) throws URISyntaxException {
        log.debug("REST request to save Codification : {}", codificationDTO);
        if (codificationDTO.getId() != null) {
            throw new BadRequestAlertException("A new codification cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CodificationDTO result = codificationService.save(codificationDTO);
        return ResponseEntity.created(new URI("/api/codifications/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /codifications} : Updates an existing codification.
     *
     * @param codificationDTO the codificationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated codificationDTO,
     * or with status {@code 400 (Bad Request)} if the codificationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the codificationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/codifications")
    public ResponseEntity<CodificationDTO> updateCodification(@RequestBody CodificationDTO codificationDTO) throws URISyntaxException {
        log.debug("REST request to update Codification : {}", codificationDTO);
        if (codificationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CodificationDTO result = codificationService.save(codificationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, codificationDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /codifications} : get all the codifications.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of codifications in body.
     */
    @GetMapping("/codifications")
    public ResponseEntity<List<CodificationDTO>> getAllCodifications(Pageable pageable) {
        log.debug("REST request to get a page of Codifications");
        Page<CodificationDTO> page = codificationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /codifications/:id} : get the "id" codification.
     *
     * @param id the id of the codificationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the codificationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/codifications/{id}")
    public ResponseEntity<CodificationDTO> getCodification(@PathVariable Long id) {
        log.debug("REST request to get Codification : {}", id);
        Optional<CodificationDTO> codificationDTO = codificationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(codificationDTO);
    }

    /**
     * {@code DELETE  /codifications/:id} : delete the "id" codification.
     *
     * @param id the id of the codificationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/codifications/{id}")
    public ResponseEntity<Void> deleteCodification(@PathVariable Long id) {
        log.debug("REST request to delete Codification : {}", id);
        codificationService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
