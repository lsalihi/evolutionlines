package com.transport.landlines.web.rest;

import com.transport.landlines.service.AutocarService;
import com.transport.landlines.web.rest.errors.BadRequestAlertException;
import com.transport.landlines.service.dto.AutocarDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.transport.landlines.domain.Autocar}.
 */
@RestController
@RequestMapping("/api")
public class AutocarResource {

    private final Logger log = LoggerFactory.getLogger(AutocarResource.class);

    private static final String ENTITY_NAME = "referentielAutocar";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AutocarService autocarService;

    public AutocarResource(AutocarService autocarService) {
        this.autocarService = autocarService;
    }

    /**
     * {@code POST  /autocars} : Create a new autocar.
     *
     * @param autocarDTO the autocarDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new autocarDTO, or with status {@code 400 (Bad Request)} if the autocar has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/autocars")
    public ResponseEntity<AutocarDTO> createAutocar(@RequestBody AutocarDTO autocarDTO) throws URISyntaxException {
        log.debug("REST request to save Autocar : {}", autocarDTO);
        if (autocarDTO.getId() != null) {
            throw new BadRequestAlertException("A new autocar cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AutocarDTO result = autocarService.save(autocarDTO);
        return ResponseEntity.created(new URI("/api/autocars/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /autocars} : Updates an existing autocar.
     *
     * @param autocarDTO the autocarDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated autocarDTO,
     * or with status {@code 400 (Bad Request)} if the autocarDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the autocarDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/autocars")
    public ResponseEntity<AutocarDTO> updateAutocar(@RequestBody AutocarDTO autocarDTO) throws URISyntaxException {
        log.debug("REST request to update Autocar : {}", autocarDTO);
        if (autocarDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AutocarDTO result = autocarService.save(autocarDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, autocarDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /autocars} : get all the autocars.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of autocars in body.
     */
    @GetMapping("/autocars")
    public ResponseEntity<List<AutocarDTO>> getAllAutocars(Pageable pageable) {
        log.debug("REST request to get a page of Autocars");
        Page<AutocarDTO> page = autocarService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /autocars/:id} : get the "id" autocar.
     *
     * @param id the id of the autocarDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the autocarDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/autocars/{id}")
    public ResponseEntity<AutocarDTO> getAutocar(@PathVariable Long id) {
        log.debug("REST request to get Autocar : {}", id);
        Optional<AutocarDTO> autocarDTO = autocarService.findOne(id);
        return ResponseUtil.wrapOrNotFound(autocarDTO);
    }

    /**
     * {@code DELETE  /autocars/:id} : delete the "id" autocar.
     *
     * @param id the id of the autocarDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/autocars/{id}")
    public ResponseEntity<Void> deleteAutocar(@PathVariable Long id) {
        log.debug("REST request to delete Autocar : {}", id);
        autocarService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
