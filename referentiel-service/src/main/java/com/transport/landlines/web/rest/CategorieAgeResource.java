package com.transport.landlines.web.rest;

import com.transport.landlines.service.CategorieAgeService;
import com.transport.landlines.web.rest.errors.BadRequestAlertException;
import com.transport.landlines.service.dto.CategorieAgeDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.transport.landlines.domain.CategorieAge}.
 */
@RestController
@RequestMapping("/api")
public class CategorieAgeResource {

    private final Logger log = LoggerFactory.getLogger(CategorieAgeResource.class);

    private static final String ENTITY_NAME = "referentielCategorieAge";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CategorieAgeService categorieAgeService;

    public CategorieAgeResource(CategorieAgeService categorieAgeService) {
        this.categorieAgeService = categorieAgeService;
    }

    /**
     * {@code POST  /categorie-ages} : Create a new categorieAge.
     *
     * @param categorieAgeDTO the categorieAgeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new categorieAgeDTO, or with status {@code 400 (Bad Request)} if the categorieAge has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/categorie-ages")
    public ResponseEntity<CategorieAgeDTO> createCategorieAge(@RequestBody CategorieAgeDTO categorieAgeDTO) throws URISyntaxException {
        log.debug("REST request to save CategorieAge : {}", categorieAgeDTO);
        if (categorieAgeDTO.getId() != null) {
            throw new BadRequestAlertException("A new categorieAge cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CategorieAgeDTO result = categorieAgeService.save(categorieAgeDTO);
        return ResponseEntity.created(new URI("/api/categorie-ages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /categorie-ages} : Updates an existing categorieAge.
     *
     * @param categorieAgeDTO the categorieAgeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated categorieAgeDTO,
     * or with status {@code 400 (Bad Request)} if the categorieAgeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the categorieAgeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/categorie-ages")
    public ResponseEntity<CategorieAgeDTO> updateCategorieAge(@RequestBody CategorieAgeDTO categorieAgeDTO) throws URISyntaxException {
        log.debug("REST request to update CategorieAge : {}", categorieAgeDTO);
        if (categorieAgeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CategorieAgeDTO result = categorieAgeService.save(categorieAgeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, categorieAgeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /categorie-ages} : get all the categorieAges.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of categorieAges in body.
     */
    @GetMapping("/categorie-ages")
    public ResponseEntity<List<CategorieAgeDTO>> getAllCategorieAges(Pageable pageable) {
        log.debug("REST request to get a page of CategorieAges");
        Page<CategorieAgeDTO> page = categorieAgeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /categorie-ages/:id} : get the "id" categorieAge.
     *
     * @param id the id of the categorieAgeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the categorieAgeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/categorie-ages/{id}")
    public ResponseEntity<CategorieAgeDTO> getCategorieAge(@PathVariable Long id) {
        log.debug("REST request to get CategorieAge : {}", id);
        Optional<CategorieAgeDTO> categorieAgeDTO = categorieAgeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(categorieAgeDTO);
    }

    /**
     * {@code DELETE  /categorie-ages/:id} : delete the "id" categorieAge.
     *
     * @param id the id of the categorieAgeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/categorie-ages/{id}")
    public ResponseEntity<Void> deleteCategorieAge(@PathVariable Long id) {
        log.debug("REST request to delete CategorieAge : {}", id);
        categorieAgeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
