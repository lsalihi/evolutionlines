package com.transport.landlines.web.rest;

import com.transport.landlines.service.CompteBancaireService;
import com.transport.landlines.web.rest.errors.BadRequestAlertException;
import com.transport.landlines.service.dto.CompteBancaireDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.transport.landlines.domain.CompteBancaire}.
 */
@RestController
@RequestMapping("/api")
public class CompteBancaireResource {

    private final Logger log = LoggerFactory.getLogger(CompteBancaireResource.class);

    private static final String ENTITY_NAME = "referentielCompteBancaire";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CompteBancaireService compteBancaireService;

    public CompteBancaireResource(CompteBancaireService compteBancaireService) {
        this.compteBancaireService = compteBancaireService;
    }

    /**
     * {@code POST  /compte-bancaires} : Create a new compteBancaire.
     *
     * @param compteBancaireDTO the compteBancaireDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new compteBancaireDTO, or with status {@code 400 (Bad Request)} if the compteBancaire has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/compte-bancaires")
    public ResponseEntity<CompteBancaireDTO> createCompteBancaire(@RequestBody CompteBancaireDTO compteBancaireDTO) throws URISyntaxException {
        log.debug("REST request to save CompteBancaire : {}", compteBancaireDTO);
        if (compteBancaireDTO.getId() != null) {
            throw new BadRequestAlertException("A new compteBancaire cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CompteBancaireDTO result = compteBancaireService.save(compteBancaireDTO);
        return ResponseEntity.created(new URI("/api/compte-bancaires/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /compte-bancaires} : Updates an existing compteBancaire.
     *
     * @param compteBancaireDTO the compteBancaireDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated compteBancaireDTO,
     * or with status {@code 400 (Bad Request)} if the compteBancaireDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the compteBancaireDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/compte-bancaires")
    public ResponseEntity<CompteBancaireDTO> updateCompteBancaire(@RequestBody CompteBancaireDTO compteBancaireDTO) throws URISyntaxException {
        log.debug("REST request to update CompteBancaire : {}", compteBancaireDTO);
        if (compteBancaireDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CompteBancaireDTO result = compteBancaireService.save(compteBancaireDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, compteBancaireDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /compte-bancaires} : get all the compteBancaires.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of compteBancaires in body.
     */
    @GetMapping("/compte-bancaires")
    public ResponseEntity<List<CompteBancaireDTO>> getAllCompteBancaires(Pageable pageable) {
        log.debug("REST request to get a page of CompteBancaires");
        Page<CompteBancaireDTO> page = compteBancaireService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /compte-bancaires/:id} : get the "id" compteBancaire.
     *
     * @param id the id of the compteBancaireDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the compteBancaireDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/compte-bancaires/{id}")
    public ResponseEntity<CompteBancaireDTO> getCompteBancaire(@PathVariable Long id) {
        log.debug("REST request to get CompteBancaire : {}", id);
        Optional<CompteBancaireDTO> compteBancaireDTO = compteBancaireService.findOne(id);
        return ResponseUtil.wrapOrNotFound(compteBancaireDTO);
    }

    /**
     * {@code DELETE  /compte-bancaires/:id} : delete the "id" compteBancaire.
     *
     * @param id the id of the compteBancaireDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/compte-bancaires/{id}")
    public ResponseEntity<Void> deleteCompteBancaire(@PathVariable Long id) {
        log.debug("REST request to delete CompteBancaire : {}", id);
        compteBancaireService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
