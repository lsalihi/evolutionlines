package com.transport.landlines.web.rest;

import com.transport.landlines.service.HoraireServiceService;
import com.transport.landlines.web.rest.errors.BadRequestAlertException;
import com.transport.landlines.service.dto.HoraireServiceDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.transport.landlines.domain.HoraireService}.
 */
@RestController
@RequestMapping("/api")
public class HoraireServiceResource {

    private final Logger log = LoggerFactory.getLogger(HoraireServiceResource.class);

    private static final String ENTITY_NAME = "referentielHoraireService";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HoraireServiceService horaireServiceService;

    public HoraireServiceResource(HoraireServiceService horaireServiceService) {
        this.horaireServiceService = horaireServiceService;
    }

    /**
     * {@code POST  /horaire-services} : Create a new horaireService.
     *
     * @param horaireServiceDTO the horaireServiceDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new horaireServiceDTO, or with status {@code 400 (Bad Request)} if the horaireService has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/horaire-services")
    public ResponseEntity<HoraireServiceDTO> createHoraireService(@RequestBody HoraireServiceDTO horaireServiceDTO) throws URISyntaxException {
        log.debug("REST request to save HoraireService : {}", horaireServiceDTO);
        if (horaireServiceDTO.getId() != null) {
            throw new BadRequestAlertException("A new horaireService cannot already have an ID", ENTITY_NAME, "idexists");
        }
        HoraireServiceDTO result = horaireServiceService.save(horaireServiceDTO);
        return ResponseEntity.created(new URI("/api/horaire-services/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /horaire-services} : Updates an existing horaireService.
     *
     * @param horaireServiceDTO the horaireServiceDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated horaireServiceDTO,
     * or with status {@code 400 (Bad Request)} if the horaireServiceDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the horaireServiceDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/horaire-services")
    public ResponseEntity<HoraireServiceDTO> updateHoraireService(@RequestBody HoraireServiceDTO horaireServiceDTO) throws URISyntaxException {
        log.debug("REST request to update HoraireService : {}", horaireServiceDTO);
        if (horaireServiceDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        HoraireServiceDTO result = horaireServiceService.save(horaireServiceDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, horaireServiceDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /horaire-services} : get all the horaireServices.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of horaireServices in body.
     */
    @GetMapping("/horaire-services")
    public ResponseEntity<List<HoraireServiceDTO>> getAllHoraireServices(Pageable pageable) {
        log.debug("REST request to get a page of HoraireServices");
        Page<HoraireServiceDTO> page = horaireServiceService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /horaire-services/:id} : get the "id" horaireService.
     *
     * @param id the id of the horaireServiceDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the horaireServiceDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/horaire-services/{id}")
    public ResponseEntity<HoraireServiceDTO> getHoraireService(@PathVariable Long id) {
        log.debug("REST request to get HoraireService : {}", id);
        Optional<HoraireServiceDTO> horaireServiceDTO = horaireServiceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(horaireServiceDTO);
    }

    /**
     * {@code DELETE  /horaire-services/:id} : delete the "id" horaireService.
     *
     * @param id the id of the horaireServiceDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/horaire-services/{id}")
    public ResponseEntity<Void> deleteHoraireService(@PathVariable Long id) {
        log.debug("REST request to delete HoraireService : {}", id);
        horaireServiceService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
