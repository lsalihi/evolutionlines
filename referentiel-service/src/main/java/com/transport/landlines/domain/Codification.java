package com.transport.landlines.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Codification.
 */
@Entity
@Table(name = "codification")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Codification implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "code_table")
    private String codeTable;

    @Column(name = "code_codification")
    private String codeCodification;

    @Column(name = "code_libelle")
    private String codeLibelle;

    @Column(name = "code_actif")
    private Boolean codeActif;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodeTable() {
        return codeTable;
    }

    public Codification codeTable(String codeTable) {
        this.codeTable = codeTable;
        return this;
    }

    public void setCodeTable(String codeTable) {
        this.codeTable = codeTable;
    }

    public String getCodeCodification() {
        return codeCodification;
    }

    public Codification codeCodification(String codeCodification) {
        this.codeCodification = codeCodification;
        return this;
    }

    public void setCodeCodification(String codeCodification) {
        this.codeCodification = codeCodification;
    }

    public String getCodeLibelle() {
        return codeLibelle;
    }

    public Codification codeLibelle(String codeLibelle) {
        this.codeLibelle = codeLibelle;
        return this;
    }

    public void setCodeLibelle(String codeLibelle) {
        this.codeLibelle = codeLibelle;
    }

    public Boolean isCodeActif() {
        return codeActif;
    }

    public Codification codeActif(Boolean codeActif) {
        this.codeActif = codeActif;
        return this;
    }

    public void setCodeActif(Boolean codeActif) {
        this.codeActif = codeActif;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Codification)) {
            return false;
        }
        return id != null && id.equals(((Codification) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Codification{" +
            "id=" + getId() +
            ", codeTable='" + getCodeTable() + "'" +
            ", codeCodification='" + getCodeCodification() + "'" +
            ", codeLibelle='" + getCodeLibelle() + "'" +
            ", codeActif='" + isCodeActif() + "'" +
            "}";
    }
}
