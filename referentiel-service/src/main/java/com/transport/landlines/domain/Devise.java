package com.transport.landlines.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Devise.
 */
@Entity
@Table(name = "devise")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Devise implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "abreviation_courte")
    private String abreviationCourte;

    @Column(name = "abreviation_longue")
    private String abreviationLongue;

    @Column(name = "code")
    private String code;

    @Column(name = "libelle")
    private String libelle;

    @Column(name = "nombre_apres_virgule")
    private Integer nombreApresVirgule;

    @Column(name = "actif")
    private Boolean actif;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAbreviationCourte() {
        return abreviationCourte;
    }

    public Devise abreviationCourte(String abreviationCourte) {
        this.abreviationCourte = abreviationCourte;
        return this;
    }

    public void setAbreviationCourte(String abreviationCourte) {
        this.abreviationCourte = abreviationCourte;
    }

    public String getAbreviationLongue() {
        return abreviationLongue;
    }

    public Devise abreviationLongue(String abreviationLongue) {
        this.abreviationLongue = abreviationLongue;
        return this;
    }

    public void setAbreviationLongue(String abreviationLongue) {
        this.abreviationLongue = abreviationLongue;
    }

    public String getCode() {
        return code;
    }

    public Devise code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public Devise libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Integer getNombreApresVirgule() {
        return nombreApresVirgule;
    }

    public Devise nombreApresVirgule(Integer nombreApresVirgule) {
        this.nombreApresVirgule = nombreApresVirgule;
        return this;
    }

    public void setNombreApresVirgule(Integer nombreApresVirgule) {
        this.nombreApresVirgule = nombreApresVirgule;
    }

    public Boolean isActif() {
        return actif;
    }

    public Devise actif(Boolean actif) {
        this.actif = actif;
        return this;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Devise)) {
            return false;
        }
        return id != null && id.equals(((Devise) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Devise{" +
            "id=" + getId() +
            ", abreviationCourte='" + getAbreviationCourte() + "'" +
            ", abreviationLongue='" + getAbreviationLongue() + "'" +
            ", code='" + getCode() + "'" +
            ", libelle='" + getLibelle() + "'" +
            ", nombreApresVirgule=" + getNombreApresVirgule() +
            ", actif='" + isActif() + "'" +
            "}";
    }
}
