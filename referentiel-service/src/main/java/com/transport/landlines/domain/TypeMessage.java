package com.transport.landlines.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A TypeMessage.
 */
@Entity
@Table(name = "type_message")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TypeMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "description")
    private String description;

    @Column(name = "titre")
    private String titre;

    @Column(name = "type")
    private String type;

    @Column(name = "code_fonction")
    private String codeFonction;

    @ManyToOne
    @JsonIgnoreProperties(value = "typeMessages", allowSetters = true)
    private Codification criticite;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public TypeMessage description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitre() {
        return titre;
    }

    public TypeMessage titre(String titre) {
        this.titre = titre;
        return this;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getType() {
        return type;
    }

    public TypeMessage type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCodeFonction() {
        return codeFonction;
    }

    public TypeMessage codeFonction(String codeFonction) {
        this.codeFonction = codeFonction;
        return this;
    }

    public void setCodeFonction(String codeFonction) {
        this.codeFonction = codeFonction;
    }

    public Codification getCriticite() {
        return criticite;
    }

    public TypeMessage criticite(Codification codification) {
        this.criticite = codification;
        return this;
    }

    public void setCriticite(Codification codification) {
        this.criticite = codification;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TypeMessage)) {
            return false;
        }
        return id != null && id.equals(((TypeMessage) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TypeMessage{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", titre='" + getTitre() + "'" +
            ", type='" + getType() + "'" +
            ", codeFonction='" + getCodeFonction() + "'" +
            "}";
    }
}
