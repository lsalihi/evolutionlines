package com.transport.landlines.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A CompteBancaire.
 */
@Entity
@Table(name = "compte_bancaire")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CompteBancaire implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "rib")
    private String rib;

    @OneToOne
    @JoinColumn(unique = true)
    private Banque banque;

    @OneToOne
    @JoinColumn(unique = true)
    private Adresse adresseAgence;

    @OneToOne
    @JoinColumn(unique = true)
    private Contact contactAgence;

    @ManyToOne
    @JsonIgnoreProperties(value = "compteBancaires", allowSetters = true)
    private Ville villeAgence;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRib() {
        return rib;
    }

    public CompteBancaire rib(String rib) {
        this.rib = rib;
        return this;
    }

    public void setRib(String rib) {
        this.rib = rib;
    }

    public Banque getBanque() {
        return banque;
    }

    public CompteBancaire banque(Banque banque) {
        this.banque = banque;
        return this;
    }

    public void setBanque(Banque banque) {
        this.banque = banque;
    }

    public Adresse getAdresseAgence() {
        return adresseAgence;
    }

    public CompteBancaire adresseAgence(Adresse adresse) {
        this.adresseAgence = adresse;
        return this;
    }

    public void setAdresseAgence(Adresse adresse) {
        this.adresseAgence = adresse;
    }

    public Contact getContactAgence() {
        return contactAgence;
    }

    public CompteBancaire contactAgence(Contact contact) {
        this.contactAgence = contact;
        return this;
    }

    public void setContactAgence(Contact contact) {
        this.contactAgence = contact;
    }

    public Ville getVilleAgence() {
        return villeAgence;
    }

    public CompteBancaire villeAgence(Ville ville) {
        this.villeAgence = ville;
        return this;
    }

    public void setVilleAgence(Ville ville) {
        this.villeAgence = ville;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompteBancaire)) {
            return false;
        }
        return id != null && id.equals(((CompteBancaire) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompteBancaire{" +
            "id=" + getId() +
            ", rib='" + getRib() + "'" +
            "}";
    }
}
