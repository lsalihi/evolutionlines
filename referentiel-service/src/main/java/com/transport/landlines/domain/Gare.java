package com.transport.landlines.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Gare.
 */
@Entity
@Table(name = "gare")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Gare implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "libelle")
    private String libelle;

    @Column(name = "correspondance")
    private Boolean correspondance;

    @Column(name = "gare_agence")
    private Boolean gareAgence;

    @Column(name = "actif")
    private Boolean actif;

    @ManyToOne
    @JsonIgnoreProperties(value = "gares", allowSetters = true)
    private Ville ville;

    @ManyToOne
    @JsonIgnoreProperties(value = "gares", allowSetters = true)
    private Agence agence;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public Gare libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Boolean isCorrespondance() {
        return correspondance;
    }

    public Gare correspondance(Boolean correspondance) {
        this.correspondance = correspondance;
        return this;
    }

    public void setCorrespondance(Boolean correspondance) {
        this.correspondance = correspondance;
    }

    public Boolean isGareAgence() {
        return gareAgence;
    }

    public Gare gareAgence(Boolean gareAgence) {
        this.gareAgence = gareAgence;
        return this;
    }

    public void setGareAgence(Boolean gareAgence) {
        this.gareAgence = gareAgence;
    }

    public Boolean isActif() {
        return actif;
    }

    public Gare actif(Boolean actif) {
        this.actif = actif;
        return this;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public Ville getVille() {
        return ville;
    }

    public Gare ville(Ville ville) {
        this.ville = ville;
        return this;
    }

    public void setVille(Ville ville) {
        this.ville = ville;
    }

    public Agence getAgence() {
        return agence;
    }

    public Gare agence(Agence agence) {
        this.agence = agence;
        return this;
    }

    public void setAgence(Agence agence) {
        this.agence = agence;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Gare)) {
            return false;
        }
        return id != null && id.equals(((Gare) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Gare{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            ", correspondance='" + isCorrespondance() + "'" +
            ", gareAgence='" + isGareAgence() + "'" +
            ", actif='" + isActif() + "'" +
            "}";
    }
}
