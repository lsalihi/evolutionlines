package com.transport.landlines.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A ModeleMail.
 */
@Entity
@Table(name = "modele_mail")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ModeleMail implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "destinataire")
    private String destinataire;

    @Column(name = "objet_mail")
    private String objetMail;

    @Column(name = "seuil")
    private Integer seuil;

    @Column(name = "actif")
    private Boolean actif;

    @ManyToOne
    @JsonIgnoreProperties(value = "modeleMails", allowSetters = true)
    private TypeMessage typeMessage;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDestinataire() {
        return destinataire;
    }

    public ModeleMail destinataire(String destinataire) {
        this.destinataire = destinataire;
        return this;
    }

    public void setDestinataire(String destinataire) {
        this.destinataire = destinataire;
    }

    public String getObjetMail() {
        return objetMail;
    }

    public ModeleMail objetMail(String objetMail) {
        this.objetMail = objetMail;
        return this;
    }

    public void setObjetMail(String objetMail) {
        this.objetMail = objetMail;
    }

    public Integer getSeuil() {
        return seuil;
    }

    public ModeleMail seuil(Integer seuil) {
        this.seuil = seuil;
        return this;
    }

    public void setSeuil(Integer seuil) {
        this.seuil = seuil;
    }

    public Boolean isActif() {
        return actif;
    }

    public ModeleMail actif(Boolean actif) {
        this.actif = actif;
        return this;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public TypeMessage getTypeMessage() {
        return typeMessage;
    }

    public ModeleMail typeMessage(TypeMessage typeMessage) {
        this.typeMessage = typeMessage;
        return this;
    }

    public void setTypeMessage(TypeMessage typeMessage) {
        this.typeMessage = typeMessage;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ModeleMail)) {
            return false;
        }
        return id != null && id.equals(((ModeleMail) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ModeleMail{" +
            "id=" + getId() +
            ", destinataire='" + getDestinataire() + "'" +
            ", objetMail='" + getObjetMail() + "'" +
            ", seuil=" + getSeuil() +
            ", actif='" + isActif() + "'" +
            "}";
    }
}
