package com.transport.landlines.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Autocar.
 */
@Entity
@Table(name = "autocar")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Autocar implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "numero_societe")
    private String numeroSociete;

    @Column(name = "marque")
    private String marque;

    @Column(name = "immatriculation")
    private String immatriculation;

    @Column(name = "capacite")
    private Integer capacite;

    @Column(name = "actif")
    private Boolean actif;

    @ManyToOne
    @JsonIgnoreProperties(value = "autocars", allowSetters = true)
    private Codification categorie;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroSociete() {
        return numeroSociete;
    }

    public Autocar numeroSociete(String numeroSociete) {
        this.numeroSociete = numeroSociete;
        return this;
    }

    public void setNumeroSociete(String numeroSociete) {
        this.numeroSociete = numeroSociete;
    }

    public String getMarque() {
        return marque;
    }

    public Autocar marque(String marque) {
        this.marque = marque;
        return this;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getImmatriculation() {
        return immatriculation;
    }

    public Autocar immatriculation(String immatriculation) {
        this.immatriculation = immatriculation;
        return this;
    }

    public void setImmatriculation(String immatriculation) {
        this.immatriculation = immatriculation;
    }

    public Integer getCapacite() {
        return capacite;
    }

    public Autocar capacite(Integer capacite) {
        this.capacite = capacite;
        return this;
    }

    public void setCapacite(Integer capacite) {
        this.capacite = capacite;
    }

    public Boolean isActif() {
        return actif;
    }

    public Autocar actif(Boolean actif) {
        this.actif = actif;
        return this;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public Codification getCategorie() {
        return categorie;
    }

    public Autocar categorie(Codification codification) {
        this.categorie = codification;
        return this;
    }

    public void setCategorie(Codification codification) {
        this.categorie = codification;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Autocar)) {
            return false;
        }
        return id != null && id.equals(((Autocar) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Autocar{" +
            "id=" + getId() +
            ", numeroSociete='" + getNumeroSociete() + "'" +
            ", marque='" + getMarque() + "'" +
            ", immatriculation='" + getImmatriculation() + "'" +
            ", capacite=" + getCapacite() +
            ", actif='" + isActif() + "'" +
            "}";
    }
}
