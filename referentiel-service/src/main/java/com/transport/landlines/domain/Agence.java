package com.transport.landlines.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Agence.
 */
@Entity
@Table(name = "agence")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Agence implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "libelle")
    private String libelle;

    @Column(name = "code")
    private String code;

    @Column(name = "code_comptable")
    private String codeComptable;

    @Column(name = "identification_fiscale")
    private String identificationFiscale;

    @Column(name = "date_creation")
    private LocalDate dateCreation;

    @Column(name = "debut_service")
    private LocalDate debutService;

    @Column(name = "fin_service")
    private LocalDate finService;

    @Column(name = "date_ouverture")
    private LocalDate dateOuverture;

    @Column(name = "date_mise_en_prod")
    private LocalDate dateMiseEnProd;

    @Column(name = "taxe_professionnelles")
    private Double taxeProfessionnelles;

    @Column(name = "fond_depense")
    private Double fondDepense;

    @Column(name = "seuil_versement_intermediaire")
    private Double seuilVersementIntermediaire;

    @Column(name = "informatise")
    private Boolean informatise;

    @Column(name = "date_desactivation")
    private LocalDate dateDesactivation;

    @Column(name = "motif_desactivation")
    private String motifDesactivation;

    @Column(name = "motif_fermeture")
    private String motifFermeture;

    @Column(name = "motif_reouverture")
    private String motifReouverture;

    @Column(name = "motif_statut_metier")
    private String motifStatutMetier;

    @Column(name = "date_fermeture")
    private LocalDate dateFermeture;

    @Column(name = "actif")
    private Boolean actif;

    @OneToOne
    @JoinColumn(unique = true)
    private Adresse adresse;

    @OneToOne
    @JoinColumn(unique = true)
    private PointVenteConfig pointVente;

    @OneToOne
    @JoinColumn(unique = true)
    private CompteBancaire compteBancaire;

    @OneToMany(mappedBy = "agence")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Contact> contacts = new HashSet<>();

    @OneToMany(mappedBy = "agence")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<PieceJointe> piecesJointes = new HashSet<>();

    @OneToMany(mappedBy = "agence")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<HoraireService> horairesServices = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "agences", allowSetters = true)
    private Ville ville;

    @ManyToOne
    @JsonIgnoreProperties(value = "agences", allowSetters = true)
    private Codification agenceBusinessStatus;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public Agence libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getCode() {
        return code;
    }

    public Agence code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeComptable() {
        return codeComptable;
    }

    public Agence codeComptable(String codeComptable) {
        this.codeComptable = codeComptable;
        return this;
    }

    public void setCodeComptable(String codeComptable) {
        this.codeComptable = codeComptable;
    }

    public String getIdentificationFiscale() {
        return identificationFiscale;
    }

    public Agence identificationFiscale(String identificationFiscale) {
        this.identificationFiscale = identificationFiscale;
        return this;
    }

    public void setIdentificationFiscale(String identificationFiscale) {
        this.identificationFiscale = identificationFiscale;
    }

    public LocalDate getDateCreation() {
        return dateCreation;
    }

    public Agence dateCreation(LocalDate dateCreation) {
        this.dateCreation = dateCreation;
        return this;
    }

    public void setDateCreation(LocalDate dateCreation) {
        this.dateCreation = dateCreation;
    }

    public LocalDate getDebutService() {
        return debutService;
    }

    public Agence debutService(LocalDate debutService) {
        this.debutService = debutService;
        return this;
    }

    public void setDebutService(LocalDate debutService) {
        this.debutService = debutService;
    }

    public LocalDate getFinService() {
        return finService;
    }

    public Agence finService(LocalDate finService) {
        this.finService = finService;
        return this;
    }

    public void setFinService(LocalDate finService) {
        this.finService = finService;
    }

    public LocalDate getDateOuverture() {
        return dateOuverture;
    }

    public Agence dateOuverture(LocalDate dateOuverture) {
        this.dateOuverture = dateOuverture;
        return this;
    }

    public void setDateOuverture(LocalDate dateOuverture) {
        this.dateOuverture = dateOuverture;
    }

    public LocalDate getDateMiseEnProd() {
        return dateMiseEnProd;
    }

    public Agence dateMiseEnProd(LocalDate dateMiseEnProd) {
        this.dateMiseEnProd = dateMiseEnProd;
        return this;
    }

    public void setDateMiseEnProd(LocalDate dateMiseEnProd) {
        this.dateMiseEnProd = dateMiseEnProd;
    }

    public Double getTaxeProfessionnelles() {
        return taxeProfessionnelles;
    }

    public Agence taxeProfessionnelles(Double taxeProfessionnelles) {
        this.taxeProfessionnelles = taxeProfessionnelles;
        return this;
    }

    public void setTaxeProfessionnelles(Double taxeProfessionnelles) {
        this.taxeProfessionnelles = taxeProfessionnelles;
    }

    public Double getFondDepense() {
        return fondDepense;
    }

    public Agence fondDepense(Double fondDepense) {
        this.fondDepense = fondDepense;
        return this;
    }

    public void setFondDepense(Double fondDepense) {
        this.fondDepense = fondDepense;
    }

    public Double getSeuilVersementIntermediaire() {
        return seuilVersementIntermediaire;
    }

    public Agence seuilVersementIntermediaire(Double seuilVersementIntermediaire) {
        this.seuilVersementIntermediaire = seuilVersementIntermediaire;
        return this;
    }

    public void setSeuilVersementIntermediaire(Double seuilVersementIntermediaire) {
        this.seuilVersementIntermediaire = seuilVersementIntermediaire;
    }

    public Boolean isInformatise() {
        return informatise;
    }

    public Agence informatise(Boolean informatise) {
        this.informatise = informatise;
        return this;
    }

    public void setInformatise(Boolean informatise) {
        this.informatise = informatise;
    }

    public LocalDate getDateDesactivation() {
        return dateDesactivation;
    }

    public Agence dateDesactivation(LocalDate dateDesactivation) {
        this.dateDesactivation = dateDesactivation;
        return this;
    }

    public void setDateDesactivation(LocalDate dateDesactivation) {
        this.dateDesactivation = dateDesactivation;
    }

    public String getMotifDesactivation() {
        return motifDesactivation;
    }

    public Agence motifDesactivation(String motifDesactivation) {
        this.motifDesactivation = motifDesactivation;
        return this;
    }

    public void setMotifDesactivation(String motifDesactivation) {
        this.motifDesactivation = motifDesactivation;
    }

    public String getMotifFermeture() {
        return motifFermeture;
    }

    public Agence motifFermeture(String motifFermeture) {
        this.motifFermeture = motifFermeture;
        return this;
    }

    public void setMotifFermeture(String motifFermeture) {
        this.motifFermeture = motifFermeture;
    }

    public String getMotifReouverture() {
        return motifReouverture;
    }

    public Agence motifReouverture(String motifReouverture) {
        this.motifReouverture = motifReouverture;
        return this;
    }

    public void setMotifReouverture(String motifReouverture) {
        this.motifReouverture = motifReouverture;
    }

    public String getMotifStatutMetier() {
        return motifStatutMetier;
    }

    public Agence motifStatutMetier(String motifStatutMetier) {
        this.motifStatutMetier = motifStatutMetier;
        return this;
    }

    public void setMotifStatutMetier(String motifStatutMetier) {
        this.motifStatutMetier = motifStatutMetier;
    }

    public LocalDate getDateFermeture() {
        return dateFermeture;
    }

    public Agence dateFermeture(LocalDate dateFermeture) {
        this.dateFermeture = dateFermeture;
        return this;
    }

    public void setDateFermeture(LocalDate dateFermeture) {
        this.dateFermeture = dateFermeture;
    }

    public Boolean isActif() {
        return actif;
    }

    public Agence actif(Boolean actif) {
        this.actif = actif;
        return this;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public Agence adresse(Adresse adresse) {
        this.adresse = adresse;
        return this;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    public PointVenteConfig getPointVente() {
        return pointVente;
    }

    public Agence pointVente(PointVenteConfig pointVenteConfig) {
        this.pointVente = pointVenteConfig;
        return this;
    }

    public void setPointVente(PointVenteConfig pointVenteConfig) {
        this.pointVente = pointVenteConfig;
    }

    public CompteBancaire getCompteBancaire() {
        return compteBancaire;
    }

    public Agence compteBancaire(CompteBancaire compteBancaire) {
        this.compteBancaire = compteBancaire;
        return this;
    }

    public void setCompteBancaire(CompteBancaire compteBancaire) {
        this.compteBancaire = compteBancaire;
    }

    public Set<Contact> getContacts() {
        return contacts;
    }

    public Agence contacts(Set<Contact> contacts) {
        this.contacts = contacts;
        return this;
    }

    public Agence addContact(Contact contact) {
        this.contacts.add(contact);
        contact.setAgence(this);
        return this;
    }

    public Agence removeContact(Contact contact) {
        this.contacts.remove(contact);
        contact.setAgence(null);
        return this;
    }

    public void setContacts(Set<Contact> contacts) {
        this.contacts = contacts;
    }

    public Set<PieceJointe> getPiecesJointes() {
        return piecesJointes;
    }

    public Agence piecesJointes(Set<PieceJointe> pieceJointes) {
        this.piecesJointes = pieceJointes;
        return this;
    }

    public Agence addPiecesJointes(PieceJointe pieceJointe) {
        this.piecesJointes.add(pieceJointe);
        pieceJointe.setAgence(this);
        return this;
    }

    public Agence removePiecesJointes(PieceJointe pieceJointe) {
        this.piecesJointes.remove(pieceJointe);
        pieceJointe.setAgence(null);
        return this;
    }

    public void setPiecesJointes(Set<PieceJointe> pieceJointes) {
        this.piecesJointes = pieceJointes;
    }

    public Set<HoraireService> getHorairesServices() {
        return horairesServices;
    }

    public Agence horairesServices(Set<HoraireService> horaireServices) {
        this.horairesServices = horaireServices;
        return this;
    }

    public Agence addHorairesServices(HoraireService horaireService) {
        this.horairesServices.add(horaireService);
        horaireService.setAgence(this);
        return this;
    }

    public Agence removeHorairesServices(HoraireService horaireService) {
        this.horairesServices.remove(horaireService);
        horaireService.setAgence(null);
        return this;
    }

    public void setHorairesServices(Set<HoraireService> horaireServices) {
        this.horairesServices = horaireServices;
    }

    public Ville getVille() {
        return ville;
    }

    public Agence ville(Ville ville) {
        this.ville = ville;
        return this;
    }

    public void setVille(Ville ville) {
        this.ville = ville;
    }

    public Codification getAgenceBusinessStatus() {
        return agenceBusinessStatus;
    }

    public Agence agenceBusinessStatus(Codification codification) {
        this.agenceBusinessStatus = codification;
        return this;
    }

    public void setAgenceBusinessStatus(Codification codification) {
        this.agenceBusinessStatus = codification;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Agence)) {
            return false;
        }
        return id != null && id.equals(((Agence) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Agence{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            ", code='" + getCode() + "'" +
            ", codeComptable='" + getCodeComptable() + "'" +
            ", identificationFiscale='" + getIdentificationFiscale() + "'" +
            ", dateCreation='" + getDateCreation() + "'" +
            ", debutService='" + getDebutService() + "'" +
            ", finService='" + getFinService() + "'" +
            ", dateOuverture='" + getDateOuverture() + "'" +
            ", dateMiseEnProd='" + getDateMiseEnProd() + "'" +
            ", taxeProfessionnelles=" + getTaxeProfessionnelles() +
            ", fondDepense=" + getFondDepense() +
            ", seuilVersementIntermediaire=" + getSeuilVersementIntermediaire() +
            ", informatise='" + isInformatise() + "'" +
            ", dateDesactivation='" + getDateDesactivation() + "'" +
            ", motifDesactivation='" + getMotifDesactivation() + "'" +
            ", motifFermeture='" + getMotifFermeture() + "'" +
            ", motifReouverture='" + getMotifReouverture() + "'" +
            ", motifStatutMetier='" + getMotifStatutMetier() + "'" +
            ", dateFermeture='" + getDateFermeture() + "'" +
            ", actif='" + isActif() + "'" +
            "}";
    }
}
