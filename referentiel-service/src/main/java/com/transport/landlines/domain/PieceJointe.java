package com.transport.landlines.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A PieceJointe.
 */
@Entity
@Table(name = "piece_jointe")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PieceJointe implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "libelle")
    private String libelle;

    @Column(name = "nom_pj")
    private String nomPj;

    @Column(name = "date_ajout")
    private LocalDate dateAjout;

    @Column(name = "path")
    private String path;

    @Column(name = "file_type")
    private String fileType;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JsonIgnoreProperties(value = "pieceJointes", allowSetters = true)
    private Codification statutPieceJointe;

    @ManyToOne
    @JsonIgnoreProperties(value = "piecesJointes", allowSetters = true)
    private Agence agence;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public PieceJointe libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getNomPj() {
        return nomPj;
    }

    public PieceJointe nomPj(String nomPj) {
        this.nomPj = nomPj;
        return this;
    }

    public void setNomPj(String nomPj) {
        this.nomPj = nomPj;
    }

    public LocalDate getDateAjout() {
        return dateAjout;
    }

    public PieceJointe dateAjout(LocalDate dateAjout) {
        this.dateAjout = dateAjout;
        return this;
    }

    public void setDateAjout(LocalDate dateAjout) {
        this.dateAjout = dateAjout;
    }

    public String getPath() {
        return path;
    }

    public PieceJointe path(String path) {
        this.path = path;
        return this;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFileType() {
        return fileType;
    }

    public PieceJointe fileType(String fileType) {
        this.fileType = fileType;
        return this;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getDescription() {
        return description;
    }

    public PieceJointe description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Codification getStatutPieceJointe() {
        return statutPieceJointe;
    }

    public PieceJointe statutPieceJointe(Codification codification) {
        this.statutPieceJointe = codification;
        return this;
    }

    public void setStatutPieceJointe(Codification codification) {
        this.statutPieceJointe = codification;
    }

    public Agence getAgence() {
        return agence;
    }

    public PieceJointe agence(Agence agence) {
        this.agence = agence;
        return this;
    }

    public void setAgence(Agence agence) {
        this.agence = agence;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PieceJointe)) {
            return false;
        }
        return id != null && id.equals(((PieceJointe) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PieceJointe{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            ", nomPj='" + getNomPj() + "'" +
            ", dateAjout='" + getDateAjout() + "'" +
            ", path='" + getPath() + "'" +
            ", fileType='" + getFileType() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
