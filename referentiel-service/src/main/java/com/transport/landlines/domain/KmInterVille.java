package com.transport.landlines.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A KmInterVille.
 */
@Entity
@Table(name = "km_inter_ville")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class KmInterVille implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "kilometres")
    private Integer kilometres;

    @Column(name = "statut")
    private Boolean statut;

    @ManyToOne
    @JsonIgnoreProperties(value = "kmInterVilles", allowSetters = true)
    private Ville villeDepart;

    @ManyToOne
    @JsonIgnoreProperties(value = "kmInterVilles", allowSetters = true)
    private Ville villeArrivee;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getKilometres() {
        return kilometres;
    }

    public KmInterVille kilometres(Integer kilometres) {
        this.kilometres = kilometres;
        return this;
    }

    public void setKilometres(Integer kilometres) {
        this.kilometres = kilometres;
    }

    public Boolean isStatut() {
        return statut;
    }

    public KmInterVille statut(Boolean statut) {
        this.statut = statut;
        return this;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public Ville getVilleDepart() {
        return villeDepart;
    }

    public KmInterVille villeDepart(Ville ville) {
        this.villeDepart = ville;
        return this;
    }

    public void setVilleDepart(Ville ville) {
        this.villeDepart = ville;
    }

    public Ville getVilleArrivee() {
        return villeArrivee;
    }

    public KmInterVille villeArrivee(Ville ville) {
        this.villeArrivee = ville;
        return this;
    }

    public void setVilleArrivee(Ville ville) {
        this.villeArrivee = ville;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof KmInterVille)) {
            return false;
        }
        return id != null && id.equals(((KmInterVille) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "KmInterVille{" +
            "id=" + getId() +
            ", kilometres=" + getKilometres() +
            ", statut='" + isStatut() + "'" +
            "}";
    }
}
