package com.transport.landlines.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Pays.
 */
@Entity
@Table(name = "pays")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Pays implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "abreviation")
    private String abreviation;

    @Column(name = "code_alphanum")
    private String codeAlphanum;

    @Column(name = "code_numerique")
    private String codeNumerique;

    @Column(name = "date_debut_heure_ete")
    private LocalDate dateDebutHeureEte;

    @Column(name = "date_fin_heure_ete")
    private LocalDate dateFinHeureEte;

    @Column(name = "heure_ete")
    private Boolean heureEte;

    @Column(name = "decalage_horaire")
    private String decalageHoraire;

    @Column(name = "libelle")
    private String libelle;

    @Column(name = "actif")
    private Boolean actif;

    @ManyToOne
    @JsonIgnoreProperties(value = "pays", allowSetters = true)
    private Devise devise;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAbreviation() {
        return abreviation;
    }

    public Pays abreviation(String abreviation) {
        this.abreviation = abreviation;
        return this;
    }

    public void setAbreviation(String abreviation) {
        this.abreviation = abreviation;
    }

    public String getCodeAlphanum() {
        return codeAlphanum;
    }

    public Pays codeAlphanum(String codeAlphanum) {
        this.codeAlphanum = codeAlphanum;
        return this;
    }

    public void setCodeAlphanum(String codeAlphanum) {
        this.codeAlphanum = codeAlphanum;
    }

    public String getCodeNumerique() {
        return codeNumerique;
    }

    public Pays codeNumerique(String codeNumerique) {
        this.codeNumerique = codeNumerique;
        return this;
    }

    public void setCodeNumerique(String codeNumerique) {
        this.codeNumerique = codeNumerique;
    }

    public LocalDate getDateDebutHeureEte() {
        return dateDebutHeureEte;
    }

    public Pays dateDebutHeureEte(LocalDate dateDebutHeureEte) {
        this.dateDebutHeureEte = dateDebutHeureEte;
        return this;
    }

    public void setDateDebutHeureEte(LocalDate dateDebutHeureEte) {
        this.dateDebutHeureEte = dateDebutHeureEte;
    }

    public LocalDate getDateFinHeureEte() {
        return dateFinHeureEte;
    }

    public Pays dateFinHeureEte(LocalDate dateFinHeureEte) {
        this.dateFinHeureEte = dateFinHeureEte;
        return this;
    }

    public void setDateFinHeureEte(LocalDate dateFinHeureEte) {
        this.dateFinHeureEte = dateFinHeureEte;
    }

    public Boolean isHeureEte() {
        return heureEte;
    }

    public Pays heureEte(Boolean heureEte) {
        this.heureEte = heureEte;
        return this;
    }

    public void setHeureEte(Boolean heureEte) {
        this.heureEte = heureEte;
    }

    public String getDecalageHoraire() {
        return decalageHoraire;
    }

    public Pays decalageHoraire(String decalageHoraire) {
        this.decalageHoraire = decalageHoraire;
        return this;
    }

    public void setDecalageHoraire(String decalageHoraire) {
        this.decalageHoraire = decalageHoraire;
    }

    public String getLibelle() {
        return libelle;
    }

    public Pays libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Boolean isActif() {
        return actif;
    }

    public Pays actif(Boolean actif) {
        this.actif = actif;
        return this;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public Devise getDevise() {
        return devise;
    }

    public Pays devise(Devise devise) {
        this.devise = devise;
        return this;
    }

    public void setDevise(Devise devise) {
        this.devise = devise;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Pays)) {
            return false;
        }
        return id != null && id.equals(((Pays) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Pays{" +
            "id=" + getId() +
            ", abreviation='" + getAbreviation() + "'" +
            ", codeAlphanum='" + getCodeAlphanum() + "'" +
            ", codeNumerique='" + getCodeNumerique() + "'" +
            ", dateDebutHeureEte='" + getDateDebutHeureEte() + "'" +
            ", dateFinHeureEte='" + getDateFinHeureEte() + "'" +
            ", heureEte='" + isHeureEte() + "'" +
            ", decalageHoraire='" + getDecalageHoraire() + "'" +
            ", libelle='" + getLibelle() + "'" +
            ", actif='" + isActif() + "'" +
            "}";
    }
}
