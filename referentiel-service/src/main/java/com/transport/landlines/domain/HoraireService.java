package com.transport.landlines.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A HoraireService.
 */
@Entity
@Table(name = "horaire_service")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class HoraireService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "ouverture")
    private LocalDate ouverture;

    @Column(name = "fermeture")
    private LocalDate fermeture;

    @ManyToOne
    @JsonIgnoreProperties(value = "horairesServices", allowSetters = true)
    private Agence agence;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getOuverture() {
        return ouverture;
    }

    public HoraireService ouverture(LocalDate ouverture) {
        this.ouverture = ouverture;
        return this;
    }

    public void setOuverture(LocalDate ouverture) {
        this.ouverture = ouverture;
    }

    public LocalDate getFermeture() {
        return fermeture;
    }

    public HoraireService fermeture(LocalDate fermeture) {
        this.fermeture = fermeture;
        return this;
    }

    public void setFermeture(LocalDate fermeture) {
        this.fermeture = fermeture;
    }

    public Agence getAgence() {
        return agence;
    }

    public HoraireService agence(Agence agence) {
        this.agence = agence;
        return this;
    }

    public void setAgence(Agence agence) {
        this.agence = agence;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HoraireService)) {
            return false;
        }
        return id != null && id.equals(((HoraireService) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "HoraireService{" +
            "id=" + getId() +
            ", ouverture='" + getOuverture() + "'" +
            ", fermeture='" + getFermeture() + "'" +
            "}";
    }
}
