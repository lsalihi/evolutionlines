package com.transport.landlines.service.mapper;


import com.transport.landlines.domain.*;
import com.transport.landlines.service.dto.CompteBancaireDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CompteBancaire} and its DTO {@link CompteBancaireDTO}.
 */
@Mapper(componentModel = "spring", uses = {BanqueMapper.class, AdresseMapper.class, ContactMapper.class, VilleMapper.class})
public interface CompteBancaireMapper extends EntityMapper<CompteBancaireDTO, CompteBancaire> {

    @Mapping(source = "banque.id", target = "banqueId")
    @Mapping(source = "adresseAgence.id", target = "adresseAgenceId")
    @Mapping(source = "contactAgence.id", target = "contactAgenceId")
    @Mapping(source = "villeAgence.id", target = "villeAgenceId")
    CompteBancaireDTO toDto(CompteBancaire compteBancaire);

    @Mapping(source = "banqueId", target = "banque")
    @Mapping(source = "adresseAgenceId", target = "adresseAgence")
    @Mapping(source = "contactAgenceId", target = "contactAgence")
    @Mapping(source = "villeAgenceId", target = "villeAgence")
    CompteBancaire toEntity(CompteBancaireDTO compteBancaireDTO);

    default CompteBancaire fromId(Long id) {
        if (id == null) {
            return null;
        }
        CompteBancaire compteBancaire = new CompteBancaire();
        compteBancaire.setId(id);
        return compteBancaire;
    }
}
