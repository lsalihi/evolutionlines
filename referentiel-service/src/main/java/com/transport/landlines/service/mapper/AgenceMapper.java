package com.transport.landlines.service.mapper;


import com.transport.landlines.domain.*;
import com.transport.landlines.service.dto.AgenceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Agence} and its DTO {@link AgenceDTO}.
 */
@Mapper(componentModel = "spring", uses = {AdresseMapper.class, PointVenteConfigMapper.class, CompteBancaireMapper.class, VilleMapper.class, CodificationMapper.class})
public interface AgenceMapper extends EntityMapper<AgenceDTO, Agence> {

    @Mapping(source = "adresse.id", target = "adresseId")
    @Mapping(source = "pointVente.id", target = "pointVenteId")
    @Mapping(source = "compteBancaire.id", target = "compteBancaireId")
    @Mapping(source = "ville.id", target = "villeId")
    @Mapping(source = "agenceBusinessStatus.id", target = "agenceBusinessStatusId")
    AgenceDTO toDto(Agence agence);

    @Mapping(source = "adresseId", target = "adresse")
    @Mapping(source = "pointVenteId", target = "pointVente")
    @Mapping(source = "compteBancaireId", target = "compteBancaire")
    @Mapping(target = "contacts", ignore = true)
    @Mapping(target = "removeContact", ignore = true)
    @Mapping(target = "piecesJointes", ignore = true)
    @Mapping(target = "removePiecesJointes", ignore = true)
    @Mapping(target = "horairesServices", ignore = true)
    @Mapping(target = "removeHorairesServices", ignore = true)
    @Mapping(source = "villeId", target = "ville")
    @Mapping(source = "agenceBusinessStatusId", target = "agenceBusinessStatus")
    Agence toEntity(AgenceDTO agenceDTO);

    default Agence fromId(Long id) {
        if (id == null) {
            return null;
        }
        Agence agence = new Agence();
        agence.setId(id);
        return agence;
    }
}
