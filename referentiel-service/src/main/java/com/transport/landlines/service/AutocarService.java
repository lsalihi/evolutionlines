package com.transport.landlines.service;

import com.transport.landlines.service.dto.AutocarDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.transport.landlines.domain.Autocar}.
 */
public interface AutocarService {

    /**
     * Save a autocar.
     *
     * @param autocarDTO the entity to save.
     * @return the persisted entity.
     */
    AutocarDTO save(AutocarDTO autocarDTO);

    /**
     * Get all the autocars.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AutocarDTO> findAll(Pageable pageable);


    /**
     * Get the "id" autocar.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AutocarDTO> findOne(Long id);

    /**
     * Delete the "id" autocar.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
