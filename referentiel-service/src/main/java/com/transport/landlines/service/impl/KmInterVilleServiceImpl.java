package com.transport.landlines.service.impl;

import com.transport.landlines.service.KmInterVilleService;
import com.transport.landlines.domain.KmInterVille;
import com.transport.landlines.repository.KmInterVilleRepository;
import com.transport.landlines.service.dto.KmInterVilleDTO;
import com.transport.landlines.service.mapper.KmInterVilleMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link KmInterVille}.
 */
@Service
@Transactional
public class KmInterVilleServiceImpl implements KmInterVilleService {

    private final Logger log = LoggerFactory.getLogger(KmInterVilleServiceImpl.class);

    private final KmInterVilleRepository kmInterVilleRepository;

    private final KmInterVilleMapper kmInterVilleMapper;

    public KmInterVilleServiceImpl(KmInterVilleRepository kmInterVilleRepository, KmInterVilleMapper kmInterVilleMapper) {
        this.kmInterVilleRepository = kmInterVilleRepository;
        this.kmInterVilleMapper = kmInterVilleMapper;
    }

    @Override
    public KmInterVilleDTO save(KmInterVilleDTO kmInterVilleDTO) {
        log.debug("Request to save KmInterVille : {}", kmInterVilleDTO);
        KmInterVille kmInterVille = kmInterVilleMapper.toEntity(kmInterVilleDTO);
        kmInterVille = kmInterVilleRepository.save(kmInterVille);
        return kmInterVilleMapper.toDto(kmInterVille);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<KmInterVilleDTO> findAll(Pageable pageable) {
        log.debug("Request to get all KmInterVilles");
        return kmInterVilleRepository.findAll(pageable)
            .map(kmInterVilleMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<KmInterVilleDTO> findOne(Long id) {
        log.debug("Request to get KmInterVille : {}", id);
        return kmInterVilleRepository.findById(id)
            .map(kmInterVilleMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete KmInterVille : {}", id);
        kmInterVilleRepository.deleteById(id);
    }
}
