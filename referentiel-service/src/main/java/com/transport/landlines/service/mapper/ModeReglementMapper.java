package com.transport.landlines.service.mapper;


import com.transport.landlines.domain.*;
import com.transport.landlines.service.dto.ModeReglementDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ModeReglement} and its DTO {@link ModeReglementDTO}.
 */
@Mapper(componentModel = "spring", uses = {CodificationMapper.class})
public interface ModeReglementMapper extends EntityMapper<ModeReglementDTO, ModeReglement> {

    @Mapping(source = "modeReglement.id", target = "modeReglementId")
    ModeReglementDTO toDto(ModeReglement modeReglement);

    @Mapping(source = "modeReglementId", target = "modeReglement")
    ModeReglement toEntity(ModeReglementDTO modeReglementDTO);

    default ModeReglement fromId(Long id) {
        if (id == null) {
            return null;
        }
        ModeReglement modeReglement = new ModeReglement();
        modeReglement.setId(id);
        return modeReglement;
    }
}
