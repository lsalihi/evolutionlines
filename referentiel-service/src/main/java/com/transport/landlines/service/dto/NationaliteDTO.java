package com.transport.landlines.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.transport.landlines.domain.Nationalite} entity.
 */
public class NationaliteDTO implements Serializable {
    
    private Long id;

    private String libelle;

    private Boolean actif;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Boolean isActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NationaliteDTO)) {
            return false;
        }

        return id != null && id.equals(((NationaliteDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NationaliteDTO{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            ", actif='" + isActif() + "'" +
            "}";
    }
}
