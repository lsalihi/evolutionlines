package com.transport.landlines.service.mapper;


import com.transport.landlines.domain.*;
import com.transport.landlines.service.dto.AutocarDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Autocar} and its DTO {@link AutocarDTO}.
 */
@Mapper(componentModel = "spring", uses = {CodificationMapper.class})
public interface AutocarMapper extends EntityMapper<AutocarDTO, Autocar> {

    @Mapping(source = "categorie.id", target = "categorieId")
    AutocarDTO toDto(Autocar autocar);

    @Mapping(source = "categorieId", target = "categorie")
    Autocar toEntity(AutocarDTO autocarDTO);

    default Autocar fromId(Long id) {
        if (id == null) {
            return null;
        }
        Autocar autocar = new Autocar();
        autocar.setId(id);
        return autocar;
    }
}
