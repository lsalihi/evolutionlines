package com.transport.landlines.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.transport.landlines.domain.TypeMessage} entity.
 */
public class TypeMessageDTO implements Serializable {
    
    private Long id;

    private String description;

    private String titre;

    private String type;

    private String codeFonction;


    private Long criticiteId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCodeFonction() {
        return codeFonction;
    }

    public void setCodeFonction(String codeFonction) {
        this.codeFonction = codeFonction;
    }

    public Long getCriticiteId() {
        return criticiteId;
    }

    public void setCriticiteId(Long codificationId) {
        this.criticiteId = codificationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TypeMessageDTO)) {
            return false;
        }

        return id != null && id.equals(((TypeMessageDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TypeMessageDTO{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", titre='" + getTitre() + "'" +
            ", type='" + getType() + "'" +
            ", codeFonction='" + getCodeFonction() + "'" +
            ", criticiteId=" + getCriticiteId() +
            "}";
    }
}
