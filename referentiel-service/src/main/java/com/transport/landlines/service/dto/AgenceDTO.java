package com.transport.landlines.service.dto;

import java.time.LocalDate;
import java.io.Serializable;

/**
 * A DTO for the {@link com.transport.landlines.domain.Agence} entity.
 */
public class AgenceDTO implements Serializable {
    
    private Long id;

    private String libelle;

    private String code;

    private String codeComptable;

    private String identificationFiscale;

    private LocalDate dateCreation;

    private LocalDate debutService;

    private LocalDate finService;

    private LocalDate dateOuverture;

    private LocalDate dateMiseEnProd;

    private Double taxeProfessionnelles;

    private Double fondDepense;

    private Double seuilVersementIntermediaire;

    private Boolean informatise;

    private LocalDate dateDesactivation;

    private String motifDesactivation;

    private String motifFermeture;

    private String motifReouverture;

    private String motifStatutMetier;

    private LocalDate dateFermeture;

    private Boolean actif;


    private Long adresseId;

    private Long pointVenteId;

    private Long compteBancaireId;

    private Long villeId;

    private Long agenceBusinessStatusId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeComptable() {
        return codeComptable;
    }

    public void setCodeComptable(String codeComptable) {
        this.codeComptable = codeComptable;
    }

    public String getIdentificationFiscale() {
        return identificationFiscale;
    }

    public void setIdentificationFiscale(String identificationFiscale) {
        this.identificationFiscale = identificationFiscale;
    }

    public LocalDate getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(LocalDate dateCreation) {
        this.dateCreation = dateCreation;
    }

    public LocalDate getDebutService() {
        return debutService;
    }

    public void setDebutService(LocalDate debutService) {
        this.debutService = debutService;
    }

    public LocalDate getFinService() {
        return finService;
    }

    public void setFinService(LocalDate finService) {
        this.finService = finService;
    }

    public LocalDate getDateOuverture() {
        return dateOuverture;
    }

    public void setDateOuverture(LocalDate dateOuverture) {
        this.dateOuverture = dateOuverture;
    }

    public LocalDate getDateMiseEnProd() {
        return dateMiseEnProd;
    }

    public void setDateMiseEnProd(LocalDate dateMiseEnProd) {
        this.dateMiseEnProd = dateMiseEnProd;
    }

    public Double getTaxeProfessionnelles() {
        return taxeProfessionnelles;
    }

    public void setTaxeProfessionnelles(Double taxeProfessionnelles) {
        this.taxeProfessionnelles = taxeProfessionnelles;
    }

    public Double getFondDepense() {
        return fondDepense;
    }

    public void setFondDepense(Double fondDepense) {
        this.fondDepense = fondDepense;
    }

    public Double getSeuilVersementIntermediaire() {
        return seuilVersementIntermediaire;
    }

    public void setSeuilVersementIntermediaire(Double seuilVersementIntermediaire) {
        this.seuilVersementIntermediaire = seuilVersementIntermediaire;
    }

    public Boolean isInformatise() {
        return informatise;
    }

    public void setInformatise(Boolean informatise) {
        this.informatise = informatise;
    }

    public LocalDate getDateDesactivation() {
        return dateDesactivation;
    }

    public void setDateDesactivation(LocalDate dateDesactivation) {
        this.dateDesactivation = dateDesactivation;
    }

    public String getMotifDesactivation() {
        return motifDesactivation;
    }

    public void setMotifDesactivation(String motifDesactivation) {
        this.motifDesactivation = motifDesactivation;
    }

    public String getMotifFermeture() {
        return motifFermeture;
    }

    public void setMotifFermeture(String motifFermeture) {
        this.motifFermeture = motifFermeture;
    }

    public String getMotifReouverture() {
        return motifReouverture;
    }

    public void setMotifReouverture(String motifReouverture) {
        this.motifReouverture = motifReouverture;
    }

    public String getMotifStatutMetier() {
        return motifStatutMetier;
    }

    public void setMotifStatutMetier(String motifStatutMetier) {
        this.motifStatutMetier = motifStatutMetier;
    }

    public LocalDate getDateFermeture() {
        return dateFermeture;
    }

    public void setDateFermeture(LocalDate dateFermeture) {
        this.dateFermeture = dateFermeture;
    }

    public Boolean isActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public Long getAdresseId() {
        return adresseId;
    }

    public void setAdresseId(Long adresseId) {
        this.adresseId = adresseId;
    }

    public Long getPointVenteId() {
        return pointVenteId;
    }

    public void setPointVenteId(Long pointVenteConfigId) {
        this.pointVenteId = pointVenteConfigId;
    }

    public Long getCompteBancaireId() {
        return compteBancaireId;
    }

    public void setCompteBancaireId(Long compteBancaireId) {
        this.compteBancaireId = compteBancaireId;
    }

    public Long getVilleId() {
        return villeId;
    }

    public void setVilleId(Long villeId) {
        this.villeId = villeId;
    }

    public Long getAgenceBusinessStatusId() {
        return agenceBusinessStatusId;
    }

    public void setAgenceBusinessStatusId(Long codificationId) {
        this.agenceBusinessStatusId = codificationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AgenceDTO)) {
            return false;
        }

        return id != null && id.equals(((AgenceDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AgenceDTO{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            ", code='" + getCode() + "'" +
            ", codeComptable='" + getCodeComptable() + "'" +
            ", identificationFiscale='" + getIdentificationFiscale() + "'" +
            ", dateCreation='" + getDateCreation() + "'" +
            ", debutService='" + getDebutService() + "'" +
            ", finService='" + getFinService() + "'" +
            ", dateOuverture='" + getDateOuverture() + "'" +
            ", dateMiseEnProd='" + getDateMiseEnProd() + "'" +
            ", taxeProfessionnelles=" + getTaxeProfessionnelles() +
            ", fondDepense=" + getFondDepense() +
            ", seuilVersementIntermediaire=" + getSeuilVersementIntermediaire() +
            ", informatise='" + isInformatise() + "'" +
            ", dateDesactivation='" + getDateDesactivation() + "'" +
            ", motifDesactivation='" + getMotifDesactivation() + "'" +
            ", motifFermeture='" + getMotifFermeture() + "'" +
            ", motifReouverture='" + getMotifReouverture() + "'" +
            ", motifStatutMetier='" + getMotifStatutMetier() + "'" +
            ", dateFermeture='" + getDateFermeture() + "'" +
            ", actif='" + isActif() + "'" +
            ", adresseId=" + getAdresseId() +
            ", pointVenteId=" + getPointVenteId() +
            ", compteBancaireId=" + getCompteBancaireId() +
            ", villeId=" + getVilleId() +
            ", agenceBusinessStatusId=" + getAgenceBusinessStatusId() +
            "}";
    }
}
