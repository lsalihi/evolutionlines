package com.transport.landlines.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.transport.landlines.domain.KmInterVille} entity.
 */
public class KmInterVilleDTO implements Serializable {
    
    private Long id;

    private Integer kilometres;

    private Boolean statut;


    private Long villeDepartId;

    private Long villeArriveeId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getKilometres() {
        return kilometres;
    }

    public void setKilometres(Integer kilometres) {
        this.kilometres = kilometres;
    }

    public Boolean isStatut() {
        return statut;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public Long getVilleDepartId() {
        return villeDepartId;
    }

    public void setVilleDepartId(Long villeId) {
        this.villeDepartId = villeId;
    }

    public Long getVilleArriveeId() {
        return villeArriveeId;
    }

    public void setVilleArriveeId(Long villeId) {
        this.villeArriveeId = villeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof KmInterVilleDTO)) {
            return false;
        }

        return id != null && id.equals(((KmInterVilleDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "KmInterVilleDTO{" +
            "id=" + getId() +
            ", kilometres=" + getKilometres() +
            ", statut='" + isStatut() + "'" +
            ", villeDepartId=" + getVilleDepartId() +
            ", villeArriveeId=" + getVilleArriveeId() +
            "}";
    }
}
