package com.transport.landlines.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.transport.landlines.domain.Gare} entity.
 */
public class GareDTO implements Serializable {
    
    private Long id;

    private String libelle;

    private Boolean correspondance;

    private Boolean gareAgence;

    private Boolean actif;


    private Long villeId;

    private Long agenceId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Boolean isCorrespondance() {
        return correspondance;
    }

    public void setCorrespondance(Boolean correspondance) {
        this.correspondance = correspondance;
    }

    public Boolean isGareAgence() {
        return gareAgence;
    }

    public void setGareAgence(Boolean gareAgence) {
        this.gareAgence = gareAgence;
    }

    public Boolean isActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public Long getVilleId() {
        return villeId;
    }

    public void setVilleId(Long villeId) {
        this.villeId = villeId;
    }

    public Long getAgenceId() {
        return agenceId;
    }

    public void setAgenceId(Long agenceId) {
        this.agenceId = agenceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GareDTO)) {
            return false;
        }

        return id != null && id.equals(((GareDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "GareDTO{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            ", correspondance='" + isCorrespondance() + "'" +
            ", gareAgence='" + isGareAgence() + "'" +
            ", actif='" + isActif() + "'" +
            ", villeId=" + getVilleId() +
            ", agenceId=" + getAgenceId() +
            "}";
    }
}
