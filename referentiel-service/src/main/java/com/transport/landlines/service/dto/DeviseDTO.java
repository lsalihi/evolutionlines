package com.transport.landlines.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.transport.landlines.domain.Devise} entity.
 */
public class DeviseDTO implements Serializable {
    
    private Long id;

    private String abreviationCourte;

    private String abreviationLongue;

    private String code;

    private String libelle;

    private Integer nombreApresVirgule;

    private Boolean actif;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAbreviationCourte() {
        return abreviationCourte;
    }

    public void setAbreviationCourte(String abreviationCourte) {
        this.abreviationCourte = abreviationCourte;
    }

    public String getAbreviationLongue() {
        return abreviationLongue;
    }

    public void setAbreviationLongue(String abreviationLongue) {
        this.abreviationLongue = abreviationLongue;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Integer getNombreApresVirgule() {
        return nombreApresVirgule;
    }

    public void setNombreApresVirgule(Integer nombreApresVirgule) {
        this.nombreApresVirgule = nombreApresVirgule;
    }

    public Boolean isActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DeviseDTO)) {
            return false;
        }

        return id != null && id.equals(((DeviseDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DeviseDTO{" +
            "id=" + getId() +
            ", abreviationCourte='" + getAbreviationCourte() + "'" +
            ", abreviationLongue='" + getAbreviationLongue() + "'" +
            ", code='" + getCode() + "'" +
            ", libelle='" + getLibelle() + "'" +
            ", nombreApresVirgule=" + getNombreApresVirgule() +
            ", actif='" + isActif() + "'" +
            "}";
    }
}
