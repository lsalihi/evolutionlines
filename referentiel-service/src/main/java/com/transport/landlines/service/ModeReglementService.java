package com.transport.landlines.service;

import com.transport.landlines.service.dto.ModeReglementDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.transport.landlines.domain.ModeReglement}.
 */
public interface ModeReglementService {

    /**
     * Save a modeReglement.
     *
     * @param modeReglementDTO the entity to save.
     * @return the persisted entity.
     */
    ModeReglementDTO save(ModeReglementDTO modeReglementDTO);

    /**
     * Get all the modeReglements.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ModeReglementDTO> findAll(Pageable pageable);


    /**
     * Get the "id" modeReglement.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ModeReglementDTO> findOne(Long id);

    /**
     * Delete the "id" modeReglement.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
