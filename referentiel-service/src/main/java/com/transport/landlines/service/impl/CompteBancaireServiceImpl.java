package com.transport.landlines.service.impl;

import com.transport.landlines.service.CompteBancaireService;
import com.transport.landlines.domain.CompteBancaire;
import com.transport.landlines.repository.CompteBancaireRepository;
import com.transport.landlines.service.dto.CompteBancaireDTO;
import com.transport.landlines.service.mapper.CompteBancaireMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link CompteBancaire}.
 */
@Service
@Transactional
public class CompteBancaireServiceImpl implements CompteBancaireService {

    private final Logger log = LoggerFactory.getLogger(CompteBancaireServiceImpl.class);

    private final CompteBancaireRepository compteBancaireRepository;

    private final CompteBancaireMapper compteBancaireMapper;

    public CompteBancaireServiceImpl(CompteBancaireRepository compteBancaireRepository, CompteBancaireMapper compteBancaireMapper) {
        this.compteBancaireRepository = compteBancaireRepository;
        this.compteBancaireMapper = compteBancaireMapper;
    }

    @Override
    public CompteBancaireDTO save(CompteBancaireDTO compteBancaireDTO) {
        log.debug("Request to save CompteBancaire : {}", compteBancaireDTO);
        CompteBancaire compteBancaire = compteBancaireMapper.toEntity(compteBancaireDTO);
        compteBancaire = compteBancaireRepository.save(compteBancaire);
        return compteBancaireMapper.toDto(compteBancaire);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CompteBancaireDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CompteBancaires");
        return compteBancaireRepository.findAll(pageable)
            .map(compteBancaireMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<CompteBancaireDTO> findOne(Long id) {
        log.debug("Request to get CompteBancaire : {}", id);
        return compteBancaireRepository.findById(id)
            .map(compteBancaireMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete CompteBancaire : {}", id);
        compteBancaireRepository.deleteById(id);
    }
}
