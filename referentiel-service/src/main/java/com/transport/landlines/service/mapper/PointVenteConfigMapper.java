package com.transport.landlines.service.mapper;


import com.transport.landlines.domain.*;
import com.transport.landlines.service.dto.PointVenteConfigDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link PointVenteConfig} and its DTO {@link PointVenteConfigDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PointVenteConfigMapper extends EntityMapper<PointVenteConfigDTO, PointVenteConfig> {



    default PointVenteConfig fromId(Long id) {
        if (id == null) {
            return null;
        }
        PointVenteConfig pointVenteConfig = new PointVenteConfig();
        pointVenteConfig.setId(id);
        return pointVenteConfig;
    }
}
