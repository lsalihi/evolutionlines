package com.transport.landlines.service.mapper;


import com.transport.landlines.domain.*;
import com.transport.landlines.service.dto.PieceJointeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link PieceJointe} and its DTO {@link PieceJointeDTO}.
 */
@Mapper(componentModel = "spring", uses = {CodificationMapper.class, AgenceMapper.class})
public interface PieceJointeMapper extends EntityMapper<PieceJointeDTO, PieceJointe> {

    @Mapping(source = "statutPieceJointe.id", target = "statutPieceJointeId")
    @Mapping(source = "agence.id", target = "agenceId")
    PieceJointeDTO toDto(PieceJointe pieceJointe);

    @Mapping(source = "statutPieceJointeId", target = "statutPieceJointe")
    @Mapping(source = "agenceId", target = "agence")
    PieceJointe toEntity(PieceJointeDTO pieceJointeDTO);

    default PieceJointe fromId(Long id) {
        if (id == null) {
            return null;
        }
        PieceJointe pieceJointe = new PieceJointe();
        pieceJointe.setId(id);
        return pieceJointe;
    }
}
