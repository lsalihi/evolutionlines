package com.transport.landlines.service.mapper;


import com.transport.landlines.domain.*;
import com.transport.landlines.service.dto.BanqueDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Banque} and its DTO {@link BanqueDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BanqueMapper extends EntityMapper<BanqueDTO, Banque> {



    default Banque fromId(Long id) {
        if (id == null) {
            return null;
        }
        Banque banque = new Banque();
        banque.setId(id);
        return banque;
    }
}
