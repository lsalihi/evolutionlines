package com.transport.landlines.service.impl;

import com.transport.landlines.service.TypeMessageService;
import com.transport.landlines.domain.TypeMessage;
import com.transport.landlines.repository.TypeMessageRepository;
import com.transport.landlines.service.dto.TypeMessageDTO;
import com.transport.landlines.service.mapper.TypeMessageMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link TypeMessage}.
 */
@Service
@Transactional
public class TypeMessageServiceImpl implements TypeMessageService {

    private final Logger log = LoggerFactory.getLogger(TypeMessageServiceImpl.class);

    private final TypeMessageRepository typeMessageRepository;

    private final TypeMessageMapper typeMessageMapper;

    public TypeMessageServiceImpl(TypeMessageRepository typeMessageRepository, TypeMessageMapper typeMessageMapper) {
        this.typeMessageRepository = typeMessageRepository;
        this.typeMessageMapper = typeMessageMapper;
    }

    @Override
    public TypeMessageDTO save(TypeMessageDTO typeMessageDTO) {
        log.debug("Request to save TypeMessage : {}", typeMessageDTO);
        TypeMessage typeMessage = typeMessageMapper.toEntity(typeMessageDTO);
        typeMessage = typeMessageRepository.save(typeMessage);
        return typeMessageMapper.toDto(typeMessage);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<TypeMessageDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TypeMessages");
        return typeMessageRepository.findAll(pageable)
            .map(typeMessageMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<TypeMessageDTO> findOne(Long id) {
        log.debug("Request to get TypeMessage : {}", id);
        return typeMessageRepository.findById(id)
            .map(typeMessageMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete TypeMessage : {}", id);
        typeMessageRepository.deleteById(id);
    }
}
