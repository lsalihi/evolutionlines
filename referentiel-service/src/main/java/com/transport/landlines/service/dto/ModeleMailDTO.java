package com.transport.landlines.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.transport.landlines.domain.ModeleMail} entity.
 */
public class ModeleMailDTO implements Serializable {
    
    private Long id;

    private String destinataire;

    private String objetMail;

    private Integer seuil;

    private Boolean actif;


    private Long typeMessageId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDestinataire() {
        return destinataire;
    }

    public void setDestinataire(String destinataire) {
        this.destinataire = destinataire;
    }

    public String getObjetMail() {
        return objetMail;
    }

    public void setObjetMail(String objetMail) {
        this.objetMail = objetMail;
    }

    public Integer getSeuil() {
        return seuil;
    }

    public void setSeuil(Integer seuil) {
        this.seuil = seuil;
    }

    public Boolean isActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public Long getTypeMessageId() {
        return typeMessageId;
    }

    public void setTypeMessageId(Long typeMessageId) {
        this.typeMessageId = typeMessageId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ModeleMailDTO)) {
            return false;
        }

        return id != null && id.equals(((ModeleMailDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ModeleMailDTO{" +
            "id=" + getId() +
            ", destinataire='" + getDestinataire() + "'" +
            ", objetMail='" + getObjetMail() + "'" +
            ", seuil=" + getSeuil() +
            ", actif='" + isActif() + "'" +
            ", typeMessageId=" + getTypeMessageId() +
            "}";
    }
}
