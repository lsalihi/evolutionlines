package com.transport.landlines.service.impl;

import com.transport.landlines.service.HoraireServiceService;
import com.transport.landlines.domain.HoraireService;
import com.transport.landlines.repository.HoraireServiceRepository;
import com.transport.landlines.service.dto.HoraireServiceDTO;
import com.transport.landlines.service.mapper.HoraireServiceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link HoraireService}.
 */
@Service
@Transactional
public class HoraireServiceServiceImpl implements HoraireServiceService {

    private final Logger log = LoggerFactory.getLogger(HoraireServiceServiceImpl.class);

    private final HoraireServiceRepository horaireServiceRepository;

    private final HoraireServiceMapper horaireServiceMapper;

    public HoraireServiceServiceImpl(HoraireServiceRepository horaireServiceRepository, HoraireServiceMapper horaireServiceMapper) {
        this.horaireServiceRepository = horaireServiceRepository;
        this.horaireServiceMapper = horaireServiceMapper;
    }

    @Override
    public HoraireServiceDTO save(HoraireServiceDTO horaireServiceDTO) {
        log.debug("Request to save HoraireService : {}", horaireServiceDTO);
        HoraireService horaireService = horaireServiceMapper.toEntity(horaireServiceDTO);
        horaireService = horaireServiceRepository.save(horaireService);
        return horaireServiceMapper.toDto(horaireService);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<HoraireServiceDTO> findAll(Pageable pageable) {
        log.debug("Request to get all HoraireServices");
        return horaireServiceRepository.findAll(pageable)
            .map(horaireServiceMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<HoraireServiceDTO> findOne(Long id) {
        log.debug("Request to get HoraireService : {}", id);
        return horaireServiceRepository.findById(id)
            .map(horaireServiceMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete HoraireService : {}", id);
        horaireServiceRepository.deleteById(id);
    }
}
