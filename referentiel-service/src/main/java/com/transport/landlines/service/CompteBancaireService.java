package com.transport.landlines.service;

import com.transport.landlines.service.dto.CompteBancaireDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.transport.landlines.domain.CompteBancaire}.
 */
public interface CompteBancaireService {

    /**
     * Save a compteBancaire.
     *
     * @param compteBancaireDTO the entity to save.
     * @return the persisted entity.
     */
    CompteBancaireDTO save(CompteBancaireDTO compteBancaireDTO);

    /**
     * Get all the compteBancaires.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CompteBancaireDTO> findAll(Pageable pageable);


    /**
     * Get the "id" compteBancaire.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CompteBancaireDTO> findOne(Long id);

    /**
     * Delete the "id" compteBancaire.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
