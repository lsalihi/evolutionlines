package com.transport.landlines.service.impl;

import com.transport.landlines.service.ModeleMailService;
import com.transport.landlines.domain.ModeleMail;
import com.transport.landlines.repository.ModeleMailRepository;
import com.transport.landlines.service.dto.ModeleMailDTO;
import com.transport.landlines.service.mapper.ModeleMailMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ModeleMail}.
 */
@Service
@Transactional
public class ModeleMailServiceImpl implements ModeleMailService {

    private final Logger log = LoggerFactory.getLogger(ModeleMailServiceImpl.class);

    private final ModeleMailRepository modeleMailRepository;

    private final ModeleMailMapper modeleMailMapper;

    public ModeleMailServiceImpl(ModeleMailRepository modeleMailRepository, ModeleMailMapper modeleMailMapper) {
        this.modeleMailRepository = modeleMailRepository;
        this.modeleMailMapper = modeleMailMapper;
    }

    @Override
    public ModeleMailDTO save(ModeleMailDTO modeleMailDTO) {
        log.debug("Request to save ModeleMail : {}", modeleMailDTO);
        ModeleMail modeleMail = modeleMailMapper.toEntity(modeleMailDTO);
        modeleMail = modeleMailRepository.save(modeleMail);
        return modeleMailMapper.toDto(modeleMail);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ModeleMailDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ModeleMails");
        return modeleMailRepository.findAll(pageable)
            .map(modeleMailMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<ModeleMailDTO> findOne(Long id) {
        log.debug("Request to get ModeleMail : {}", id);
        return modeleMailRepository.findById(id)
            .map(modeleMailMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ModeleMail : {}", id);
        modeleMailRepository.deleteById(id);
    }
}
