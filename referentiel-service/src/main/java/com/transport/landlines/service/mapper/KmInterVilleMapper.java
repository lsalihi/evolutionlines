package com.transport.landlines.service.mapper;


import com.transport.landlines.domain.*;
import com.transport.landlines.service.dto.KmInterVilleDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link KmInterVille} and its DTO {@link KmInterVilleDTO}.
 */
@Mapper(componentModel = "spring", uses = {VilleMapper.class})
public interface KmInterVilleMapper extends EntityMapper<KmInterVilleDTO, KmInterVille> {

    @Mapping(source = "villeDepart.id", target = "villeDepartId")
    @Mapping(source = "villeArrivee.id", target = "villeArriveeId")
    KmInterVilleDTO toDto(KmInterVille kmInterVille);

    @Mapping(source = "villeDepartId", target = "villeDepart")
    @Mapping(source = "villeArriveeId", target = "villeArrivee")
    KmInterVille toEntity(KmInterVilleDTO kmInterVilleDTO);

    default KmInterVille fromId(Long id) {
        if (id == null) {
            return null;
        }
        KmInterVille kmInterVille = new KmInterVille();
        kmInterVille.setId(id);
        return kmInterVille;
    }
}
