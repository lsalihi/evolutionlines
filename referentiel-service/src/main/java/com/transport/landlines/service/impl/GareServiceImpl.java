package com.transport.landlines.service.impl;

import com.transport.landlines.service.GareService;
import com.transport.landlines.domain.Gare;
import com.transport.landlines.repository.GareRepository;
import com.transport.landlines.service.dto.GareDTO;
import com.transport.landlines.service.mapper.GareMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Gare}.
 */
@Service
@Transactional
public class GareServiceImpl implements GareService {

    private final Logger log = LoggerFactory.getLogger(GareServiceImpl.class);

    private final GareRepository gareRepository;

    private final GareMapper gareMapper;

    public GareServiceImpl(GareRepository gareRepository, GareMapper gareMapper) {
        this.gareRepository = gareRepository;
        this.gareMapper = gareMapper;
    }

    @Override
    public GareDTO save(GareDTO gareDTO) {
        log.debug("Request to save Gare : {}", gareDTO);
        Gare gare = gareMapper.toEntity(gareDTO);
        gare = gareRepository.save(gare);
        return gareMapper.toDto(gare);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<GareDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Gares");
        return gareRepository.findAll(pageable)
            .map(gareMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<GareDTO> findOne(Long id) {
        log.debug("Request to get Gare : {}", id);
        return gareRepository.findById(id)
            .map(gareMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Gare : {}", id);
        gareRepository.deleteById(id);
    }
}
