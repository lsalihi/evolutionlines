package com.transport.landlines.service.impl;

import com.transport.landlines.service.PointVenteConfigService;
import com.transport.landlines.domain.PointVenteConfig;
import com.transport.landlines.repository.PointVenteConfigRepository;
import com.transport.landlines.service.dto.PointVenteConfigDTO;
import com.transport.landlines.service.mapper.PointVenteConfigMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link PointVenteConfig}.
 */
@Service
@Transactional
public class PointVenteConfigServiceImpl implements PointVenteConfigService {

    private final Logger log = LoggerFactory.getLogger(PointVenteConfigServiceImpl.class);

    private final PointVenteConfigRepository pointVenteConfigRepository;

    private final PointVenteConfigMapper pointVenteConfigMapper;

    public PointVenteConfigServiceImpl(PointVenteConfigRepository pointVenteConfigRepository, PointVenteConfigMapper pointVenteConfigMapper) {
        this.pointVenteConfigRepository = pointVenteConfigRepository;
        this.pointVenteConfigMapper = pointVenteConfigMapper;
    }

    @Override
    public PointVenteConfigDTO save(PointVenteConfigDTO pointVenteConfigDTO) {
        log.debug("Request to save PointVenteConfig : {}", pointVenteConfigDTO);
        PointVenteConfig pointVenteConfig = pointVenteConfigMapper.toEntity(pointVenteConfigDTO);
        pointVenteConfig = pointVenteConfigRepository.save(pointVenteConfig);
        return pointVenteConfigMapper.toDto(pointVenteConfig);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PointVenteConfigDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PointVenteConfigs");
        return pointVenteConfigRepository.findAll(pageable)
            .map(pointVenteConfigMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<PointVenteConfigDTO> findOne(Long id) {
        log.debug("Request to get PointVenteConfig : {}", id);
        return pointVenteConfigRepository.findById(id)
            .map(pointVenteConfigMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete PointVenteConfig : {}", id);
        pointVenteConfigRepository.deleteById(id);
    }
}
