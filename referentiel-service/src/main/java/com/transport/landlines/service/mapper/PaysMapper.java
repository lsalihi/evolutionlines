package com.transport.landlines.service.mapper;


import com.transport.landlines.domain.*;
import com.transport.landlines.service.dto.PaysDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Pays} and its DTO {@link PaysDTO}.
 */
@Mapper(componentModel = "spring", uses = {DeviseMapper.class})
public interface PaysMapper extends EntityMapper<PaysDTO, Pays> {

    @Mapping(source = "devise.id", target = "deviseId")
    PaysDTO toDto(Pays pays);

    @Mapping(source = "deviseId", target = "devise")
    Pays toEntity(PaysDTO paysDTO);

    default Pays fromId(Long id) {
        if (id == null) {
            return null;
        }
        Pays pays = new Pays();
        pays.setId(id);
        return pays;
    }
}
