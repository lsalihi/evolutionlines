package com.transport.landlines.service.mapper;


import com.transport.landlines.domain.*;
import com.transport.landlines.service.dto.NationaliteDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Nationalite} and its DTO {@link NationaliteDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface NationaliteMapper extends EntityMapper<NationaliteDTO, Nationalite> {



    default Nationalite fromId(Long id) {
        if (id == null) {
            return null;
        }
        Nationalite nationalite = new Nationalite();
        nationalite.setId(id);
        return nationalite;
    }
}
