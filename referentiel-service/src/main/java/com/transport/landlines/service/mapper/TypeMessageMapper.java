package com.transport.landlines.service.mapper;


import com.transport.landlines.domain.*;
import com.transport.landlines.service.dto.TypeMessageDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TypeMessage} and its DTO {@link TypeMessageDTO}.
 */
@Mapper(componentModel = "spring", uses = {CodificationMapper.class})
public interface TypeMessageMapper extends EntityMapper<TypeMessageDTO, TypeMessage> {

    @Mapping(source = "criticite.id", target = "criticiteId")
    TypeMessageDTO toDto(TypeMessage typeMessage);

    @Mapping(source = "criticiteId", target = "criticite")
    TypeMessage toEntity(TypeMessageDTO typeMessageDTO);

    default TypeMessage fromId(Long id) {
        if (id == null) {
            return null;
        }
        TypeMessage typeMessage = new TypeMessage();
        typeMessage.setId(id);
        return typeMessage;
    }
}
