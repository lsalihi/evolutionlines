package com.transport.landlines.service.mapper;


import com.transport.landlines.domain.*;
import com.transport.landlines.service.dto.DeviseDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Devise} and its DTO {@link DeviseDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DeviseMapper extends EntityMapper<DeviseDTO, Devise> {



    default Devise fromId(Long id) {
        if (id == null) {
            return null;
        }
        Devise devise = new Devise();
        devise.setId(id);
        return devise;
    }
}
