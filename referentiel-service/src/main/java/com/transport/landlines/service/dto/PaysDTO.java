package com.transport.landlines.service.dto;

import java.time.LocalDate;
import java.io.Serializable;

/**
 * A DTO for the {@link com.transport.landlines.domain.Pays} entity.
 */
public class PaysDTO implements Serializable {
    
    private Long id;

    private String abreviation;

    private String codeAlphanum;

    private String codeNumerique;

    private LocalDate dateDebutHeureEte;

    private LocalDate dateFinHeureEte;

    private Boolean heureEte;

    private String decalageHoraire;

    private String libelle;

    private Boolean actif;


    private Long deviseId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAbreviation() {
        return abreviation;
    }

    public void setAbreviation(String abreviation) {
        this.abreviation = abreviation;
    }

    public String getCodeAlphanum() {
        return codeAlphanum;
    }

    public void setCodeAlphanum(String codeAlphanum) {
        this.codeAlphanum = codeAlphanum;
    }

    public String getCodeNumerique() {
        return codeNumerique;
    }

    public void setCodeNumerique(String codeNumerique) {
        this.codeNumerique = codeNumerique;
    }

    public LocalDate getDateDebutHeureEte() {
        return dateDebutHeureEte;
    }

    public void setDateDebutHeureEte(LocalDate dateDebutHeureEte) {
        this.dateDebutHeureEte = dateDebutHeureEte;
    }

    public LocalDate getDateFinHeureEte() {
        return dateFinHeureEte;
    }

    public void setDateFinHeureEte(LocalDate dateFinHeureEte) {
        this.dateFinHeureEte = dateFinHeureEte;
    }

    public Boolean isHeureEte() {
        return heureEte;
    }

    public void setHeureEte(Boolean heureEte) {
        this.heureEte = heureEte;
    }

    public String getDecalageHoraire() {
        return decalageHoraire;
    }

    public void setDecalageHoraire(String decalageHoraire) {
        this.decalageHoraire = decalageHoraire;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Boolean isActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public Long getDeviseId() {
        return deviseId;
    }

    public void setDeviseId(Long deviseId) {
        this.deviseId = deviseId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PaysDTO)) {
            return false;
        }

        return id != null && id.equals(((PaysDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PaysDTO{" +
            "id=" + getId() +
            ", abreviation='" + getAbreviation() + "'" +
            ", codeAlphanum='" + getCodeAlphanum() + "'" +
            ", codeNumerique='" + getCodeNumerique() + "'" +
            ", dateDebutHeureEte='" + getDateDebutHeureEte() + "'" +
            ", dateFinHeureEte='" + getDateFinHeureEte() + "'" +
            ", heureEte='" + isHeureEte() + "'" +
            ", decalageHoraire='" + getDecalageHoraire() + "'" +
            ", libelle='" + getLibelle() + "'" +
            ", actif='" + isActif() + "'" +
            ", deviseId=" + getDeviseId() +
            "}";
    }
}
