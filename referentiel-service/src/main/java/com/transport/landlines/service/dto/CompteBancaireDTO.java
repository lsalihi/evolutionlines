package com.transport.landlines.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.transport.landlines.domain.CompteBancaire} entity.
 */
public class CompteBancaireDTO implements Serializable {
    
    private Long id;

    private String rib;


    private Long banqueId;

    private Long adresseAgenceId;

    private Long contactAgenceId;

    private Long villeAgenceId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRib() {
        return rib;
    }

    public void setRib(String rib) {
        this.rib = rib;
    }

    public Long getBanqueId() {
        return banqueId;
    }

    public void setBanqueId(Long banqueId) {
        this.banqueId = banqueId;
    }

    public Long getAdresseAgenceId() {
        return adresseAgenceId;
    }

    public void setAdresseAgenceId(Long adresseId) {
        this.adresseAgenceId = adresseId;
    }

    public Long getContactAgenceId() {
        return contactAgenceId;
    }

    public void setContactAgenceId(Long contactId) {
        this.contactAgenceId = contactId;
    }

    public Long getVilleAgenceId() {
        return villeAgenceId;
    }

    public void setVilleAgenceId(Long villeId) {
        this.villeAgenceId = villeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompteBancaireDTO)) {
            return false;
        }

        return id != null && id.equals(((CompteBancaireDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompteBancaireDTO{" +
            "id=" + getId() +
            ", rib='" + getRib() + "'" +
            ", banqueId=" + getBanqueId() +
            ", adresseAgenceId=" + getAdresseAgenceId() +
            ", contactAgenceId=" + getContactAgenceId() +
            ", villeAgenceId=" + getVilleAgenceId() +
            "}";
    }
}
