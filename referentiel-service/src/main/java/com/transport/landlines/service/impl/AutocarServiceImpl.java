package com.transport.landlines.service.impl;

import com.transport.landlines.service.AutocarService;
import com.transport.landlines.domain.Autocar;
import com.transport.landlines.repository.AutocarRepository;
import com.transport.landlines.service.dto.AutocarDTO;
import com.transport.landlines.service.mapper.AutocarMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Autocar}.
 */
@Service
@Transactional
public class AutocarServiceImpl implements AutocarService {

    private final Logger log = LoggerFactory.getLogger(AutocarServiceImpl.class);

    private final AutocarRepository autocarRepository;

    private final AutocarMapper autocarMapper;

    public AutocarServiceImpl(AutocarRepository autocarRepository, AutocarMapper autocarMapper) {
        this.autocarRepository = autocarRepository;
        this.autocarMapper = autocarMapper;
    }

    @Override
    public AutocarDTO save(AutocarDTO autocarDTO) {
        log.debug("Request to save Autocar : {}", autocarDTO);
        Autocar autocar = autocarMapper.toEntity(autocarDTO);
        autocar = autocarRepository.save(autocar);
        return autocarMapper.toDto(autocar);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<AutocarDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Autocars");
        return autocarRepository.findAll(pageable)
            .map(autocarMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<AutocarDTO> findOne(Long id) {
        log.debug("Request to get Autocar : {}", id);
        return autocarRepository.findById(id)
            .map(autocarMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Autocar : {}", id);
        autocarRepository.deleteById(id);
    }
}
