package com.transport.landlines.service.mapper;


import com.transport.landlines.domain.*;
import com.transport.landlines.service.dto.CategorieAgeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CategorieAge} and its DTO {@link CategorieAgeDTO}.
 */
@Mapper(componentModel = "spring", uses = {CodificationMapper.class})
public interface CategorieAgeMapper extends EntityMapper<CategorieAgeDTO, CategorieAge> {

    @Mapping(source = "activiteMetier.id", target = "activiteMetierId")
    CategorieAgeDTO toDto(CategorieAge categorieAge);

    @Mapping(source = "activiteMetierId", target = "activiteMetier")
    CategorieAge toEntity(CategorieAgeDTO categorieAgeDTO);

    default CategorieAge fromId(Long id) {
        if (id == null) {
            return null;
        }
        CategorieAge categorieAge = new CategorieAge();
        categorieAge.setId(id);
        return categorieAge;
    }
}
