package com.transport.landlines.service.mapper;


import com.transport.landlines.domain.*;
import com.transport.landlines.service.dto.ModeleMailDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ModeleMail} and its DTO {@link ModeleMailDTO}.
 */
@Mapper(componentModel = "spring", uses = {TypeMessageMapper.class})
public interface ModeleMailMapper extends EntityMapper<ModeleMailDTO, ModeleMail> {

    @Mapping(source = "typeMessage.id", target = "typeMessageId")
    ModeleMailDTO toDto(ModeleMail modeleMail);

    @Mapping(source = "typeMessageId", target = "typeMessage")
    ModeleMail toEntity(ModeleMailDTO modeleMailDTO);

    default ModeleMail fromId(Long id) {
        if (id == null) {
            return null;
        }
        ModeleMail modeleMail = new ModeleMail();
        modeleMail.setId(id);
        return modeleMail;
    }
}
