package com.transport.landlines.service.impl;

import com.transport.landlines.service.NationaliteService;
import com.transport.landlines.domain.Nationalite;
import com.transport.landlines.repository.NationaliteRepository;
import com.transport.landlines.service.dto.NationaliteDTO;
import com.transport.landlines.service.mapper.NationaliteMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Nationalite}.
 */
@Service
@Transactional
public class NationaliteServiceImpl implements NationaliteService {

    private final Logger log = LoggerFactory.getLogger(NationaliteServiceImpl.class);

    private final NationaliteRepository nationaliteRepository;

    private final NationaliteMapper nationaliteMapper;

    public NationaliteServiceImpl(NationaliteRepository nationaliteRepository, NationaliteMapper nationaliteMapper) {
        this.nationaliteRepository = nationaliteRepository;
        this.nationaliteMapper = nationaliteMapper;
    }

    @Override
    public NationaliteDTO save(NationaliteDTO nationaliteDTO) {
        log.debug("Request to save Nationalite : {}", nationaliteDTO);
        Nationalite nationalite = nationaliteMapper.toEntity(nationaliteDTO);
        nationalite = nationaliteRepository.save(nationalite);
        return nationaliteMapper.toDto(nationalite);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<NationaliteDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Nationalites");
        return nationaliteRepository.findAll(pageable)
            .map(nationaliteMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<NationaliteDTO> findOne(Long id) {
        log.debug("Request to get Nationalite : {}", id);
        return nationaliteRepository.findById(id)
            .map(nationaliteMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Nationalite : {}", id);
        nationaliteRepository.deleteById(id);
    }
}
