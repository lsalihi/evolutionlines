package com.transport.landlines.service;

import com.transport.landlines.service.dto.KmInterVilleDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.transport.landlines.domain.KmInterVille}.
 */
public interface KmInterVilleService {

    /**
     * Save a kmInterVille.
     *
     * @param kmInterVilleDTO the entity to save.
     * @return the persisted entity.
     */
    KmInterVilleDTO save(KmInterVilleDTO kmInterVilleDTO);

    /**
     * Get all the kmInterVilles.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<KmInterVilleDTO> findAll(Pageable pageable);


    /**
     * Get the "id" kmInterVille.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<KmInterVilleDTO> findOne(Long id);

    /**
     * Delete the "id" kmInterVille.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
