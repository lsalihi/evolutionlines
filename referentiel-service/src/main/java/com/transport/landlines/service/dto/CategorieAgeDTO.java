package com.transport.landlines.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.transport.landlines.domain.CategorieAge} entity.
 */
public class CategorieAgeDTO implements Serializable {
    
    private Long id;

    private String libelle;

    private String description;

    private Integer minAge;

    private Integer maxAge;


    private Long activiteMetierId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getMinAge() {
        return minAge;
    }

    public void setMinAge(Integer minAge) {
        this.minAge = minAge;
    }

    public Integer getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(Integer maxAge) {
        this.maxAge = maxAge;
    }

    public Long getActiviteMetierId() {
        return activiteMetierId;
    }

    public void setActiviteMetierId(Long codificationId) {
        this.activiteMetierId = codificationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CategorieAgeDTO)) {
            return false;
        }

        return id != null && id.equals(((CategorieAgeDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CategorieAgeDTO{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            ", description='" + getDescription() + "'" +
            ", minAge=" + getMinAge() +
            ", maxAge=" + getMaxAge() +
            ", activiteMetierId=" + getActiviteMetierId() +
            "}";
    }
}
