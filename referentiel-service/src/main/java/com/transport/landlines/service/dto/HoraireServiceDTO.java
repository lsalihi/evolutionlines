package com.transport.landlines.service.dto;

import java.time.LocalDate;
import java.io.Serializable;

/**
 * A DTO for the {@link com.transport.landlines.domain.HoraireService} entity.
 */
public class HoraireServiceDTO implements Serializable {
    
    private Long id;

    private LocalDate ouverture;

    private LocalDate fermeture;


    private Long agenceId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getOuverture() {
        return ouverture;
    }

    public void setOuverture(LocalDate ouverture) {
        this.ouverture = ouverture;
    }

    public LocalDate getFermeture() {
        return fermeture;
    }

    public void setFermeture(LocalDate fermeture) {
        this.fermeture = fermeture;
    }

    public Long getAgenceId() {
        return agenceId;
    }

    public void setAgenceId(Long agenceId) {
        this.agenceId = agenceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HoraireServiceDTO)) {
            return false;
        }

        return id != null && id.equals(((HoraireServiceDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "HoraireServiceDTO{" +
            "id=" + getId() +
            ", ouverture='" + getOuverture() + "'" +
            ", fermeture='" + getFermeture() + "'" +
            ", agenceId=" + getAgenceId() +
            "}";
    }
}
