package com.transport.landlines.service.impl;

import com.transport.landlines.service.BanqueService;
import com.transport.landlines.domain.Banque;
import com.transport.landlines.repository.BanqueRepository;
import com.transport.landlines.service.dto.BanqueDTO;
import com.transport.landlines.service.mapper.BanqueMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Banque}.
 */
@Service
@Transactional
public class BanqueServiceImpl implements BanqueService {

    private final Logger log = LoggerFactory.getLogger(BanqueServiceImpl.class);

    private final BanqueRepository banqueRepository;

    private final BanqueMapper banqueMapper;

    public BanqueServiceImpl(BanqueRepository banqueRepository, BanqueMapper banqueMapper) {
        this.banqueRepository = banqueRepository;
        this.banqueMapper = banqueMapper;
    }

    @Override
    public BanqueDTO save(BanqueDTO banqueDTO) {
        log.debug("Request to save Banque : {}", banqueDTO);
        Banque banque = banqueMapper.toEntity(banqueDTO);
        banque = banqueRepository.save(banque);
        return banqueMapper.toDto(banque);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<BanqueDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Banques");
        return banqueRepository.findAll(pageable)
            .map(banqueMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<BanqueDTO> findOne(Long id) {
        log.debug("Request to get Banque : {}", id);
        return banqueRepository.findById(id)
            .map(banqueMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Banque : {}", id);
        banqueRepository.deleteById(id);
    }
}
