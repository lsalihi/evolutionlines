package com.transport.landlines.service.mapper;


import com.transport.landlines.domain.*;
import com.transport.landlines.service.dto.HoraireServiceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link HoraireService} and its DTO {@link HoraireServiceDTO}.
 */
@Mapper(componentModel = "spring", uses = {AgenceMapper.class})
public interface HoraireServiceMapper extends EntityMapper<HoraireServiceDTO, HoraireService> {

    @Mapping(source = "agence.id", target = "agenceId")
    HoraireServiceDTO toDto(HoraireService horaireService);

    @Mapping(source = "agenceId", target = "agence")
    HoraireService toEntity(HoraireServiceDTO horaireServiceDTO);

    default HoraireService fromId(Long id) {
        if (id == null) {
            return null;
        }
        HoraireService horaireService = new HoraireService();
        horaireService.setId(id);
        return horaireService;
    }
}
