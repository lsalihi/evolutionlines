package com.transport.landlines.service.impl;

import com.transport.landlines.service.CodificationService;
import com.transport.landlines.domain.Codification;
import com.transport.landlines.repository.CodificationRepository;
import com.transport.landlines.service.dto.CodificationDTO;
import com.transport.landlines.service.mapper.CodificationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Codification}.
 */
@Service
@Transactional
public class CodificationServiceImpl implements CodificationService {

    private final Logger log = LoggerFactory.getLogger(CodificationServiceImpl.class);

    private final CodificationRepository codificationRepository;

    private final CodificationMapper codificationMapper;

    public CodificationServiceImpl(CodificationRepository codificationRepository, CodificationMapper codificationMapper) {
        this.codificationRepository = codificationRepository;
        this.codificationMapper = codificationMapper;
    }

    @Override
    public CodificationDTO save(CodificationDTO codificationDTO) {
        log.debug("Request to save Codification : {}", codificationDTO);
        Codification codification = codificationMapper.toEntity(codificationDTO);
        codification = codificationRepository.save(codification);
        return codificationMapper.toDto(codification);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CodificationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Codifications");
        return codificationRepository.findAll(pageable)
            .map(codificationMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<CodificationDTO> findOne(Long id) {
        log.debug("Request to get Codification : {}", id);
        return codificationRepository.findById(id)
            .map(codificationMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Codification : {}", id);
        codificationRepository.deleteById(id);
    }
}
