package com.transport.landlines.service;

import com.transport.landlines.service.dto.CodificationDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.transport.landlines.domain.Codification}.
 */
public interface CodificationService {

    /**
     * Save a codification.
     *
     * @param codificationDTO the entity to save.
     * @return the persisted entity.
     */
    CodificationDTO save(CodificationDTO codificationDTO);

    /**
     * Get all the codifications.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CodificationDTO> findAll(Pageable pageable);


    /**
     * Get the "id" codification.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CodificationDTO> findOne(Long id);

    /**
     * Delete the "id" codification.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
