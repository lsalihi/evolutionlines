package com.transport.landlines.service.impl;

import com.transport.landlines.service.ModeReglementService;
import com.transport.landlines.domain.ModeReglement;
import com.transport.landlines.repository.ModeReglementRepository;
import com.transport.landlines.service.dto.ModeReglementDTO;
import com.transport.landlines.service.mapper.ModeReglementMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ModeReglement}.
 */
@Service
@Transactional
public class ModeReglementServiceImpl implements ModeReglementService {

    private final Logger log = LoggerFactory.getLogger(ModeReglementServiceImpl.class);

    private final ModeReglementRepository modeReglementRepository;

    private final ModeReglementMapper modeReglementMapper;

    public ModeReglementServiceImpl(ModeReglementRepository modeReglementRepository, ModeReglementMapper modeReglementMapper) {
        this.modeReglementRepository = modeReglementRepository;
        this.modeReglementMapper = modeReglementMapper;
    }

    @Override
    public ModeReglementDTO save(ModeReglementDTO modeReglementDTO) {
        log.debug("Request to save ModeReglement : {}", modeReglementDTO);
        ModeReglement modeReglement = modeReglementMapper.toEntity(modeReglementDTO);
        modeReglement = modeReglementRepository.save(modeReglement);
        return modeReglementMapper.toDto(modeReglement);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ModeReglementDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ModeReglements");
        return modeReglementRepository.findAll(pageable)
            .map(modeReglementMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<ModeReglementDTO> findOne(Long id) {
        log.debug("Request to get ModeReglement : {}", id);
        return modeReglementRepository.findById(id)
            .map(modeReglementMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ModeReglement : {}", id);
        modeReglementRepository.deleteById(id);
    }
}
