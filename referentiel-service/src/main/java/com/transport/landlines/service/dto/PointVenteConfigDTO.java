package com.transport.landlines.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.transport.landlines.domain.PointVenteConfig} entity.
 */
public class PointVenteConfigDTO implements Serializable {
    
    private Long id;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PointVenteConfigDTO)) {
            return false;
        }

        return id != null && id.equals(((PointVenteConfigDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PointVenteConfigDTO{" +
            "id=" + getId() +
            "}";
    }
}
