package com.transport.landlines.service;

import com.transport.landlines.service.dto.PointVenteConfigDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.transport.landlines.domain.PointVenteConfig}.
 */
public interface PointVenteConfigService {

    /**
     * Save a pointVenteConfig.
     *
     * @param pointVenteConfigDTO the entity to save.
     * @return the persisted entity.
     */
    PointVenteConfigDTO save(PointVenteConfigDTO pointVenteConfigDTO);

    /**
     * Get all the pointVenteConfigs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PointVenteConfigDTO> findAll(Pageable pageable);


    /**
     * Get the "id" pointVenteConfig.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PointVenteConfigDTO> findOne(Long id);

    /**
     * Delete the "id" pointVenteConfig.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
