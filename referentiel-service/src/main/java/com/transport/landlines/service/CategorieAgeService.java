package com.transport.landlines.service;

import com.transport.landlines.service.dto.CategorieAgeDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.transport.landlines.domain.CategorieAge}.
 */
public interface CategorieAgeService {

    /**
     * Save a categorieAge.
     *
     * @param categorieAgeDTO the entity to save.
     * @return the persisted entity.
     */
    CategorieAgeDTO save(CategorieAgeDTO categorieAgeDTO);

    /**
     * Get all the categorieAges.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CategorieAgeDTO> findAll(Pageable pageable);


    /**
     * Get the "id" categorieAge.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CategorieAgeDTO> findOne(Long id);

    /**
     * Delete the "id" categorieAge.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
