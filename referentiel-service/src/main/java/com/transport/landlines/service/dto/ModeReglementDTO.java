package com.transport.landlines.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.transport.landlines.domain.ModeReglement} entity.
 */
public class ModeReglementDTO implements Serializable {
    
    private Long id;

    private String libelle;

    private String description;

    private Boolean actif;


    private Long modeReglementId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean isActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public Long getModeReglementId() {
        return modeReglementId;
    }

    public void setModeReglementId(Long codificationId) {
        this.modeReglementId = codificationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ModeReglementDTO)) {
            return false;
        }

        return id != null && id.equals(((ModeReglementDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ModeReglementDTO{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            ", description='" + getDescription() + "'" +
            ", actif='" + isActif() + "'" +
            ", modeReglementId=" + getModeReglementId() +
            "}";
    }
}
