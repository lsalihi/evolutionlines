package com.transport.landlines.service;

import com.transport.landlines.service.dto.HoraireServiceDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.transport.landlines.domain.HoraireService}.
 */
public interface HoraireServiceService {

    /**
     * Save a horaireService.
     *
     * @param horaireServiceDTO the entity to save.
     * @return the persisted entity.
     */
    HoraireServiceDTO save(HoraireServiceDTO horaireServiceDTO);

    /**
     * Get all the horaireServices.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<HoraireServiceDTO> findAll(Pageable pageable);


    /**
     * Get the "id" horaireService.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<HoraireServiceDTO> findOne(Long id);

    /**
     * Delete the "id" horaireService.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
