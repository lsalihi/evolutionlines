package com.transport.landlines.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.transport.landlines.domain.Autocar} entity.
 */
public class AutocarDTO implements Serializable {
    
    private Long id;

    private String numeroSociete;

    private String marque;

    private String immatriculation;

    private Integer capacite;

    private Boolean actif;


    private Long categorieId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroSociete() {
        return numeroSociete;
    }

    public void setNumeroSociete(String numeroSociete) {
        this.numeroSociete = numeroSociete;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getImmatriculation() {
        return immatriculation;
    }

    public void setImmatriculation(String immatriculation) {
        this.immatriculation = immatriculation;
    }

    public Integer getCapacite() {
        return capacite;
    }

    public void setCapacite(Integer capacite) {
        this.capacite = capacite;
    }

    public Boolean isActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public Long getCategorieId() {
        return categorieId;
    }

    public void setCategorieId(Long codificationId) {
        this.categorieId = codificationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AutocarDTO)) {
            return false;
        }

        return id != null && id.equals(((AutocarDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AutocarDTO{" +
            "id=" + getId() +
            ", numeroSociete='" + getNumeroSociete() + "'" +
            ", marque='" + getMarque() + "'" +
            ", immatriculation='" + getImmatriculation() + "'" +
            ", capacite=" + getCapacite() +
            ", actif='" + isActif() + "'" +
            ", categorieId=" + getCategorieId() +
            "}";
    }
}
