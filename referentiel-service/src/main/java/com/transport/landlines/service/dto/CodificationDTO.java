package com.transport.landlines.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.transport.landlines.domain.Codification} entity.
 */
public class CodificationDTO implements Serializable {
    
    private Long id;

    private String codeTable;

    private String codeCodification;

    private String codeLibelle;

    private Boolean codeActif;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodeTable() {
        return codeTable;
    }

    public void setCodeTable(String codeTable) {
        this.codeTable = codeTable;
    }

    public String getCodeCodification() {
        return codeCodification;
    }

    public void setCodeCodification(String codeCodification) {
        this.codeCodification = codeCodification;
    }

    public String getCodeLibelle() {
        return codeLibelle;
    }

    public void setCodeLibelle(String codeLibelle) {
        this.codeLibelle = codeLibelle;
    }

    public Boolean isCodeActif() {
        return codeActif;
    }

    public void setCodeActif(Boolean codeActif) {
        this.codeActif = codeActif;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CodificationDTO)) {
            return false;
        }

        return id != null && id.equals(((CodificationDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CodificationDTO{" +
            "id=" + getId() +
            ", codeTable='" + getCodeTable() + "'" +
            ", codeCodification='" + getCodeCodification() + "'" +
            ", codeLibelle='" + getCodeLibelle() + "'" +
            ", codeActif='" + isCodeActif() + "'" +
            "}";
    }
}
