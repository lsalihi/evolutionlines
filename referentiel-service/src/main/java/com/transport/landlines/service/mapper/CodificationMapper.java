package com.transport.landlines.service.mapper;


import com.transport.landlines.domain.*;
import com.transport.landlines.service.dto.CodificationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Codification} and its DTO {@link CodificationDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CodificationMapper extends EntityMapper<CodificationDTO, Codification> {



    default Codification fromId(Long id) {
        if (id == null) {
            return null;
        }
        Codification codification = new Codification();
        codification.setId(id);
        return codification;
    }
}
