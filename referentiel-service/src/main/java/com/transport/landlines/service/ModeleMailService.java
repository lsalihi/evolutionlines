package com.transport.landlines.service;

import com.transport.landlines.service.dto.ModeleMailDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.transport.landlines.domain.ModeleMail}.
 */
public interface ModeleMailService {

    /**
     * Save a modeleMail.
     *
     * @param modeleMailDTO the entity to save.
     * @return the persisted entity.
     */
    ModeleMailDTO save(ModeleMailDTO modeleMailDTO);

    /**
     * Get all the modeleMails.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ModeleMailDTO> findAll(Pageable pageable);


    /**
     * Get the "id" modeleMail.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ModeleMailDTO> findOne(Long id);

    /**
     * Delete the "id" modeleMail.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
