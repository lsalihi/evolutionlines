package com.transport.landlines.service;

import com.transport.landlines.service.dto.GareDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.transport.landlines.domain.Gare}.
 */
public interface GareService {

    /**
     * Save a gare.
     *
     * @param gareDTO the entity to save.
     * @return the persisted entity.
     */
    GareDTO save(GareDTO gareDTO);

    /**
     * Get all the gares.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<GareDTO> findAll(Pageable pageable);


    /**
     * Get the "id" gare.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<GareDTO> findOne(Long id);

    /**
     * Delete the "id" gare.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
