package com.transport.landlines.service.impl;

import com.transport.landlines.service.CategorieAgeService;
import com.transport.landlines.domain.CategorieAge;
import com.transport.landlines.repository.CategorieAgeRepository;
import com.transport.landlines.service.dto.CategorieAgeDTO;
import com.transport.landlines.service.mapper.CategorieAgeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link CategorieAge}.
 */
@Service
@Transactional
public class CategorieAgeServiceImpl implements CategorieAgeService {

    private final Logger log = LoggerFactory.getLogger(CategorieAgeServiceImpl.class);

    private final CategorieAgeRepository categorieAgeRepository;

    private final CategorieAgeMapper categorieAgeMapper;

    public CategorieAgeServiceImpl(CategorieAgeRepository categorieAgeRepository, CategorieAgeMapper categorieAgeMapper) {
        this.categorieAgeRepository = categorieAgeRepository;
        this.categorieAgeMapper = categorieAgeMapper;
    }

    @Override
    public CategorieAgeDTO save(CategorieAgeDTO categorieAgeDTO) {
        log.debug("Request to save CategorieAge : {}", categorieAgeDTO);
        CategorieAge categorieAge = categorieAgeMapper.toEntity(categorieAgeDTO);
        categorieAge = categorieAgeRepository.save(categorieAge);
        return categorieAgeMapper.toDto(categorieAge);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CategorieAgeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CategorieAges");
        return categorieAgeRepository.findAll(pageable)
            .map(categorieAgeMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<CategorieAgeDTO> findOne(Long id) {
        log.debug("Request to get CategorieAge : {}", id);
        return categorieAgeRepository.findById(id)
            .map(categorieAgeMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete CategorieAge : {}", id);
        categorieAgeRepository.deleteById(id);
    }
}
