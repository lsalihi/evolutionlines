package com.transport.landlines.service.dto;

import java.time.LocalDate;
import java.io.Serializable;

/**
 * A DTO for the {@link com.transport.landlines.domain.PieceJointe} entity.
 */
public class PieceJointeDTO implements Serializable {
    
    private Long id;

    private String libelle;

    private String nomPj;

    private LocalDate dateAjout;

    private String path;

    private String fileType;

    private String description;


    private Long statutPieceJointeId;

    private Long agenceId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getNomPj() {
        return nomPj;
    }

    public void setNomPj(String nomPj) {
        this.nomPj = nomPj;
    }

    public LocalDate getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(LocalDate dateAjout) {
        this.dateAjout = dateAjout;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getStatutPieceJointeId() {
        return statutPieceJointeId;
    }

    public void setStatutPieceJointeId(Long codificationId) {
        this.statutPieceJointeId = codificationId;
    }

    public Long getAgenceId() {
        return agenceId;
    }

    public void setAgenceId(Long agenceId) {
        this.agenceId = agenceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PieceJointeDTO)) {
            return false;
        }

        return id != null && id.equals(((PieceJointeDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PieceJointeDTO{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            ", nomPj='" + getNomPj() + "'" +
            ", dateAjout='" + getDateAjout() + "'" +
            ", path='" + getPath() + "'" +
            ", fileType='" + getFileType() + "'" +
            ", description='" + getDescription() + "'" +
            ", statutPieceJointeId=" + getStatutPieceJointeId() +
            ", agenceId=" + getAgenceId() +
            "}";
    }
}
