package com.transport.landlines.service;

import com.transport.landlines.service.dto.PieceJointeDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.transport.landlines.domain.PieceJointe}.
 */
public interface PieceJointeService {

    /**
     * Save a pieceJointe.
     *
     * @param pieceJointeDTO the entity to save.
     * @return the persisted entity.
     */
    PieceJointeDTO save(PieceJointeDTO pieceJointeDTO);

    /**
     * Get all the pieceJointes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PieceJointeDTO> findAll(Pageable pageable);


    /**
     * Get the "id" pieceJointe.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PieceJointeDTO> findOne(Long id);

    /**
     * Delete the "id" pieceJointe.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
