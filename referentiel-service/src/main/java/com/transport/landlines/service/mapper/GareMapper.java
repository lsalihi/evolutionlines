package com.transport.landlines.service.mapper;


import com.transport.landlines.domain.*;
import com.transport.landlines.service.dto.GareDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Gare} and its DTO {@link GareDTO}.
 */
@Mapper(componentModel = "spring", uses = {VilleMapper.class, AgenceMapper.class})
public interface GareMapper extends EntityMapper<GareDTO, Gare> {

    @Mapping(source = "ville.id", target = "villeId")
    @Mapping(source = "agence.id", target = "agenceId")
    GareDTO toDto(Gare gare);

    @Mapping(source = "villeId", target = "ville")
    @Mapping(source = "agenceId", target = "agence")
    Gare toEntity(GareDTO gareDTO);

    default Gare fromId(Long id) {
        if (id == null) {
            return null;
        }
        Gare gare = new Gare();
        gare.setId(id);
        return gare;
    }
}
