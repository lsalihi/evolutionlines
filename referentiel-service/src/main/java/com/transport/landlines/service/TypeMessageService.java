package com.transport.landlines.service;

import com.transport.landlines.service.dto.TypeMessageDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.transport.landlines.domain.TypeMessage}.
 */
public interface TypeMessageService {

    /**
     * Save a typeMessage.
     *
     * @param typeMessageDTO the entity to save.
     * @return the persisted entity.
     */
    TypeMessageDTO save(TypeMessageDTO typeMessageDTO);

    /**
     * Get all the typeMessages.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TypeMessageDTO> findAll(Pageable pageable);


    /**
     * Get the "id" typeMessage.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TypeMessageDTO> findOne(Long id);

    /**
     * Delete the "id" typeMessage.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
