package com.transport.landlines.service;

import com.transport.landlines.service.dto.NationaliteDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.transport.landlines.domain.Nationalite}.
 */
public interface NationaliteService {

    /**
     * Save a nationalite.
     *
     * @param nationaliteDTO the entity to save.
     * @return the persisted entity.
     */
    NationaliteDTO save(NationaliteDTO nationaliteDTO);

    /**
     * Get all the nationalites.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<NationaliteDTO> findAll(Pageable pageable);


    /**
     * Get the "id" nationalite.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<NationaliteDTO> findOne(Long id);

    /**
     * Delete the "id" nationalite.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
