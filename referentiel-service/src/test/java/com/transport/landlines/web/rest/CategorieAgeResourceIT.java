package com.transport.landlines.web.rest;

import com.transport.landlines.ReferentielApp;
import com.transport.landlines.domain.CategorieAge;
import com.transport.landlines.repository.CategorieAgeRepository;
import com.transport.landlines.service.CategorieAgeService;
import com.transport.landlines.service.dto.CategorieAgeDTO;
import com.transport.landlines.service.mapper.CategorieAgeMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CategorieAgeResource} REST controller.
 */
@SpringBootTest(classes = ReferentielApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CategorieAgeResourceIT {

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_MIN_AGE = 1;
    private static final Integer UPDATED_MIN_AGE = 2;

    private static final Integer DEFAULT_MAX_AGE = 1;
    private static final Integer UPDATED_MAX_AGE = 2;

    @Autowired
    private CategorieAgeRepository categorieAgeRepository;

    @Autowired
    private CategorieAgeMapper categorieAgeMapper;

    @Autowired
    private CategorieAgeService categorieAgeService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCategorieAgeMockMvc;

    private CategorieAge categorieAge;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CategorieAge createEntity(EntityManager em) {
        CategorieAge categorieAge = new CategorieAge()
            .libelle(DEFAULT_LIBELLE)
            .description(DEFAULT_DESCRIPTION)
            .minAge(DEFAULT_MIN_AGE)
            .maxAge(DEFAULT_MAX_AGE);
        return categorieAge;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CategorieAge createUpdatedEntity(EntityManager em) {
        CategorieAge categorieAge = new CategorieAge()
            .libelle(UPDATED_LIBELLE)
            .description(UPDATED_DESCRIPTION)
            .minAge(UPDATED_MIN_AGE)
            .maxAge(UPDATED_MAX_AGE);
        return categorieAge;
    }

    @BeforeEach
    public void initTest() {
        categorieAge = createEntity(em);
    }

    @Test
    @Transactional
    public void createCategorieAge() throws Exception {
        int databaseSizeBeforeCreate = categorieAgeRepository.findAll().size();
        // Create the CategorieAge
        CategorieAgeDTO categorieAgeDTO = categorieAgeMapper.toDto(categorieAge);
        restCategorieAgeMockMvc.perform(post("/api/categorie-ages")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(categorieAgeDTO)))
            .andExpect(status().isCreated());

        // Validate the CategorieAge in the database
        List<CategorieAge> categorieAgeList = categorieAgeRepository.findAll();
        assertThat(categorieAgeList).hasSize(databaseSizeBeforeCreate + 1);
        CategorieAge testCategorieAge = categorieAgeList.get(categorieAgeList.size() - 1);
        assertThat(testCategorieAge.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
        assertThat(testCategorieAge.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testCategorieAge.getMinAge()).isEqualTo(DEFAULT_MIN_AGE);
        assertThat(testCategorieAge.getMaxAge()).isEqualTo(DEFAULT_MAX_AGE);
    }

    @Test
    @Transactional
    public void createCategorieAgeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = categorieAgeRepository.findAll().size();

        // Create the CategorieAge with an existing ID
        categorieAge.setId(1L);
        CategorieAgeDTO categorieAgeDTO = categorieAgeMapper.toDto(categorieAge);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCategorieAgeMockMvc.perform(post("/api/categorie-ages")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(categorieAgeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CategorieAge in the database
        List<CategorieAge> categorieAgeList = categorieAgeRepository.findAll();
        assertThat(categorieAgeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCategorieAges() throws Exception {
        // Initialize the database
        categorieAgeRepository.saveAndFlush(categorieAge);

        // Get all the categorieAgeList
        restCategorieAgeMockMvc.perform(get("/api/categorie-ages?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(categorieAge.getId().intValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].minAge").value(hasItem(DEFAULT_MIN_AGE)))
            .andExpect(jsonPath("$.[*].maxAge").value(hasItem(DEFAULT_MAX_AGE)));
    }
    
    @Test
    @Transactional
    public void getCategorieAge() throws Exception {
        // Initialize the database
        categorieAgeRepository.saveAndFlush(categorieAge);

        // Get the categorieAge
        restCategorieAgeMockMvc.perform(get("/api/categorie-ages/{id}", categorieAge.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(categorieAge.getId().intValue()))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.minAge").value(DEFAULT_MIN_AGE))
            .andExpect(jsonPath("$.maxAge").value(DEFAULT_MAX_AGE));
    }
    @Test
    @Transactional
    public void getNonExistingCategorieAge() throws Exception {
        // Get the categorieAge
        restCategorieAgeMockMvc.perform(get("/api/categorie-ages/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCategorieAge() throws Exception {
        // Initialize the database
        categorieAgeRepository.saveAndFlush(categorieAge);

        int databaseSizeBeforeUpdate = categorieAgeRepository.findAll().size();

        // Update the categorieAge
        CategorieAge updatedCategorieAge = categorieAgeRepository.findById(categorieAge.getId()).get();
        // Disconnect from session so that the updates on updatedCategorieAge are not directly saved in db
        em.detach(updatedCategorieAge);
        updatedCategorieAge
            .libelle(UPDATED_LIBELLE)
            .description(UPDATED_DESCRIPTION)
            .minAge(UPDATED_MIN_AGE)
            .maxAge(UPDATED_MAX_AGE);
        CategorieAgeDTO categorieAgeDTO = categorieAgeMapper.toDto(updatedCategorieAge);

        restCategorieAgeMockMvc.perform(put("/api/categorie-ages")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(categorieAgeDTO)))
            .andExpect(status().isOk());

        // Validate the CategorieAge in the database
        List<CategorieAge> categorieAgeList = categorieAgeRepository.findAll();
        assertThat(categorieAgeList).hasSize(databaseSizeBeforeUpdate);
        CategorieAge testCategorieAge = categorieAgeList.get(categorieAgeList.size() - 1);
        assertThat(testCategorieAge.getLibelle()).isEqualTo(UPDATED_LIBELLE);
        assertThat(testCategorieAge.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testCategorieAge.getMinAge()).isEqualTo(UPDATED_MIN_AGE);
        assertThat(testCategorieAge.getMaxAge()).isEqualTo(UPDATED_MAX_AGE);
    }

    @Test
    @Transactional
    public void updateNonExistingCategorieAge() throws Exception {
        int databaseSizeBeforeUpdate = categorieAgeRepository.findAll().size();

        // Create the CategorieAge
        CategorieAgeDTO categorieAgeDTO = categorieAgeMapper.toDto(categorieAge);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCategorieAgeMockMvc.perform(put("/api/categorie-ages")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(categorieAgeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CategorieAge in the database
        List<CategorieAge> categorieAgeList = categorieAgeRepository.findAll();
        assertThat(categorieAgeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCategorieAge() throws Exception {
        // Initialize the database
        categorieAgeRepository.saveAndFlush(categorieAge);

        int databaseSizeBeforeDelete = categorieAgeRepository.findAll().size();

        // Delete the categorieAge
        restCategorieAgeMockMvc.perform(delete("/api/categorie-ages/{id}", categorieAge.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CategorieAge> categorieAgeList = categorieAgeRepository.findAll();
        assertThat(categorieAgeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
