package com.transport.landlines.web.rest;

import com.transport.landlines.ReferentielApp;
import com.transport.landlines.domain.Codification;
import com.transport.landlines.repository.CodificationRepository;
import com.transport.landlines.service.CodificationService;
import com.transport.landlines.service.dto.CodificationDTO;
import com.transport.landlines.service.mapper.CodificationMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CodificationResource} REST controller.
 */
@SpringBootTest(classes = ReferentielApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CodificationResourceIT {

    private static final String DEFAULT_CODE_TABLE = "AAAAAAAAAA";
    private static final String UPDATED_CODE_TABLE = "BBBBBBBBBB";

    private static final String DEFAULT_CODE_CODIFICATION = "AAAAAAAAAA";
    private static final String UPDATED_CODE_CODIFICATION = "BBBBBBBBBB";

    private static final String DEFAULT_CODE_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_CODE_LIBELLE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_CODE_ACTIF = false;
    private static final Boolean UPDATED_CODE_ACTIF = true;

    @Autowired
    private CodificationRepository codificationRepository;

    @Autowired
    private CodificationMapper codificationMapper;

    @Autowired
    private CodificationService codificationService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCodificationMockMvc;

    private Codification codification;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Codification createEntity(EntityManager em) {
        Codification codification = new Codification()
            .codeTable(DEFAULT_CODE_TABLE)
            .codeCodification(DEFAULT_CODE_CODIFICATION)
            .codeLibelle(DEFAULT_CODE_LIBELLE)
            .codeActif(DEFAULT_CODE_ACTIF);
        return codification;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Codification createUpdatedEntity(EntityManager em) {
        Codification codification = new Codification()
            .codeTable(UPDATED_CODE_TABLE)
            .codeCodification(UPDATED_CODE_CODIFICATION)
            .codeLibelle(UPDATED_CODE_LIBELLE)
            .codeActif(UPDATED_CODE_ACTIF);
        return codification;
    }

    @BeforeEach
    public void initTest() {
        codification = createEntity(em);
    }

    @Test
    @Transactional
    public void createCodification() throws Exception {
        int databaseSizeBeforeCreate = codificationRepository.findAll().size();
        // Create the Codification
        CodificationDTO codificationDTO = codificationMapper.toDto(codification);
        restCodificationMockMvc.perform(post("/api/codifications")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(codificationDTO)))
            .andExpect(status().isCreated());

        // Validate the Codification in the database
        List<Codification> codificationList = codificationRepository.findAll();
        assertThat(codificationList).hasSize(databaseSizeBeforeCreate + 1);
        Codification testCodification = codificationList.get(codificationList.size() - 1);
        assertThat(testCodification.getCodeTable()).isEqualTo(DEFAULT_CODE_TABLE);
        assertThat(testCodification.getCodeCodification()).isEqualTo(DEFAULT_CODE_CODIFICATION);
        assertThat(testCodification.getCodeLibelle()).isEqualTo(DEFAULT_CODE_LIBELLE);
        assertThat(testCodification.isCodeActif()).isEqualTo(DEFAULT_CODE_ACTIF);
    }

    @Test
    @Transactional
    public void createCodificationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = codificationRepository.findAll().size();

        // Create the Codification with an existing ID
        codification.setId(1L);
        CodificationDTO codificationDTO = codificationMapper.toDto(codification);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCodificationMockMvc.perform(post("/api/codifications")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(codificationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Codification in the database
        List<Codification> codificationList = codificationRepository.findAll();
        assertThat(codificationList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCodifications() throws Exception {
        // Initialize the database
        codificationRepository.saveAndFlush(codification);

        // Get all the codificationList
        restCodificationMockMvc.perform(get("/api/codifications?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(codification.getId().intValue())))
            .andExpect(jsonPath("$.[*].codeTable").value(hasItem(DEFAULT_CODE_TABLE)))
            .andExpect(jsonPath("$.[*].codeCodification").value(hasItem(DEFAULT_CODE_CODIFICATION)))
            .andExpect(jsonPath("$.[*].codeLibelle").value(hasItem(DEFAULT_CODE_LIBELLE)))
            .andExpect(jsonPath("$.[*].codeActif").value(hasItem(DEFAULT_CODE_ACTIF.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getCodification() throws Exception {
        // Initialize the database
        codificationRepository.saveAndFlush(codification);

        // Get the codification
        restCodificationMockMvc.perform(get("/api/codifications/{id}", codification.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(codification.getId().intValue()))
            .andExpect(jsonPath("$.codeTable").value(DEFAULT_CODE_TABLE))
            .andExpect(jsonPath("$.codeCodification").value(DEFAULT_CODE_CODIFICATION))
            .andExpect(jsonPath("$.codeLibelle").value(DEFAULT_CODE_LIBELLE))
            .andExpect(jsonPath("$.codeActif").value(DEFAULT_CODE_ACTIF.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingCodification() throws Exception {
        // Get the codification
        restCodificationMockMvc.perform(get("/api/codifications/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCodification() throws Exception {
        // Initialize the database
        codificationRepository.saveAndFlush(codification);

        int databaseSizeBeforeUpdate = codificationRepository.findAll().size();

        // Update the codification
        Codification updatedCodification = codificationRepository.findById(codification.getId()).get();
        // Disconnect from session so that the updates on updatedCodification are not directly saved in db
        em.detach(updatedCodification);
        updatedCodification
            .codeTable(UPDATED_CODE_TABLE)
            .codeCodification(UPDATED_CODE_CODIFICATION)
            .codeLibelle(UPDATED_CODE_LIBELLE)
            .codeActif(UPDATED_CODE_ACTIF);
        CodificationDTO codificationDTO = codificationMapper.toDto(updatedCodification);

        restCodificationMockMvc.perform(put("/api/codifications")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(codificationDTO)))
            .andExpect(status().isOk());

        // Validate the Codification in the database
        List<Codification> codificationList = codificationRepository.findAll();
        assertThat(codificationList).hasSize(databaseSizeBeforeUpdate);
        Codification testCodification = codificationList.get(codificationList.size() - 1);
        assertThat(testCodification.getCodeTable()).isEqualTo(UPDATED_CODE_TABLE);
        assertThat(testCodification.getCodeCodification()).isEqualTo(UPDATED_CODE_CODIFICATION);
        assertThat(testCodification.getCodeLibelle()).isEqualTo(UPDATED_CODE_LIBELLE);
        assertThat(testCodification.isCodeActif()).isEqualTo(UPDATED_CODE_ACTIF);
    }

    @Test
    @Transactional
    public void updateNonExistingCodification() throws Exception {
        int databaseSizeBeforeUpdate = codificationRepository.findAll().size();

        // Create the Codification
        CodificationDTO codificationDTO = codificationMapper.toDto(codification);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCodificationMockMvc.perform(put("/api/codifications")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(codificationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Codification in the database
        List<Codification> codificationList = codificationRepository.findAll();
        assertThat(codificationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCodification() throws Exception {
        // Initialize the database
        codificationRepository.saveAndFlush(codification);

        int databaseSizeBeforeDelete = codificationRepository.findAll().size();

        // Delete the codification
        restCodificationMockMvc.perform(delete("/api/codifications/{id}", codification.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Codification> codificationList = codificationRepository.findAll();
        assertThat(codificationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
