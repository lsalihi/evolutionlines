package com.transport.landlines.web.rest;

import com.transport.landlines.ReferentielApp;
import com.transport.landlines.domain.Pays;
import com.transport.landlines.repository.PaysRepository;
import com.transport.landlines.service.PaysService;
import com.transport.landlines.service.dto.PaysDTO;
import com.transport.landlines.service.mapper.PaysMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PaysResource} REST controller.
 */
@SpringBootTest(classes = ReferentielApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class PaysResourceIT {

    private static final String DEFAULT_ABREVIATION = "AAAAAAAAAA";
    private static final String UPDATED_ABREVIATION = "BBBBBBBBBB";

    private static final String DEFAULT_CODE_ALPHANUM = "AAAAAAAAAA";
    private static final String UPDATED_CODE_ALPHANUM = "BBBBBBBBBB";

    private static final String DEFAULT_CODE_NUMERIQUE = "AAAAAAAAAA";
    private static final String UPDATED_CODE_NUMERIQUE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_DEBUT_HEURE_ETE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_DEBUT_HEURE_ETE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DATE_FIN_HEURE_ETE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_FIN_HEURE_ETE = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_HEURE_ETE = false;
    private static final Boolean UPDATED_HEURE_ETE = true;

    private static final String DEFAULT_DECALAGE_HORAIRE = "AAAAAAAAAA";
    private static final String UPDATED_DECALAGE_HORAIRE = "BBBBBBBBBB";

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIF = false;
    private static final Boolean UPDATED_ACTIF = true;

    @Autowired
    private PaysRepository paysRepository;

    @Autowired
    private PaysMapper paysMapper;

    @Autowired
    private PaysService paysService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPaysMockMvc;

    private Pays pays;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pays createEntity(EntityManager em) {
        Pays pays = new Pays()
            .abreviation(DEFAULT_ABREVIATION)
            .codeAlphanum(DEFAULT_CODE_ALPHANUM)
            .codeNumerique(DEFAULT_CODE_NUMERIQUE)
            .dateDebutHeureEte(DEFAULT_DATE_DEBUT_HEURE_ETE)
            .dateFinHeureEte(DEFAULT_DATE_FIN_HEURE_ETE)
            .heureEte(DEFAULT_HEURE_ETE)
            .decalageHoraire(DEFAULT_DECALAGE_HORAIRE)
            .libelle(DEFAULT_LIBELLE)
            .actif(DEFAULT_ACTIF);
        return pays;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pays createUpdatedEntity(EntityManager em) {
        Pays pays = new Pays()
            .abreviation(UPDATED_ABREVIATION)
            .codeAlphanum(UPDATED_CODE_ALPHANUM)
            .codeNumerique(UPDATED_CODE_NUMERIQUE)
            .dateDebutHeureEte(UPDATED_DATE_DEBUT_HEURE_ETE)
            .dateFinHeureEte(UPDATED_DATE_FIN_HEURE_ETE)
            .heureEte(UPDATED_HEURE_ETE)
            .decalageHoraire(UPDATED_DECALAGE_HORAIRE)
            .libelle(UPDATED_LIBELLE)
            .actif(UPDATED_ACTIF);
        return pays;
    }

    @BeforeEach
    public void initTest() {
        pays = createEntity(em);
    }

    @Test
    @Transactional
    public void createPays() throws Exception {
        int databaseSizeBeforeCreate = paysRepository.findAll().size();
        // Create the Pays
        PaysDTO paysDTO = paysMapper.toDto(pays);
        restPaysMockMvc.perform(post("/api/pays")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(paysDTO)))
            .andExpect(status().isCreated());

        // Validate the Pays in the database
        List<Pays> paysList = paysRepository.findAll();
        assertThat(paysList).hasSize(databaseSizeBeforeCreate + 1);
        Pays testPays = paysList.get(paysList.size() - 1);
        assertThat(testPays.getAbreviation()).isEqualTo(DEFAULT_ABREVIATION);
        assertThat(testPays.getCodeAlphanum()).isEqualTo(DEFAULT_CODE_ALPHANUM);
        assertThat(testPays.getCodeNumerique()).isEqualTo(DEFAULT_CODE_NUMERIQUE);
        assertThat(testPays.getDateDebutHeureEte()).isEqualTo(DEFAULT_DATE_DEBUT_HEURE_ETE);
        assertThat(testPays.getDateFinHeureEte()).isEqualTo(DEFAULT_DATE_FIN_HEURE_ETE);
        assertThat(testPays.isHeureEte()).isEqualTo(DEFAULT_HEURE_ETE);
        assertThat(testPays.getDecalageHoraire()).isEqualTo(DEFAULT_DECALAGE_HORAIRE);
        assertThat(testPays.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
        assertThat(testPays.isActif()).isEqualTo(DEFAULT_ACTIF);
    }

    @Test
    @Transactional
    public void createPaysWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = paysRepository.findAll().size();

        // Create the Pays with an existing ID
        pays.setId(1L);
        PaysDTO paysDTO = paysMapper.toDto(pays);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPaysMockMvc.perform(post("/api/pays")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(paysDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Pays in the database
        List<Pays> paysList = paysRepository.findAll();
        assertThat(paysList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPays() throws Exception {
        // Initialize the database
        paysRepository.saveAndFlush(pays);

        // Get all the paysList
        restPaysMockMvc.perform(get("/api/pays?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pays.getId().intValue())))
            .andExpect(jsonPath("$.[*].abreviation").value(hasItem(DEFAULT_ABREVIATION)))
            .andExpect(jsonPath("$.[*].codeAlphanum").value(hasItem(DEFAULT_CODE_ALPHANUM)))
            .andExpect(jsonPath("$.[*].codeNumerique").value(hasItem(DEFAULT_CODE_NUMERIQUE)))
            .andExpect(jsonPath("$.[*].dateDebutHeureEte").value(hasItem(DEFAULT_DATE_DEBUT_HEURE_ETE.toString())))
            .andExpect(jsonPath("$.[*].dateFinHeureEte").value(hasItem(DEFAULT_DATE_FIN_HEURE_ETE.toString())))
            .andExpect(jsonPath("$.[*].heureEte").value(hasItem(DEFAULT_HEURE_ETE.booleanValue())))
            .andExpect(jsonPath("$.[*].decalageHoraire").value(hasItem(DEFAULT_DECALAGE_HORAIRE)))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)))
            .andExpect(jsonPath("$.[*].actif").value(hasItem(DEFAULT_ACTIF.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getPays() throws Exception {
        // Initialize the database
        paysRepository.saveAndFlush(pays);

        // Get the pays
        restPaysMockMvc.perform(get("/api/pays/{id}", pays.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(pays.getId().intValue()))
            .andExpect(jsonPath("$.abreviation").value(DEFAULT_ABREVIATION))
            .andExpect(jsonPath("$.codeAlphanum").value(DEFAULT_CODE_ALPHANUM))
            .andExpect(jsonPath("$.codeNumerique").value(DEFAULT_CODE_NUMERIQUE))
            .andExpect(jsonPath("$.dateDebutHeureEte").value(DEFAULT_DATE_DEBUT_HEURE_ETE.toString()))
            .andExpect(jsonPath("$.dateFinHeureEte").value(DEFAULT_DATE_FIN_HEURE_ETE.toString()))
            .andExpect(jsonPath("$.heureEte").value(DEFAULT_HEURE_ETE.booleanValue()))
            .andExpect(jsonPath("$.decalageHoraire").value(DEFAULT_DECALAGE_HORAIRE))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE))
            .andExpect(jsonPath("$.actif").value(DEFAULT_ACTIF.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingPays() throws Exception {
        // Get the pays
        restPaysMockMvc.perform(get("/api/pays/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePays() throws Exception {
        // Initialize the database
        paysRepository.saveAndFlush(pays);

        int databaseSizeBeforeUpdate = paysRepository.findAll().size();

        // Update the pays
        Pays updatedPays = paysRepository.findById(pays.getId()).get();
        // Disconnect from session so that the updates on updatedPays are not directly saved in db
        em.detach(updatedPays);
        updatedPays
            .abreviation(UPDATED_ABREVIATION)
            .codeAlphanum(UPDATED_CODE_ALPHANUM)
            .codeNumerique(UPDATED_CODE_NUMERIQUE)
            .dateDebutHeureEte(UPDATED_DATE_DEBUT_HEURE_ETE)
            .dateFinHeureEte(UPDATED_DATE_FIN_HEURE_ETE)
            .heureEte(UPDATED_HEURE_ETE)
            .decalageHoraire(UPDATED_DECALAGE_HORAIRE)
            .libelle(UPDATED_LIBELLE)
            .actif(UPDATED_ACTIF);
        PaysDTO paysDTO = paysMapper.toDto(updatedPays);

        restPaysMockMvc.perform(put("/api/pays")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(paysDTO)))
            .andExpect(status().isOk());

        // Validate the Pays in the database
        List<Pays> paysList = paysRepository.findAll();
        assertThat(paysList).hasSize(databaseSizeBeforeUpdate);
        Pays testPays = paysList.get(paysList.size() - 1);
        assertThat(testPays.getAbreviation()).isEqualTo(UPDATED_ABREVIATION);
        assertThat(testPays.getCodeAlphanum()).isEqualTo(UPDATED_CODE_ALPHANUM);
        assertThat(testPays.getCodeNumerique()).isEqualTo(UPDATED_CODE_NUMERIQUE);
        assertThat(testPays.getDateDebutHeureEte()).isEqualTo(UPDATED_DATE_DEBUT_HEURE_ETE);
        assertThat(testPays.getDateFinHeureEte()).isEqualTo(UPDATED_DATE_FIN_HEURE_ETE);
        assertThat(testPays.isHeureEte()).isEqualTo(UPDATED_HEURE_ETE);
        assertThat(testPays.getDecalageHoraire()).isEqualTo(UPDATED_DECALAGE_HORAIRE);
        assertThat(testPays.getLibelle()).isEqualTo(UPDATED_LIBELLE);
        assertThat(testPays.isActif()).isEqualTo(UPDATED_ACTIF);
    }

    @Test
    @Transactional
    public void updateNonExistingPays() throws Exception {
        int databaseSizeBeforeUpdate = paysRepository.findAll().size();

        // Create the Pays
        PaysDTO paysDTO = paysMapper.toDto(pays);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPaysMockMvc.perform(put("/api/pays")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(paysDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Pays in the database
        List<Pays> paysList = paysRepository.findAll();
        assertThat(paysList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePays() throws Exception {
        // Initialize the database
        paysRepository.saveAndFlush(pays);

        int databaseSizeBeforeDelete = paysRepository.findAll().size();

        // Delete the pays
        restPaysMockMvc.perform(delete("/api/pays/{id}", pays.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Pays> paysList = paysRepository.findAll();
        assertThat(paysList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
