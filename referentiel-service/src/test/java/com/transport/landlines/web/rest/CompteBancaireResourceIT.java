package com.transport.landlines.web.rest;

import com.transport.landlines.ReferentielApp;
import com.transport.landlines.domain.CompteBancaire;
import com.transport.landlines.repository.CompteBancaireRepository;
import com.transport.landlines.service.CompteBancaireService;
import com.transport.landlines.service.dto.CompteBancaireDTO;
import com.transport.landlines.service.mapper.CompteBancaireMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CompteBancaireResource} REST controller.
 */
@SpringBootTest(classes = ReferentielApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CompteBancaireResourceIT {

    private static final String DEFAULT_RIB = "AAAAAAAAAA";
    private static final String UPDATED_RIB = "BBBBBBBBBB";

    @Autowired
    private CompteBancaireRepository compteBancaireRepository;

    @Autowired
    private CompteBancaireMapper compteBancaireMapper;

    @Autowired
    private CompteBancaireService compteBancaireService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCompteBancaireMockMvc;

    private CompteBancaire compteBancaire;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompteBancaire createEntity(EntityManager em) {
        CompteBancaire compteBancaire = new CompteBancaire()
            .rib(DEFAULT_RIB);
        return compteBancaire;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompteBancaire createUpdatedEntity(EntityManager em) {
        CompteBancaire compteBancaire = new CompteBancaire()
            .rib(UPDATED_RIB);
        return compteBancaire;
    }

    @BeforeEach
    public void initTest() {
        compteBancaire = createEntity(em);
    }

    @Test
    @Transactional
    public void createCompteBancaire() throws Exception {
        int databaseSizeBeforeCreate = compteBancaireRepository.findAll().size();
        // Create the CompteBancaire
        CompteBancaireDTO compteBancaireDTO = compteBancaireMapper.toDto(compteBancaire);
        restCompteBancaireMockMvc.perform(post("/api/compte-bancaires")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(compteBancaireDTO)))
            .andExpect(status().isCreated());

        // Validate the CompteBancaire in the database
        List<CompteBancaire> compteBancaireList = compteBancaireRepository.findAll();
        assertThat(compteBancaireList).hasSize(databaseSizeBeforeCreate + 1);
        CompteBancaire testCompteBancaire = compteBancaireList.get(compteBancaireList.size() - 1);
        assertThat(testCompteBancaire.getRib()).isEqualTo(DEFAULT_RIB);
    }

    @Test
    @Transactional
    public void createCompteBancaireWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = compteBancaireRepository.findAll().size();

        // Create the CompteBancaire with an existing ID
        compteBancaire.setId(1L);
        CompteBancaireDTO compteBancaireDTO = compteBancaireMapper.toDto(compteBancaire);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCompteBancaireMockMvc.perform(post("/api/compte-bancaires")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(compteBancaireDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CompteBancaire in the database
        List<CompteBancaire> compteBancaireList = compteBancaireRepository.findAll();
        assertThat(compteBancaireList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCompteBancaires() throws Exception {
        // Initialize the database
        compteBancaireRepository.saveAndFlush(compteBancaire);

        // Get all the compteBancaireList
        restCompteBancaireMockMvc.perform(get("/api/compte-bancaires?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(compteBancaire.getId().intValue())))
            .andExpect(jsonPath("$.[*].rib").value(hasItem(DEFAULT_RIB)));
    }
    
    @Test
    @Transactional
    public void getCompteBancaire() throws Exception {
        // Initialize the database
        compteBancaireRepository.saveAndFlush(compteBancaire);

        // Get the compteBancaire
        restCompteBancaireMockMvc.perform(get("/api/compte-bancaires/{id}", compteBancaire.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(compteBancaire.getId().intValue()))
            .andExpect(jsonPath("$.rib").value(DEFAULT_RIB));
    }
    @Test
    @Transactional
    public void getNonExistingCompteBancaire() throws Exception {
        // Get the compteBancaire
        restCompteBancaireMockMvc.perform(get("/api/compte-bancaires/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCompteBancaire() throws Exception {
        // Initialize the database
        compteBancaireRepository.saveAndFlush(compteBancaire);

        int databaseSizeBeforeUpdate = compteBancaireRepository.findAll().size();

        // Update the compteBancaire
        CompteBancaire updatedCompteBancaire = compteBancaireRepository.findById(compteBancaire.getId()).get();
        // Disconnect from session so that the updates on updatedCompteBancaire are not directly saved in db
        em.detach(updatedCompteBancaire);
        updatedCompteBancaire
            .rib(UPDATED_RIB);
        CompteBancaireDTO compteBancaireDTO = compteBancaireMapper.toDto(updatedCompteBancaire);

        restCompteBancaireMockMvc.perform(put("/api/compte-bancaires")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(compteBancaireDTO)))
            .andExpect(status().isOk());

        // Validate the CompteBancaire in the database
        List<CompteBancaire> compteBancaireList = compteBancaireRepository.findAll();
        assertThat(compteBancaireList).hasSize(databaseSizeBeforeUpdate);
        CompteBancaire testCompteBancaire = compteBancaireList.get(compteBancaireList.size() - 1);
        assertThat(testCompteBancaire.getRib()).isEqualTo(UPDATED_RIB);
    }

    @Test
    @Transactional
    public void updateNonExistingCompteBancaire() throws Exception {
        int databaseSizeBeforeUpdate = compteBancaireRepository.findAll().size();

        // Create the CompteBancaire
        CompteBancaireDTO compteBancaireDTO = compteBancaireMapper.toDto(compteBancaire);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCompteBancaireMockMvc.perform(put("/api/compte-bancaires")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(compteBancaireDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CompteBancaire in the database
        List<CompteBancaire> compteBancaireList = compteBancaireRepository.findAll();
        assertThat(compteBancaireList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCompteBancaire() throws Exception {
        // Initialize the database
        compteBancaireRepository.saveAndFlush(compteBancaire);

        int databaseSizeBeforeDelete = compteBancaireRepository.findAll().size();

        // Delete the compteBancaire
        restCompteBancaireMockMvc.perform(delete("/api/compte-bancaires/{id}", compteBancaire.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CompteBancaire> compteBancaireList = compteBancaireRepository.findAll();
        assertThat(compteBancaireList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
