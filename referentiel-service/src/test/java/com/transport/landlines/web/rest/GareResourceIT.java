package com.transport.landlines.web.rest;

import com.transport.landlines.ReferentielApp;
import com.transport.landlines.domain.Gare;
import com.transport.landlines.repository.GareRepository;
import com.transport.landlines.service.GareService;
import com.transport.landlines.service.dto.GareDTO;
import com.transport.landlines.service.mapper.GareMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link GareResource} REST controller.
 */
@SpringBootTest(classes = ReferentielApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class GareResourceIT {

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_CORRESPONDANCE = false;
    private static final Boolean UPDATED_CORRESPONDANCE = true;

    private static final Boolean DEFAULT_GARE_AGENCE = false;
    private static final Boolean UPDATED_GARE_AGENCE = true;

    private static final Boolean DEFAULT_ACTIF = false;
    private static final Boolean UPDATED_ACTIF = true;

    @Autowired
    private GareRepository gareRepository;

    @Autowired
    private GareMapper gareMapper;

    @Autowired
    private GareService gareService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restGareMockMvc;

    private Gare gare;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Gare createEntity(EntityManager em) {
        Gare gare = new Gare()
            .libelle(DEFAULT_LIBELLE)
            .correspondance(DEFAULT_CORRESPONDANCE)
            .gareAgence(DEFAULT_GARE_AGENCE)
            .actif(DEFAULT_ACTIF);
        return gare;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Gare createUpdatedEntity(EntityManager em) {
        Gare gare = new Gare()
            .libelle(UPDATED_LIBELLE)
            .correspondance(UPDATED_CORRESPONDANCE)
            .gareAgence(UPDATED_GARE_AGENCE)
            .actif(UPDATED_ACTIF);
        return gare;
    }

    @BeforeEach
    public void initTest() {
        gare = createEntity(em);
    }

    @Test
    @Transactional
    public void createGare() throws Exception {
        int databaseSizeBeforeCreate = gareRepository.findAll().size();
        // Create the Gare
        GareDTO gareDTO = gareMapper.toDto(gare);
        restGareMockMvc.perform(post("/api/gares")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(gareDTO)))
            .andExpect(status().isCreated());

        // Validate the Gare in the database
        List<Gare> gareList = gareRepository.findAll();
        assertThat(gareList).hasSize(databaseSizeBeforeCreate + 1);
        Gare testGare = gareList.get(gareList.size() - 1);
        assertThat(testGare.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
        assertThat(testGare.isCorrespondance()).isEqualTo(DEFAULT_CORRESPONDANCE);
        assertThat(testGare.isGareAgence()).isEqualTo(DEFAULT_GARE_AGENCE);
        assertThat(testGare.isActif()).isEqualTo(DEFAULT_ACTIF);
    }

    @Test
    @Transactional
    public void createGareWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = gareRepository.findAll().size();

        // Create the Gare with an existing ID
        gare.setId(1L);
        GareDTO gareDTO = gareMapper.toDto(gare);

        // An entity with an existing ID cannot be created, so this API call must fail
        restGareMockMvc.perform(post("/api/gares")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(gareDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Gare in the database
        List<Gare> gareList = gareRepository.findAll();
        assertThat(gareList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllGares() throws Exception {
        // Initialize the database
        gareRepository.saveAndFlush(gare);

        // Get all the gareList
        restGareMockMvc.perform(get("/api/gares?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(gare.getId().intValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)))
            .andExpect(jsonPath("$.[*].correspondance").value(hasItem(DEFAULT_CORRESPONDANCE.booleanValue())))
            .andExpect(jsonPath("$.[*].gareAgence").value(hasItem(DEFAULT_GARE_AGENCE.booleanValue())))
            .andExpect(jsonPath("$.[*].actif").value(hasItem(DEFAULT_ACTIF.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getGare() throws Exception {
        // Initialize the database
        gareRepository.saveAndFlush(gare);

        // Get the gare
        restGareMockMvc.perform(get("/api/gares/{id}", gare.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(gare.getId().intValue()))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE))
            .andExpect(jsonPath("$.correspondance").value(DEFAULT_CORRESPONDANCE.booleanValue()))
            .andExpect(jsonPath("$.gareAgence").value(DEFAULT_GARE_AGENCE.booleanValue()))
            .andExpect(jsonPath("$.actif").value(DEFAULT_ACTIF.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingGare() throws Exception {
        // Get the gare
        restGareMockMvc.perform(get("/api/gares/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGare() throws Exception {
        // Initialize the database
        gareRepository.saveAndFlush(gare);

        int databaseSizeBeforeUpdate = gareRepository.findAll().size();

        // Update the gare
        Gare updatedGare = gareRepository.findById(gare.getId()).get();
        // Disconnect from session so that the updates on updatedGare are not directly saved in db
        em.detach(updatedGare);
        updatedGare
            .libelle(UPDATED_LIBELLE)
            .correspondance(UPDATED_CORRESPONDANCE)
            .gareAgence(UPDATED_GARE_AGENCE)
            .actif(UPDATED_ACTIF);
        GareDTO gareDTO = gareMapper.toDto(updatedGare);

        restGareMockMvc.perform(put("/api/gares")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(gareDTO)))
            .andExpect(status().isOk());

        // Validate the Gare in the database
        List<Gare> gareList = gareRepository.findAll();
        assertThat(gareList).hasSize(databaseSizeBeforeUpdate);
        Gare testGare = gareList.get(gareList.size() - 1);
        assertThat(testGare.getLibelle()).isEqualTo(UPDATED_LIBELLE);
        assertThat(testGare.isCorrespondance()).isEqualTo(UPDATED_CORRESPONDANCE);
        assertThat(testGare.isGareAgence()).isEqualTo(UPDATED_GARE_AGENCE);
        assertThat(testGare.isActif()).isEqualTo(UPDATED_ACTIF);
    }

    @Test
    @Transactional
    public void updateNonExistingGare() throws Exception {
        int databaseSizeBeforeUpdate = gareRepository.findAll().size();

        // Create the Gare
        GareDTO gareDTO = gareMapper.toDto(gare);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGareMockMvc.perform(put("/api/gares")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(gareDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Gare in the database
        List<Gare> gareList = gareRepository.findAll();
        assertThat(gareList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteGare() throws Exception {
        // Initialize the database
        gareRepository.saveAndFlush(gare);

        int databaseSizeBeforeDelete = gareRepository.findAll().size();

        // Delete the gare
        restGareMockMvc.perform(delete("/api/gares/{id}", gare.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Gare> gareList = gareRepository.findAll();
        assertThat(gareList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
