package com.transport.landlines.web.rest;

import com.transport.landlines.ReferentielApp;
import com.transport.landlines.domain.Devise;
import com.transport.landlines.repository.DeviseRepository;
import com.transport.landlines.service.DeviseService;
import com.transport.landlines.service.dto.DeviseDTO;
import com.transport.landlines.service.mapper.DeviseMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DeviseResource} REST controller.
 */
@SpringBootTest(classes = ReferentielApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class DeviseResourceIT {

    private static final String DEFAULT_ABREVIATION_COURTE = "AAAAAAAAAA";
    private static final String UPDATED_ABREVIATION_COURTE = "BBBBBBBBBB";

    private static final String DEFAULT_ABREVIATION_LONGUE = "AAAAAAAAAA";
    private static final String UPDATED_ABREVIATION_LONGUE = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    private static final Integer DEFAULT_NOMBRE_APRES_VIRGULE = 1;
    private static final Integer UPDATED_NOMBRE_APRES_VIRGULE = 2;

    private static final Boolean DEFAULT_ACTIF = false;
    private static final Boolean UPDATED_ACTIF = true;

    @Autowired
    private DeviseRepository deviseRepository;

    @Autowired
    private DeviseMapper deviseMapper;

    @Autowired
    private DeviseService deviseService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDeviseMockMvc;

    private Devise devise;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Devise createEntity(EntityManager em) {
        Devise devise = new Devise()
            .abreviationCourte(DEFAULT_ABREVIATION_COURTE)
            .abreviationLongue(DEFAULT_ABREVIATION_LONGUE)
            .code(DEFAULT_CODE)
            .libelle(DEFAULT_LIBELLE)
            .nombreApresVirgule(DEFAULT_NOMBRE_APRES_VIRGULE)
            .actif(DEFAULT_ACTIF);
        return devise;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Devise createUpdatedEntity(EntityManager em) {
        Devise devise = new Devise()
            .abreviationCourte(UPDATED_ABREVIATION_COURTE)
            .abreviationLongue(UPDATED_ABREVIATION_LONGUE)
            .code(UPDATED_CODE)
            .libelle(UPDATED_LIBELLE)
            .nombreApresVirgule(UPDATED_NOMBRE_APRES_VIRGULE)
            .actif(UPDATED_ACTIF);
        return devise;
    }

    @BeforeEach
    public void initTest() {
        devise = createEntity(em);
    }

    @Test
    @Transactional
    public void createDevise() throws Exception {
        int databaseSizeBeforeCreate = deviseRepository.findAll().size();
        // Create the Devise
        DeviseDTO deviseDTO = deviseMapper.toDto(devise);
        restDeviseMockMvc.perform(post("/api/devises")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(deviseDTO)))
            .andExpect(status().isCreated());

        // Validate the Devise in the database
        List<Devise> deviseList = deviseRepository.findAll();
        assertThat(deviseList).hasSize(databaseSizeBeforeCreate + 1);
        Devise testDevise = deviseList.get(deviseList.size() - 1);
        assertThat(testDevise.getAbreviationCourte()).isEqualTo(DEFAULT_ABREVIATION_COURTE);
        assertThat(testDevise.getAbreviationLongue()).isEqualTo(DEFAULT_ABREVIATION_LONGUE);
        assertThat(testDevise.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testDevise.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
        assertThat(testDevise.getNombreApresVirgule()).isEqualTo(DEFAULT_NOMBRE_APRES_VIRGULE);
        assertThat(testDevise.isActif()).isEqualTo(DEFAULT_ACTIF);
    }

    @Test
    @Transactional
    public void createDeviseWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = deviseRepository.findAll().size();

        // Create the Devise with an existing ID
        devise.setId(1L);
        DeviseDTO deviseDTO = deviseMapper.toDto(devise);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDeviseMockMvc.perform(post("/api/devises")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(deviseDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Devise in the database
        List<Devise> deviseList = deviseRepository.findAll();
        assertThat(deviseList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDevises() throws Exception {
        // Initialize the database
        deviseRepository.saveAndFlush(devise);

        // Get all the deviseList
        restDeviseMockMvc.perform(get("/api/devises?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(devise.getId().intValue())))
            .andExpect(jsonPath("$.[*].abreviationCourte").value(hasItem(DEFAULT_ABREVIATION_COURTE)))
            .andExpect(jsonPath("$.[*].abreviationLongue").value(hasItem(DEFAULT_ABREVIATION_LONGUE)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)))
            .andExpect(jsonPath("$.[*].nombreApresVirgule").value(hasItem(DEFAULT_NOMBRE_APRES_VIRGULE)))
            .andExpect(jsonPath("$.[*].actif").value(hasItem(DEFAULT_ACTIF.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getDevise() throws Exception {
        // Initialize the database
        deviseRepository.saveAndFlush(devise);

        // Get the devise
        restDeviseMockMvc.perform(get("/api/devises/{id}", devise.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(devise.getId().intValue()))
            .andExpect(jsonPath("$.abreviationCourte").value(DEFAULT_ABREVIATION_COURTE))
            .andExpect(jsonPath("$.abreviationLongue").value(DEFAULT_ABREVIATION_LONGUE))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE))
            .andExpect(jsonPath("$.nombreApresVirgule").value(DEFAULT_NOMBRE_APRES_VIRGULE))
            .andExpect(jsonPath("$.actif").value(DEFAULT_ACTIF.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingDevise() throws Exception {
        // Get the devise
        restDeviseMockMvc.perform(get("/api/devises/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDevise() throws Exception {
        // Initialize the database
        deviseRepository.saveAndFlush(devise);

        int databaseSizeBeforeUpdate = deviseRepository.findAll().size();

        // Update the devise
        Devise updatedDevise = deviseRepository.findById(devise.getId()).get();
        // Disconnect from session so that the updates on updatedDevise are not directly saved in db
        em.detach(updatedDevise);
        updatedDevise
            .abreviationCourte(UPDATED_ABREVIATION_COURTE)
            .abreviationLongue(UPDATED_ABREVIATION_LONGUE)
            .code(UPDATED_CODE)
            .libelle(UPDATED_LIBELLE)
            .nombreApresVirgule(UPDATED_NOMBRE_APRES_VIRGULE)
            .actif(UPDATED_ACTIF);
        DeviseDTO deviseDTO = deviseMapper.toDto(updatedDevise);

        restDeviseMockMvc.perform(put("/api/devises")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(deviseDTO)))
            .andExpect(status().isOk());

        // Validate the Devise in the database
        List<Devise> deviseList = deviseRepository.findAll();
        assertThat(deviseList).hasSize(databaseSizeBeforeUpdate);
        Devise testDevise = deviseList.get(deviseList.size() - 1);
        assertThat(testDevise.getAbreviationCourte()).isEqualTo(UPDATED_ABREVIATION_COURTE);
        assertThat(testDevise.getAbreviationLongue()).isEqualTo(UPDATED_ABREVIATION_LONGUE);
        assertThat(testDevise.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testDevise.getLibelle()).isEqualTo(UPDATED_LIBELLE);
        assertThat(testDevise.getNombreApresVirgule()).isEqualTo(UPDATED_NOMBRE_APRES_VIRGULE);
        assertThat(testDevise.isActif()).isEqualTo(UPDATED_ACTIF);
    }

    @Test
    @Transactional
    public void updateNonExistingDevise() throws Exception {
        int databaseSizeBeforeUpdate = deviseRepository.findAll().size();

        // Create the Devise
        DeviseDTO deviseDTO = deviseMapper.toDto(devise);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDeviseMockMvc.perform(put("/api/devises")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(deviseDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Devise in the database
        List<Devise> deviseList = deviseRepository.findAll();
        assertThat(deviseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDevise() throws Exception {
        // Initialize the database
        deviseRepository.saveAndFlush(devise);

        int databaseSizeBeforeDelete = deviseRepository.findAll().size();

        // Delete the devise
        restDeviseMockMvc.perform(delete("/api/devises/{id}", devise.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Devise> deviseList = deviseRepository.findAll();
        assertThat(deviseList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
