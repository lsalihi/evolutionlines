package com.transport.landlines.web.rest;

import com.transport.landlines.ReferentielApp;
import com.transport.landlines.domain.ModeReglement;
import com.transport.landlines.repository.ModeReglementRepository;
import com.transport.landlines.service.ModeReglementService;
import com.transport.landlines.service.dto.ModeReglementDTO;
import com.transport.landlines.service.mapper.ModeReglementMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ModeReglementResource} REST controller.
 */
@SpringBootTest(classes = ReferentielApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ModeReglementResourceIT {

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIF = false;
    private static final Boolean UPDATED_ACTIF = true;

    @Autowired
    private ModeReglementRepository modeReglementRepository;

    @Autowired
    private ModeReglementMapper modeReglementMapper;

    @Autowired
    private ModeReglementService modeReglementService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restModeReglementMockMvc;

    private ModeReglement modeReglement;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ModeReglement createEntity(EntityManager em) {
        ModeReglement modeReglement = new ModeReglement()
            .libelle(DEFAULT_LIBELLE)
            .description(DEFAULT_DESCRIPTION)
            .actif(DEFAULT_ACTIF);
        return modeReglement;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ModeReglement createUpdatedEntity(EntityManager em) {
        ModeReglement modeReglement = new ModeReglement()
            .libelle(UPDATED_LIBELLE)
            .description(UPDATED_DESCRIPTION)
            .actif(UPDATED_ACTIF);
        return modeReglement;
    }

    @BeforeEach
    public void initTest() {
        modeReglement = createEntity(em);
    }

    @Test
    @Transactional
    public void createModeReglement() throws Exception {
        int databaseSizeBeforeCreate = modeReglementRepository.findAll().size();
        // Create the ModeReglement
        ModeReglementDTO modeReglementDTO = modeReglementMapper.toDto(modeReglement);
        restModeReglementMockMvc.perform(post("/api/mode-reglements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(modeReglementDTO)))
            .andExpect(status().isCreated());

        // Validate the ModeReglement in the database
        List<ModeReglement> modeReglementList = modeReglementRepository.findAll();
        assertThat(modeReglementList).hasSize(databaseSizeBeforeCreate + 1);
        ModeReglement testModeReglement = modeReglementList.get(modeReglementList.size() - 1);
        assertThat(testModeReglement.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
        assertThat(testModeReglement.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testModeReglement.isActif()).isEqualTo(DEFAULT_ACTIF);
    }

    @Test
    @Transactional
    public void createModeReglementWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = modeReglementRepository.findAll().size();

        // Create the ModeReglement with an existing ID
        modeReglement.setId(1L);
        ModeReglementDTO modeReglementDTO = modeReglementMapper.toDto(modeReglement);

        // An entity with an existing ID cannot be created, so this API call must fail
        restModeReglementMockMvc.perform(post("/api/mode-reglements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(modeReglementDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ModeReglement in the database
        List<ModeReglement> modeReglementList = modeReglementRepository.findAll();
        assertThat(modeReglementList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllModeReglements() throws Exception {
        // Initialize the database
        modeReglementRepository.saveAndFlush(modeReglement);

        // Get all the modeReglementList
        restModeReglementMockMvc.perform(get("/api/mode-reglements?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(modeReglement.getId().intValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].actif").value(hasItem(DEFAULT_ACTIF.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getModeReglement() throws Exception {
        // Initialize the database
        modeReglementRepository.saveAndFlush(modeReglement);

        // Get the modeReglement
        restModeReglementMockMvc.perform(get("/api/mode-reglements/{id}", modeReglement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(modeReglement.getId().intValue()))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.actif").value(DEFAULT_ACTIF.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingModeReglement() throws Exception {
        // Get the modeReglement
        restModeReglementMockMvc.perform(get("/api/mode-reglements/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateModeReglement() throws Exception {
        // Initialize the database
        modeReglementRepository.saveAndFlush(modeReglement);

        int databaseSizeBeforeUpdate = modeReglementRepository.findAll().size();

        // Update the modeReglement
        ModeReglement updatedModeReglement = modeReglementRepository.findById(modeReglement.getId()).get();
        // Disconnect from session so that the updates on updatedModeReglement are not directly saved in db
        em.detach(updatedModeReglement);
        updatedModeReglement
            .libelle(UPDATED_LIBELLE)
            .description(UPDATED_DESCRIPTION)
            .actif(UPDATED_ACTIF);
        ModeReglementDTO modeReglementDTO = modeReglementMapper.toDto(updatedModeReglement);

        restModeReglementMockMvc.perform(put("/api/mode-reglements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(modeReglementDTO)))
            .andExpect(status().isOk());

        // Validate the ModeReglement in the database
        List<ModeReglement> modeReglementList = modeReglementRepository.findAll();
        assertThat(modeReglementList).hasSize(databaseSizeBeforeUpdate);
        ModeReglement testModeReglement = modeReglementList.get(modeReglementList.size() - 1);
        assertThat(testModeReglement.getLibelle()).isEqualTo(UPDATED_LIBELLE);
        assertThat(testModeReglement.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testModeReglement.isActif()).isEqualTo(UPDATED_ACTIF);
    }

    @Test
    @Transactional
    public void updateNonExistingModeReglement() throws Exception {
        int databaseSizeBeforeUpdate = modeReglementRepository.findAll().size();

        // Create the ModeReglement
        ModeReglementDTO modeReglementDTO = modeReglementMapper.toDto(modeReglement);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restModeReglementMockMvc.perform(put("/api/mode-reglements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(modeReglementDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ModeReglement in the database
        List<ModeReglement> modeReglementList = modeReglementRepository.findAll();
        assertThat(modeReglementList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteModeReglement() throws Exception {
        // Initialize the database
        modeReglementRepository.saveAndFlush(modeReglement);

        int databaseSizeBeforeDelete = modeReglementRepository.findAll().size();

        // Delete the modeReglement
        restModeReglementMockMvc.perform(delete("/api/mode-reglements/{id}", modeReglement.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ModeReglement> modeReglementList = modeReglementRepository.findAll();
        assertThat(modeReglementList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
