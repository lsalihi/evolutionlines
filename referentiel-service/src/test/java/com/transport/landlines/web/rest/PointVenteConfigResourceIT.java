package com.transport.landlines.web.rest;

import com.transport.landlines.ReferentielApp;
import com.transport.landlines.domain.PointVenteConfig;
import com.transport.landlines.repository.PointVenteConfigRepository;
import com.transport.landlines.service.PointVenteConfigService;
import com.transport.landlines.service.dto.PointVenteConfigDTO;
import com.transport.landlines.service.mapper.PointVenteConfigMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PointVenteConfigResource} REST controller.
 */
@SpringBootTest(classes = ReferentielApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class PointVenteConfigResourceIT {

    @Autowired
    private PointVenteConfigRepository pointVenteConfigRepository;

    @Autowired
    private PointVenteConfigMapper pointVenteConfigMapper;

    @Autowired
    private PointVenteConfigService pointVenteConfigService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPointVenteConfigMockMvc;

    private PointVenteConfig pointVenteConfig;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PointVenteConfig createEntity(EntityManager em) {
        PointVenteConfig pointVenteConfig = new PointVenteConfig();
        return pointVenteConfig;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PointVenteConfig createUpdatedEntity(EntityManager em) {
        PointVenteConfig pointVenteConfig = new PointVenteConfig();
        return pointVenteConfig;
    }

    @BeforeEach
    public void initTest() {
        pointVenteConfig = createEntity(em);
    }

    @Test
    @Transactional
    public void createPointVenteConfig() throws Exception {
        int databaseSizeBeforeCreate = pointVenteConfigRepository.findAll().size();
        // Create the PointVenteConfig
        PointVenteConfigDTO pointVenteConfigDTO = pointVenteConfigMapper.toDto(pointVenteConfig);
        restPointVenteConfigMockMvc.perform(post("/api/point-vente-configs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pointVenteConfigDTO)))
            .andExpect(status().isCreated());

        // Validate the PointVenteConfig in the database
        List<PointVenteConfig> pointVenteConfigList = pointVenteConfigRepository.findAll();
        assertThat(pointVenteConfigList).hasSize(databaseSizeBeforeCreate + 1);
        PointVenteConfig testPointVenteConfig = pointVenteConfigList.get(pointVenteConfigList.size() - 1);
    }

    @Test
    @Transactional
    public void createPointVenteConfigWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pointVenteConfigRepository.findAll().size();

        // Create the PointVenteConfig with an existing ID
        pointVenteConfig.setId(1L);
        PointVenteConfigDTO pointVenteConfigDTO = pointVenteConfigMapper.toDto(pointVenteConfig);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPointVenteConfigMockMvc.perform(post("/api/point-vente-configs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pointVenteConfigDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PointVenteConfig in the database
        List<PointVenteConfig> pointVenteConfigList = pointVenteConfigRepository.findAll();
        assertThat(pointVenteConfigList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPointVenteConfigs() throws Exception {
        // Initialize the database
        pointVenteConfigRepository.saveAndFlush(pointVenteConfig);

        // Get all the pointVenteConfigList
        restPointVenteConfigMockMvc.perform(get("/api/point-vente-configs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pointVenteConfig.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getPointVenteConfig() throws Exception {
        // Initialize the database
        pointVenteConfigRepository.saveAndFlush(pointVenteConfig);

        // Get the pointVenteConfig
        restPointVenteConfigMockMvc.perform(get("/api/point-vente-configs/{id}", pointVenteConfig.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(pointVenteConfig.getId().intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingPointVenteConfig() throws Exception {
        // Get the pointVenteConfig
        restPointVenteConfigMockMvc.perform(get("/api/point-vente-configs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePointVenteConfig() throws Exception {
        // Initialize the database
        pointVenteConfigRepository.saveAndFlush(pointVenteConfig);

        int databaseSizeBeforeUpdate = pointVenteConfigRepository.findAll().size();

        // Update the pointVenteConfig
        PointVenteConfig updatedPointVenteConfig = pointVenteConfigRepository.findById(pointVenteConfig.getId()).get();
        // Disconnect from session so that the updates on updatedPointVenteConfig are not directly saved in db
        em.detach(updatedPointVenteConfig);
        PointVenteConfigDTO pointVenteConfigDTO = pointVenteConfigMapper.toDto(updatedPointVenteConfig);

        restPointVenteConfigMockMvc.perform(put("/api/point-vente-configs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pointVenteConfigDTO)))
            .andExpect(status().isOk());

        // Validate the PointVenteConfig in the database
        List<PointVenteConfig> pointVenteConfigList = pointVenteConfigRepository.findAll();
        assertThat(pointVenteConfigList).hasSize(databaseSizeBeforeUpdate);
        PointVenteConfig testPointVenteConfig = pointVenteConfigList.get(pointVenteConfigList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingPointVenteConfig() throws Exception {
        int databaseSizeBeforeUpdate = pointVenteConfigRepository.findAll().size();

        // Create the PointVenteConfig
        PointVenteConfigDTO pointVenteConfigDTO = pointVenteConfigMapper.toDto(pointVenteConfig);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPointVenteConfigMockMvc.perform(put("/api/point-vente-configs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pointVenteConfigDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PointVenteConfig in the database
        List<PointVenteConfig> pointVenteConfigList = pointVenteConfigRepository.findAll();
        assertThat(pointVenteConfigList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePointVenteConfig() throws Exception {
        // Initialize the database
        pointVenteConfigRepository.saveAndFlush(pointVenteConfig);

        int databaseSizeBeforeDelete = pointVenteConfigRepository.findAll().size();

        // Delete the pointVenteConfig
        restPointVenteConfigMockMvc.perform(delete("/api/point-vente-configs/{id}", pointVenteConfig.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PointVenteConfig> pointVenteConfigList = pointVenteConfigRepository.findAll();
        assertThat(pointVenteConfigList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
