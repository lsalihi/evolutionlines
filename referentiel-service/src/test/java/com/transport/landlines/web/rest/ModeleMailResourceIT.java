package com.transport.landlines.web.rest;

import com.transport.landlines.ReferentielApp;
import com.transport.landlines.domain.ModeleMail;
import com.transport.landlines.repository.ModeleMailRepository;
import com.transport.landlines.service.ModeleMailService;
import com.transport.landlines.service.dto.ModeleMailDTO;
import com.transport.landlines.service.mapper.ModeleMailMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ModeleMailResource} REST controller.
 */
@SpringBootTest(classes = ReferentielApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ModeleMailResourceIT {

    private static final String DEFAULT_DESTINATAIRE = "AAAAAAAAAA";
    private static final String UPDATED_DESTINATAIRE = "BBBBBBBBBB";

    private static final String DEFAULT_OBJET_MAIL = "AAAAAAAAAA";
    private static final String UPDATED_OBJET_MAIL = "BBBBBBBBBB";

    private static final Integer DEFAULT_SEUIL = 1;
    private static final Integer UPDATED_SEUIL = 2;

    private static final Boolean DEFAULT_ACTIF = false;
    private static final Boolean UPDATED_ACTIF = true;

    @Autowired
    private ModeleMailRepository modeleMailRepository;

    @Autowired
    private ModeleMailMapper modeleMailMapper;

    @Autowired
    private ModeleMailService modeleMailService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restModeleMailMockMvc;

    private ModeleMail modeleMail;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ModeleMail createEntity(EntityManager em) {
        ModeleMail modeleMail = new ModeleMail()
            .destinataire(DEFAULT_DESTINATAIRE)
            .objetMail(DEFAULT_OBJET_MAIL)
            .seuil(DEFAULT_SEUIL)
            .actif(DEFAULT_ACTIF);
        return modeleMail;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ModeleMail createUpdatedEntity(EntityManager em) {
        ModeleMail modeleMail = new ModeleMail()
            .destinataire(UPDATED_DESTINATAIRE)
            .objetMail(UPDATED_OBJET_MAIL)
            .seuil(UPDATED_SEUIL)
            .actif(UPDATED_ACTIF);
        return modeleMail;
    }

    @BeforeEach
    public void initTest() {
        modeleMail = createEntity(em);
    }

    @Test
    @Transactional
    public void createModeleMail() throws Exception {
        int databaseSizeBeforeCreate = modeleMailRepository.findAll().size();
        // Create the ModeleMail
        ModeleMailDTO modeleMailDTO = modeleMailMapper.toDto(modeleMail);
        restModeleMailMockMvc.perform(post("/api/modele-mails")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(modeleMailDTO)))
            .andExpect(status().isCreated());

        // Validate the ModeleMail in the database
        List<ModeleMail> modeleMailList = modeleMailRepository.findAll();
        assertThat(modeleMailList).hasSize(databaseSizeBeforeCreate + 1);
        ModeleMail testModeleMail = modeleMailList.get(modeleMailList.size() - 1);
        assertThat(testModeleMail.getDestinataire()).isEqualTo(DEFAULT_DESTINATAIRE);
        assertThat(testModeleMail.getObjetMail()).isEqualTo(DEFAULT_OBJET_MAIL);
        assertThat(testModeleMail.getSeuil()).isEqualTo(DEFAULT_SEUIL);
        assertThat(testModeleMail.isActif()).isEqualTo(DEFAULT_ACTIF);
    }

    @Test
    @Transactional
    public void createModeleMailWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = modeleMailRepository.findAll().size();

        // Create the ModeleMail with an existing ID
        modeleMail.setId(1L);
        ModeleMailDTO modeleMailDTO = modeleMailMapper.toDto(modeleMail);

        // An entity with an existing ID cannot be created, so this API call must fail
        restModeleMailMockMvc.perform(post("/api/modele-mails")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(modeleMailDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ModeleMail in the database
        List<ModeleMail> modeleMailList = modeleMailRepository.findAll();
        assertThat(modeleMailList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllModeleMails() throws Exception {
        // Initialize the database
        modeleMailRepository.saveAndFlush(modeleMail);

        // Get all the modeleMailList
        restModeleMailMockMvc.perform(get("/api/modele-mails?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(modeleMail.getId().intValue())))
            .andExpect(jsonPath("$.[*].destinataire").value(hasItem(DEFAULT_DESTINATAIRE)))
            .andExpect(jsonPath("$.[*].objetMail").value(hasItem(DEFAULT_OBJET_MAIL)))
            .andExpect(jsonPath("$.[*].seuil").value(hasItem(DEFAULT_SEUIL)))
            .andExpect(jsonPath("$.[*].actif").value(hasItem(DEFAULT_ACTIF.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getModeleMail() throws Exception {
        // Initialize the database
        modeleMailRepository.saveAndFlush(modeleMail);

        // Get the modeleMail
        restModeleMailMockMvc.perform(get("/api/modele-mails/{id}", modeleMail.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(modeleMail.getId().intValue()))
            .andExpect(jsonPath("$.destinataire").value(DEFAULT_DESTINATAIRE))
            .andExpect(jsonPath("$.objetMail").value(DEFAULT_OBJET_MAIL))
            .andExpect(jsonPath("$.seuil").value(DEFAULT_SEUIL))
            .andExpect(jsonPath("$.actif").value(DEFAULT_ACTIF.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingModeleMail() throws Exception {
        // Get the modeleMail
        restModeleMailMockMvc.perform(get("/api/modele-mails/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateModeleMail() throws Exception {
        // Initialize the database
        modeleMailRepository.saveAndFlush(modeleMail);

        int databaseSizeBeforeUpdate = modeleMailRepository.findAll().size();

        // Update the modeleMail
        ModeleMail updatedModeleMail = modeleMailRepository.findById(modeleMail.getId()).get();
        // Disconnect from session so that the updates on updatedModeleMail are not directly saved in db
        em.detach(updatedModeleMail);
        updatedModeleMail
            .destinataire(UPDATED_DESTINATAIRE)
            .objetMail(UPDATED_OBJET_MAIL)
            .seuil(UPDATED_SEUIL)
            .actif(UPDATED_ACTIF);
        ModeleMailDTO modeleMailDTO = modeleMailMapper.toDto(updatedModeleMail);

        restModeleMailMockMvc.perform(put("/api/modele-mails")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(modeleMailDTO)))
            .andExpect(status().isOk());

        // Validate the ModeleMail in the database
        List<ModeleMail> modeleMailList = modeleMailRepository.findAll();
        assertThat(modeleMailList).hasSize(databaseSizeBeforeUpdate);
        ModeleMail testModeleMail = modeleMailList.get(modeleMailList.size() - 1);
        assertThat(testModeleMail.getDestinataire()).isEqualTo(UPDATED_DESTINATAIRE);
        assertThat(testModeleMail.getObjetMail()).isEqualTo(UPDATED_OBJET_MAIL);
        assertThat(testModeleMail.getSeuil()).isEqualTo(UPDATED_SEUIL);
        assertThat(testModeleMail.isActif()).isEqualTo(UPDATED_ACTIF);
    }

    @Test
    @Transactional
    public void updateNonExistingModeleMail() throws Exception {
        int databaseSizeBeforeUpdate = modeleMailRepository.findAll().size();

        // Create the ModeleMail
        ModeleMailDTO modeleMailDTO = modeleMailMapper.toDto(modeleMail);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restModeleMailMockMvc.perform(put("/api/modele-mails")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(modeleMailDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ModeleMail in the database
        List<ModeleMail> modeleMailList = modeleMailRepository.findAll();
        assertThat(modeleMailList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteModeleMail() throws Exception {
        // Initialize the database
        modeleMailRepository.saveAndFlush(modeleMail);

        int databaseSizeBeforeDelete = modeleMailRepository.findAll().size();

        // Delete the modeleMail
        restModeleMailMockMvc.perform(delete("/api/modele-mails/{id}", modeleMail.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ModeleMail> modeleMailList = modeleMailRepository.findAll();
        assertThat(modeleMailList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
