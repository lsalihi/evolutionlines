package com.transport.landlines.web.rest;

import com.transport.landlines.ReferentielApp;
import com.transport.landlines.domain.Autocar;
import com.transport.landlines.repository.AutocarRepository;
import com.transport.landlines.service.AutocarService;
import com.transport.landlines.service.dto.AutocarDTO;
import com.transport.landlines.service.mapper.AutocarMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AutocarResource} REST controller.
 */
@SpringBootTest(classes = ReferentielApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AutocarResourceIT {

    private static final String DEFAULT_NUMERO_SOCIETE = "AAAAAAAAAA";
    private static final String UPDATED_NUMERO_SOCIETE = "BBBBBBBBBB";

    private static final String DEFAULT_MARQUE = "AAAAAAAAAA";
    private static final String UPDATED_MARQUE = "BBBBBBBBBB";

    private static final String DEFAULT_IMMATRICULATION = "AAAAAAAAAA";
    private static final String UPDATED_IMMATRICULATION = "BBBBBBBBBB";

    private static final Integer DEFAULT_CAPACITE = 1;
    private static final Integer UPDATED_CAPACITE = 2;

    private static final Boolean DEFAULT_ACTIF = false;
    private static final Boolean UPDATED_ACTIF = true;

    @Autowired
    private AutocarRepository autocarRepository;

    @Autowired
    private AutocarMapper autocarMapper;

    @Autowired
    private AutocarService autocarService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAutocarMockMvc;

    private Autocar autocar;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Autocar createEntity(EntityManager em) {
        Autocar autocar = new Autocar()
            .numeroSociete(DEFAULT_NUMERO_SOCIETE)
            .marque(DEFAULT_MARQUE)
            .immatriculation(DEFAULT_IMMATRICULATION)
            .capacite(DEFAULT_CAPACITE)
            .actif(DEFAULT_ACTIF);
        return autocar;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Autocar createUpdatedEntity(EntityManager em) {
        Autocar autocar = new Autocar()
            .numeroSociete(UPDATED_NUMERO_SOCIETE)
            .marque(UPDATED_MARQUE)
            .immatriculation(UPDATED_IMMATRICULATION)
            .capacite(UPDATED_CAPACITE)
            .actif(UPDATED_ACTIF);
        return autocar;
    }

    @BeforeEach
    public void initTest() {
        autocar = createEntity(em);
    }

    @Test
    @Transactional
    public void createAutocar() throws Exception {
        int databaseSizeBeforeCreate = autocarRepository.findAll().size();
        // Create the Autocar
        AutocarDTO autocarDTO = autocarMapper.toDto(autocar);
        restAutocarMockMvc.perform(post("/api/autocars")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(autocarDTO)))
            .andExpect(status().isCreated());

        // Validate the Autocar in the database
        List<Autocar> autocarList = autocarRepository.findAll();
        assertThat(autocarList).hasSize(databaseSizeBeforeCreate + 1);
        Autocar testAutocar = autocarList.get(autocarList.size() - 1);
        assertThat(testAutocar.getNumeroSociete()).isEqualTo(DEFAULT_NUMERO_SOCIETE);
        assertThat(testAutocar.getMarque()).isEqualTo(DEFAULT_MARQUE);
        assertThat(testAutocar.getImmatriculation()).isEqualTo(DEFAULT_IMMATRICULATION);
        assertThat(testAutocar.getCapacite()).isEqualTo(DEFAULT_CAPACITE);
        assertThat(testAutocar.isActif()).isEqualTo(DEFAULT_ACTIF);
    }

    @Test
    @Transactional
    public void createAutocarWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = autocarRepository.findAll().size();

        // Create the Autocar with an existing ID
        autocar.setId(1L);
        AutocarDTO autocarDTO = autocarMapper.toDto(autocar);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAutocarMockMvc.perform(post("/api/autocars")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(autocarDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Autocar in the database
        List<Autocar> autocarList = autocarRepository.findAll();
        assertThat(autocarList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAutocars() throws Exception {
        // Initialize the database
        autocarRepository.saveAndFlush(autocar);

        // Get all the autocarList
        restAutocarMockMvc.perform(get("/api/autocars?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(autocar.getId().intValue())))
            .andExpect(jsonPath("$.[*].numeroSociete").value(hasItem(DEFAULT_NUMERO_SOCIETE)))
            .andExpect(jsonPath("$.[*].marque").value(hasItem(DEFAULT_MARQUE)))
            .andExpect(jsonPath("$.[*].immatriculation").value(hasItem(DEFAULT_IMMATRICULATION)))
            .andExpect(jsonPath("$.[*].capacite").value(hasItem(DEFAULT_CAPACITE)))
            .andExpect(jsonPath("$.[*].actif").value(hasItem(DEFAULT_ACTIF.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getAutocar() throws Exception {
        // Initialize the database
        autocarRepository.saveAndFlush(autocar);

        // Get the autocar
        restAutocarMockMvc.perform(get("/api/autocars/{id}", autocar.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(autocar.getId().intValue()))
            .andExpect(jsonPath("$.numeroSociete").value(DEFAULT_NUMERO_SOCIETE))
            .andExpect(jsonPath("$.marque").value(DEFAULT_MARQUE))
            .andExpect(jsonPath("$.immatriculation").value(DEFAULT_IMMATRICULATION))
            .andExpect(jsonPath("$.capacite").value(DEFAULT_CAPACITE))
            .andExpect(jsonPath("$.actif").value(DEFAULT_ACTIF.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingAutocar() throws Exception {
        // Get the autocar
        restAutocarMockMvc.perform(get("/api/autocars/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAutocar() throws Exception {
        // Initialize the database
        autocarRepository.saveAndFlush(autocar);

        int databaseSizeBeforeUpdate = autocarRepository.findAll().size();

        // Update the autocar
        Autocar updatedAutocar = autocarRepository.findById(autocar.getId()).get();
        // Disconnect from session so that the updates on updatedAutocar are not directly saved in db
        em.detach(updatedAutocar);
        updatedAutocar
            .numeroSociete(UPDATED_NUMERO_SOCIETE)
            .marque(UPDATED_MARQUE)
            .immatriculation(UPDATED_IMMATRICULATION)
            .capacite(UPDATED_CAPACITE)
            .actif(UPDATED_ACTIF);
        AutocarDTO autocarDTO = autocarMapper.toDto(updatedAutocar);

        restAutocarMockMvc.perform(put("/api/autocars")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(autocarDTO)))
            .andExpect(status().isOk());

        // Validate the Autocar in the database
        List<Autocar> autocarList = autocarRepository.findAll();
        assertThat(autocarList).hasSize(databaseSizeBeforeUpdate);
        Autocar testAutocar = autocarList.get(autocarList.size() - 1);
        assertThat(testAutocar.getNumeroSociete()).isEqualTo(UPDATED_NUMERO_SOCIETE);
        assertThat(testAutocar.getMarque()).isEqualTo(UPDATED_MARQUE);
        assertThat(testAutocar.getImmatriculation()).isEqualTo(UPDATED_IMMATRICULATION);
        assertThat(testAutocar.getCapacite()).isEqualTo(UPDATED_CAPACITE);
        assertThat(testAutocar.isActif()).isEqualTo(UPDATED_ACTIF);
    }

    @Test
    @Transactional
    public void updateNonExistingAutocar() throws Exception {
        int databaseSizeBeforeUpdate = autocarRepository.findAll().size();

        // Create the Autocar
        AutocarDTO autocarDTO = autocarMapper.toDto(autocar);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAutocarMockMvc.perform(put("/api/autocars")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(autocarDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Autocar in the database
        List<Autocar> autocarList = autocarRepository.findAll();
        assertThat(autocarList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAutocar() throws Exception {
        // Initialize the database
        autocarRepository.saveAndFlush(autocar);

        int databaseSizeBeforeDelete = autocarRepository.findAll().size();

        // Delete the autocar
        restAutocarMockMvc.perform(delete("/api/autocars/{id}", autocar.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Autocar> autocarList = autocarRepository.findAll();
        assertThat(autocarList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
