package com.transport.landlines.web.rest;

import com.transport.landlines.ReferentielApp;
import com.transport.landlines.domain.Agence;
import com.transport.landlines.repository.AgenceRepository;
import com.transport.landlines.service.AgenceService;
import com.transport.landlines.service.dto.AgenceDTO;
import com.transport.landlines.service.mapper.AgenceMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AgenceResource} REST controller.
 */
@SpringBootTest(classes = ReferentielApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AgenceResourceIT {

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_CODE_COMPTABLE = "AAAAAAAAAA";
    private static final String UPDATED_CODE_COMPTABLE = "BBBBBBBBBB";

    private static final String DEFAULT_IDENTIFICATION_FISCALE = "AAAAAAAAAA";
    private static final String UPDATED_IDENTIFICATION_FISCALE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_CREATION = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_CREATION = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DEBUT_SERVICE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DEBUT_SERVICE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_FIN_SERVICE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_FIN_SERVICE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DATE_OUVERTURE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_OUVERTURE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DATE_MISE_EN_PROD = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_MISE_EN_PROD = LocalDate.now(ZoneId.systemDefault());

    private static final Double DEFAULT_TAXE_PROFESSIONNELLES = 1D;
    private static final Double UPDATED_TAXE_PROFESSIONNELLES = 2D;

    private static final Double DEFAULT_FOND_DEPENSE = 1D;
    private static final Double UPDATED_FOND_DEPENSE = 2D;

    private static final Double DEFAULT_SEUIL_VERSEMENT_INTERMEDIAIRE = 1D;
    private static final Double UPDATED_SEUIL_VERSEMENT_INTERMEDIAIRE = 2D;

    private static final Boolean DEFAULT_INFORMATISE = false;
    private static final Boolean UPDATED_INFORMATISE = true;

    private static final LocalDate DEFAULT_DATE_DESACTIVATION = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_DESACTIVATION = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_MOTIF_DESACTIVATION = "AAAAAAAAAA";
    private static final String UPDATED_MOTIF_DESACTIVATION = "BBBBBBBBBB";

    private static final String DEFAULT_MOTIF_FERMETURE = "AAAAAAAAAA";
    private static final String UPDATED_MOTIF_FERMETURE = "BBBBBBBBBB";

    private static final String DEFAULT_MOTIF_REOUVERTURE = "AAAAAAAAAA";
    private static final String UPDATED_MOTIF_REOUVERTURE = "BBBBBBBBBB";

    private static final String DEFAULT_MOTIF_STATUT_METIER = "AAAAAAAAAA";
    private static final String UPDATED_MOTIF_STATUT_METIER = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_FERMETURE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_FERMETURE = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_ACTIF = false;
    private static final Boolean UPDATED_ACTIF = true;

    @Autowired
    private AgenceRepository agenceRepository;

    @Autowired
    private AgenceMapper agenceMapper;

    @Autowired
    private AgenceService agenceService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAgenceMockMvc;

    private Agence agence;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Agence createEntity(EntityManager em) {
        Agence agence = new Agence()
            .libelle(DEFAULT_LIBELLE)
            .code(DEFAULT_CODE)
            .codeComptable(DEFAULT_CODE_COMPTABLE)
            .identificationFiscale(DEFAULT_IDENTIFICATION_FISCALE)
            .dateCreation(DEFAULT_DATE_CREATION)
            .debutService(DEFAULT_DEBUT_SERVICE)
            .finService(DEFAULT_FIN_SERVICE)
            .dateOuverture(DEFAULT_DATE_OUVERTURE)
            .dateMiseEnProd(DEFAULT_DATE_MISE_EN_PROD)
            .taxeProfessionnelles(DEFAULT_TAXE_PROFESSIONNELLES)
            .fondDepense(DEFAULT_FOND_DEPENSE)
            .seuilVersementIntermediaire(DEFAULT_SEUIL_VERSEMENT_INTERMEDIAIRE)
            .informatise(DEFAULT_INFORMATISE)
            .dateDesactivation(DEFAULT_DATE_DESACTIVATION)
            .motifDesactivation(DEFAULT_MOTIF_DESACTIVATION)
            .motifFermeture(DEFAULT_MOTIF_FERMETURE)
            .motifReouverture(DEFAULT_MOTIF_REOUVERTURE)
            .motifStatutMetier(DEFAULT_MOTIF_STATUT_METIER)
            .dateFermeture(DEFAULT_DATE_FERMETURE)
            .actif(DEFAULT_ACTIF);
        return agence;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Agence createUpdatedEntity(EntityManager em) {
        Agence agence = new Agence()
            .libelle(UPDATED_LIBELLE)
            .code(UPDATED_CODE)
            .codeComptable(UPDATED_CODE_COMPTABLE)
            .identificationFiscale(UPDATED_IDENTIFICATION_FISCALE)
            .dateCreation(UPDATED_DATE_CREATION)
            .debutService(UPDATED_DEBUT_SERVICE)
            .finService(UPDATED_FIN_SERVICE)
            .dateOuverture(UPDATED_DATE_OUVERTURE)
            .dateMiseEnProd(UPDATED_DATE_MISE_EN_PROD)
            .taxeProfessionnelles(UPDATED_TAXE_PROFESSIONNELLES)
            .fondDepense(UPDATED_FOND_DEPENSE)
            .seuilVersementIntermediaire(UPDATED_SEUIL_VERSEMENT_INTERMEDIAIRE)
            .informatise(UPDATED_INFORMATISE)
            .dateDesactivation(UPDATED_DATE_DESACTIVATION)
            .motifDesactivation(UPDATED_MOTIF_DESACTIVATION)
            .motifFermeture(UPDATED_MOTIF_FERMETURE)
            .motifReouverture(UPDATED_MOTIF_REOUVERTURE)
            .motifStatutMetier(UPDATED_MOTIF_STATUT_METIER)
            .dateFermeture(UPDATED_DATE_FERMETURE)
            .actif(UPDATED_ACTIF);
        return agence;
    }

    @BeforeEach
    public void initTest() {
        agence = createEntity(em);
    }

    @Test
    @Transactional
    public void createAgence() throws Exception {
        int databaseSizeBeforeCreate = agenceRepository.findAll().size();
        // Create the Agence
        AgenceDTO agenceDTO = agenceMapper.toDto(agence);
        restAgenceMockMvc.perform(post("/api/agences")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(agenceDTO)))
            .andExpect(status().isCreated());

        // Validate the Agence in the database
        List<Agence> agenceList = agenceRepository.findAll();
        assertThat(agenceList).hasSize(databaseSizeBeforeCreate + 1);
        Agence testAgence = agenceList.get(agenceList.size() - 1);
        assertThat(testAgence.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
        assertThat(testAgence.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testAgence.getCodeComptable()).isEqualTo(DEFAULT_CODE_COMPTABLE);
        assertThat(testAgence.getIdentificationFiscale()).isEqualTo(DEFAULT_IDENTIFICATION_FISCALE);
        assertThat(testAgence.getDateCreation()).isEqualTo(DEFAULT_DATE_CREATION);
        assertThat(testAgence.getDebutService()).isEqualTo(DEFAULT_DEBUT_SERVICE);
        assertThat(testAgence.getFinService()).isEqualTo(DEFAULT_FIN_SERVICE);
        assertThat(testAgence.getDateOuverture()).isEqualTo(DEFAULT_DATE_OUVERTURE);
        assertThat(testAgence.getDateMiseEnProd()).isEqualTo(DEFAULT_DATE_MISE_EN_PROD);
        assertThat(testAgence.getTaxeProfessionnelles()).isEqualTo(DEFAULT_TAXE_PROFESSIONNELLES);
        assertThat(testAgence.getFondDepense()).isEqualTo(DEFAULT_FOND_DEPENSE);
        assertThat(testAgence.getSeuilVersementIntermediaire()).isEqualTo(DEFAULT_SEUIL_VERSEMENT_INTERMEDIAIRE);
        assertThat(testAgence.isInformatise()).isEqualTo(DEFAULT_INFORMATISE);
        assertThat(testAgence.getDateDesactivation()).isEqualTo(DEFAULT_DATE_DESACTIVATION);
        assertThat(testAgence.getMotifDesactivation()).isEqualTo(DEFAULT_MOTIF_DESACTIVATION);
        assertThat(testAgence.getMotifFermeture()).isEqualTo(DEFAULT_MOTIF_FERMETURE);
        assertThat(testAgence.getMotifReouverture()).isEqualTo(DEFAULT_MOTIF_REOUVERTURE);
        assertThat(testAgence.getMotifStatutMetier()).isEqualTo(DEFAULT_MOTIF_STATUT_METIER);
        assertThat(testAgence.getDateFermeture()).isEqualTo(DEFAULT_DATE_FERMETURE);
        assertThat(testAgence.isActif()).isEqualTo(DEFAULT_ACTIF);
    }

    @Test
    @Transactional
    public void createAgenceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = agenceRepository.findAll().size();

        // Create the Agence with an existing ID
        agence.setId(1L);
        AgenceDTO agenceDTO = agenceMapper.toDto(agence);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAgenceMockMvc.perform(post("/api/agences")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(agenceDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Agence in the database
        List<Agence> agenceList = agenceRepository.findAll();
        assertThat(agenceList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAgences() throws Exception {
        // Initialize the database
        agenceRepository.saveAndFlush(agence);

        // Get all the agenceList
        restAgenceMockMvc.perform(get("/api/agences?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(agence.getId().intValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].codeComptable").value(hasItem(DEFAULT_CODE_COMPTABLE)))
            .andExpect(jsonPath("$.[*].identificationFiscale").value(hasItem(DEFAULT_IDENTIFICATION_FISCALE)))
            .andExpect(jsonPath("$.[*].dateCreation").value(hasItem(DEFAULT_DATE_CREATION.toString())))
            .andExpect(jsonPath("$.[*].debutService").value(hasItem(DEFAULT_DEBUT_SERVICE.toString())))
            .andExpect(jsonPath("$.[*].finService").value(hasItem(DEFAULT_FIN_SERVICE.toString())))
            .andExpect(jsonPath("$.[*].dateOuverture").value(hasItem(DEFAULT_DATE_OUVERTURE.toString())))
            .andExpect(jsonPath("$.[*].dateMiseEnProd").value(hasItem(DEFAULT_DATE_MISE_EN_PROD.toString())))
            .andExpect(jsonPath("$.[*].taxeProfessionnelles").value(hasItem(DEFAULT_TAXE_PROFESSIONNELLES.doubleValue())))
            .andExpect(jsonPath("$.[*].fondDepense").value(hasItem(DEFAULT_FOND_DEPENSE.doubleValue())))
            .andExpect(jsonPath("$.[*].seuilVersementIntermediaire").value(hasItem(DEFAULT_SEUIL_VERSEMENT_INTERMEDIAIRE.doubleValue())))
            .andExpect(jsonPath("$.[*].informatise").value(hasItem(DEFAULT_INFORMATISE.booleanValue())))
            .andExpect(jsonPath("$.[*].dateDesactivation").value(hasItem(DEFAULT_DATE_DESACTIVATION.toString())))
            .andExpect(jsonPath("$.[*].motifDesactivation").value(hasItem(DEFAULT_MOTIF_DESACTIVATION)))
            .andExpect(jsonPath("$.[*].motifFermeture").value(hasItem(DEFAULT_MOTIF_FERMETURE)))
            .andExpect(jsonPath("$.[*].motifReouverture").value(hasItem(DEFAULT_MOTIF_REOUVERTURE)))
            .andExpect(jsonPath("$.[*].motifStatutMetier").value(hasItem(DEFAULT_MOTIF_STATUT_METIER)))
            .andExpect(jsonPath("$.[*].dateFermeture").value(hasItem(DEFAULT_DATE_FERMETURE.toString())))
            .andExpect(jsonPath("$.[*].actif").value(hasItem(DEFAULT_ACTIF.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getAgence() throws Exception {
        // Initialize the database
        agenceRepository.saveAndFlush(agence);

        // Get the agence
        restAgenceMockMvc.perform(get("/api/agences/{id}", agence.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(agence.getId().intValue()))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.codeComptable").value(DEFAULT_CODE_COMPTABLE))
            .andExpect(jsonPath("$.identificationFiscale").value(DEFAULT_IDENTIFICATION_FISCALE))
            .andExpect(jsonPath("$.dateCreation").value(DEFAULT_DATE_CREATION.toString()))
            .andExpect(jsonPath("$.debutService").value(DEFAULT_DEBUT_SERVICE.toString()))
            .andExpect(jsonPath("$.finService").value(DEFAULT_FIN_SERVICE.toString()))
            .andExpect(jsonPath("$.dateOuverture").value(DEFAULT_DATE_OUVERTURE.toString()))
            .andExpect(jsonPath("$.dateMiseEnProd").value(DEFAULT_DATE_MISE_EN_PROD.toString()))
            .andExpect(jsonPath("$.taxeProfessionnelles").value(DEFAULT_TAXE_PROFESSIONNELLES.doubleValue()))
            .andExpect(jsonPath("$.fondDepense").value(DEFAULT_FOND_DEPENSE.doubleValue()))
            .andExpect(jsonPath("$.seuilVersementIntermediaire").value(DEFAULT_SEUIL_VERSEMENT_INTERMEDIAIRE.doubleValue()))
            .andExpect(jsonPath("$.informatise").value(DEFAULT_INFORMATISE.booleanValue()))
            .andExpect(jsonPath("$.dateDesactivation").value(DEFAULT_DATE_DESACTIVATION.toString()))
            .andExpect(jsonPath("$.motifDesactivation").value(DEFAULT_MOTIF_DESACTIVATION))
            .andExpect(jsonPath("$.motifFermeture").value(DEFAULT_MOTIF_FERMETURE))
            .andExpect(jsonPath("$.motifReouverture").value(DEFAULT_MOTIF_REOUVERTURE))
            .andExpect(jsonPath("$.motifStatutMetier").value(DEFAULT_MOTIF_STATUT_METIER))
            .andExpect(jsonPath("$.dateFermeture").value(DEFAULT_DATE_FERMETURE.toString()))
            .andExpect(jsonPath("$.actif").value(DEFAULT_ACTIF.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingAgence() throws Exception {
        // Get the agence
        restAgenceMockMvc.perform(get("/api/agences/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAgence() throws Exception {
        // Initialize the database
        agenceRepository.saveAndFlush(agence);

        int databaseSizeBeforeUpdate = agenceRepository.findAll().size();

        // Update the agence
        Agence updatedAgence = agenceRepository.findById(agence.getId()).get();
        // Disconnect from session so that the updates on updatedAgence are not directly saved in db
        em.detach(updatedAgence);
        updatedAgence
            .libelle(UPDATED_LIBELLE)
            .code(UPDATED_CODE)
            .codeComptable(UPDATED_CODE_COMPTABLE)
            .identificationFiscale(UPDATED_IDENTIFICATION_FISCALE)
            .dateCreation(UPDATED_DATE_CREATION)
            .debutService(UPDATED_DEBUT_SERVICE)
            .finService(UPDATED_FIN_SERVICE)
            .dateOuverture(UPDATED_DATE_OUVERTURE)
            .dateMiseEnProd(UPDATED_DATE_MISE_EN_PROD)
            .taxeProfessionnelles(UPDATED_TAXE_PROFESSIONNELLES)
            .fondDepense(UPDATED_FOND_DEPENSE)
            .seuilVersementIntermediaire(UPDATED_SEUIL_VERSEMENT_INTERMEDIAIRE)
            .informatise(UPDATED_INFORMATISE)
            .dateDesactivation(UPDATED_DATE_DESACTIVATION)
            .motifDesactivation(UPDATED_MOTIF_DESACTIVATION)
            .motifFermeture(UPDATED_MOTIF_FERMETURE)
            .motifReouverture(UPDATED_MOTIF_REOUVERTURE)
            .motifStatutMetier(UPDATED_MOTIF_STATUT_METIER)
            .dateFermeture(UPDATED_DATE_FERMETURE)
            .actif(UPDATED_ACTIF);
        AgenceDTO agenceDTO = agenceMapper.toDto(updatedAgence);

        restAgenceMockMvc.perform(put("/api/agences")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(agenceDTO)))
            .andExpect(status().isOk());

        // Validate the Agence in the database
        List<Agence> agenceList = agenceRepository.findAll();
        assertThat(agenceList).hasSize(databaseSizeBeforeUpdate);
        Agence testAgence = agenceList.get(agenceList.size() - 1);
        assertThat(testAgence.getLibelle()).isEqualTo(UPDATED_LIBELLE);
        assertThat(testAgence.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testAgence.getCodeComptable()).isEqualTo(UPDATED_CODE_COMPTABLE);
        assertThat(testAgence.getIdentificationFiscale()).isEqualTo(UPDATED_IDENTIFICATION_FISCALE);
        assertThat(testAgence.getDateCreation()).isEqualTo(UPDATED_DATE_CREATION);
        assertThat(testAgence.getDebutService()).isEqualTo(UPDATED_DEBUT_SERVICE);
        assertThat(testAgence.getFinService()).isEqualTo(UPDATED_FIN_SERVICE);
        assertThat(testAgence.getDateOuverture()).isEqualTo(UPDATED_DATE_OUVERTURE);
        assertThat(testAgence.getDateMiseEnProd()).isEqualTo(UPDATED_DATE_MISE_EN_PROD);
        assertThat(testAgence.getTaxeProfessionnelles()).isEqualTo(UPDATED_TAXE_PROFESSIONNELLES);
        assertThat(testAgence.getFondDepense()).isEqualTo(UPDATED_FOND_DEPENSE);
        assertThat(testAgence.getSeuilVersementIntermediaire()).isEqualTo(UPDATED_SEUIL_VERSEMENT_INTERMEDIAIRE);
        assertThat(testAgence.isInformatise()).isEqualTo(UPDATED_INFORMATISE);
        assertThat(testAgence.getDateDesactivation()).isEqualTo(UPDATED_DATE_DESACTIVATION);
        assertThat(testAgence.getMotifDesactivation()).isEqualTo(UPDATED_MOTIF_DESACTIVATION);
        assertThat(testAgence.getMotifFermeture()).isEqualTo(UPDATED_MOTIF_FERMETURE);
        assertThat(testAgence.getMotifReouverture()).isEqualTo(UPDATED_MOTIF_REOUVERTURE);
        assertThat(testAgence.getMotifStatutMetier()).isEqualTo(UPDATED_MOTIF_STATUT_METIER);
        assertThat(testAgence.getDateFermeture()).isEqualTo(UPDATED_DATE_FERMETURE);
        assertThat(testAgence.isActif()).isEqualTo(UPDATED_ACTIF);
    }

    @Test
    @Transactional
    public void updateNonExistingAgence() throws Exception {
        int databaseSizeBeforeUpdate = agenceRepository.findAll().size();

        // Create the Agence
        AgenceDTO agenceDTO = agenceMapper.toDto(agence);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAgenceMockMvc.perform(put("/api/agences")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(agenceDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Agence in the database
        List<Agence> agenceList = agenceRepository.findAll();
        assertThat(agenceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAgence() throws Exception {
        // Initialize the database
        agenceRepository.saveAndFlush(agence);

        int databaseSizeBeforeDelete = agenceRepository.findAll().size();

        // Delete the agence
        restAgenceMockMvc.perform(delete("/api/agences/{id}", agence.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Agence> agenceList = agenceRepository.findAll();
        assertThat(agenceList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
