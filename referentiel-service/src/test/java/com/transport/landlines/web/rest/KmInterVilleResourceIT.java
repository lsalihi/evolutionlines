package com.transport.landlines.web.rest;

import com.transport.landlines.ReferentielApp;
import com.transport.landlines.domain.KmInterVille;
import com.transport.landlines.repository.KmInterVilleRepository;
import com.transport.landlines.service.KmInterVilleService;
import com.transport.landlines.service.dto.KmInterVilleDTO;
import com.transport.landlines.service.mapper.KmInterVilleMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link KmInterVilleResource} REST controller.
 */
@SpringBootTest(classes = ReferentielApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class KmInterVilleResourceIT {

    private static final Integer DEFAULT_KILOMETRES = 1;
    private static final Integer UPDATED_KILOMETRES = 2;

    private static final Boolean DEFAULT_STATUT = false;
    private static final Boolean UPDATED_STATUT = true;

    @Autowired
    private KmInterVilleRepository kmInterVilleRepository;

    @Autowired
    private KmInterVilleMapper kmInterVilleMapper;

    @Autowired
    private KmInterVilleService kmInterVilleService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restKmInterVilleMockMvc;

    private KmInterVille kmInterVille;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static KmInterVille createEntity(EntityManager em) {
        KmInterVille kmInterVille = new KmInterVille()
            .kilometres(DEFAULT_KILOMETRES)
            .statut(DEFAULT_STATUT);
        return kmInterVille;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static KmInterVille createUpdatedEntity(EntityManager em) {
        KmInterVille kmInterVille = new KmInterVille()
            .kilometres(UPDATED_KILOMETRES)
            .statut(UPDATED_STATUT);
        return kmInterVille;
    }

    @BeforeEach
    public void initTest() {
        kmInterVille = createEntity(em);
    }

    @Test
    @Transactional
    public void createKmInterVille() throws Exception {
        int databaseSizeBeforeCreate = kmInterVilleRepository.findAll().size();
        // Create the KmInterVille
        KmInterVilleDTO kmInterVilleDTO = kmInterVilleMapper.toDto(kmInterVille);
        restKmInterVilleMockMvc.perform(post("/api/km-inter-villes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(kmInterVilleDTO)))
            .andExpect(status().isCreated());

        // Validate the KmInterVille in the database
        List<KmInterVille> kmInterVilleList = kmInterVilleRepository.findAll();
        assertThat(kmInterVilleList).hasSize(databaseSizeBeforeCreate + 1);
        KmInterVille testKmInterVille = kmInterVilleList.get(kmInterVilleList.size() - 1);
        assertThat(testKmInterVille.getKilometres()).isEqualTo(DEFAULT_KILOMETRES);
        assertThat(testKmInterVille.isStatut()).isEqualTo(DEFAULT_STATUT);
    }

    @Test
    @Transactional
    public void createKmInterVilleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = kmInterVilleRepository.findAll().size();

        // Create the KmInterVille with an existing ID
        kmInterVille.setId(1L);
        KmInterVilleDTO kmInterVilleDTO = kmInterVilleMapper.toDto(kmInterVille);

        // An entity with an existing ID cannot be created, so this API call must fail
        restKmInterVilleMockMvc.perform(post("/api/km-inter-villes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(kmInterVilleDTO)))
            .andExpect(status().isBadRequest());

        // Validate the KmInterVille in the database
        List<KmInterVille> kmInterVilleList = kmInterVilleRepository.findAll();
        assertThat(kmInterVilleList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllKmInterVilles() throws Exception {
        // Initialize the database
        kmInterVilleRepository.saveAndFlush(kmInterVille);

        // Get all the kmInterVilleList
        restKmInterVilleMockMvc.perform(get("/api/km-inter-villes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(kmInterVille.getId().intValue())))
            .andExpect(jsonPath("$.[*].kilometres").value(hasItem(DEFAULT_KILOMETRES)))
            .andExpect(jsonPath("$.[*].statut").value(hasItem(DEFAULT_STATUT.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getKmInterVille() throws Exception {
        // Initialize the database
        kmInterVilleRepository.saveAndFlush(kmInterVille);

        // Get the kmInterVille
        restKmInterVilleMockMvc.perform(get("/api/km-inter-villes/{id}", kmInterVille.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(kmInterVille.getId().intValue()))
            .andExpect(jsonPath("$.kilometres").value(DEFAULT_KILOMETRES))
            .andExpect(jsonPath("$.statut").value(DEFAULT_STATUT.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingKmInterVille() throws Exception {
        // Get the kmInterVille
        restKmInterVilleMockMvc.perform(get("/api/km-inter-villes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateKmInterVille() throws Exception {
        // Initialize the database
        kmInterVilleRepository.saveAndFlush(kmInterVille);

        int databaseSizeBeforeUpdate = kmInterVilleRepository.findAll().size();

        // Update the kmInterVille
        KmInterVille updatedKmInterVille = kmInterVilleRepository.findById(kmInterVille.getId()).get();
        // Disconnect from session so that the updates on updatedKmInterVille are not directly saved in db
        em.detach(updatedKmInterVille);
        updatedKmInterVille
            .kilometres(UPDATED_KILOMETRES)
            .statut(UPDATED_STATUT);
        KmInterVilleDTO kmInterVilleDTO = kmInterVilleMapper.toDto(updatedKmInterVille);

        restKmInterVilleMockMvc.perform(put("/api/km-inter-villes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(kmInterVilleDTO)))
            .andExpect(status().isOk());

        // Validate the KmInterVille in the database
        List<KmInterVille> kmInterVilleList = kmInterVilleRepository.findAll();
        assertThat(kmInterVilleList).hasSize(databaseSizeBeforeUpdate);
        KmInterVille testKmInterVille = kmInterVilleList.get(kmInterVilleList.size() - 1);
        assertThat(testKmInterVille.getKilometres()).isEqualTo(UPDATED_KILOMETRES);
        assertThat(testKmInterVille.isStatut()).isEqualTo(UPDATED_STATUT);
    }

    @Test
    @Transactional
    public void updateNonExistingKmInterVille() throws Exception {
        int databaseSizeBeforeUpdate = kmInterVilleRepository.findAll().size();

        // Create the KmInterVille
        KmInterVilleDTO kmInterVilleDTO = kmInterVilleMapper.toDto(kmInterVille);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKmInterVilleMockMvc.perform(put("/api/km-inter-villes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(kmInterVilleDTO)))
            .andExpect(status().isBadRequest());

        // Validate the KmInterVille in the database
        List<KmInterVille> kmInterVilleList = kmInterVilleRepository.findAll();
        assertThat(kmInterVilleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteKmInterVille() throws Exception {
        // Initialize the database
        kmInterVilleRepository.saveAndFlush(kmInterVille);

        int databaseSizeBeforeDelete = kmInterVilleRepository.findAll().size();

        // Delete the kmInterVille
        restKmInterVilleMockMvc.perform(delete("/api/km-inter-villes/{id}", kmInterVille.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<KmInterVille> kmInterVilleList = kmInterVilleRepository.findAll();
        assertThat(kmInterVilleList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
