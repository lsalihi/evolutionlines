package com.transport.landlines.web.rest;

import com.transport.landlines.ReferentielApp;
import com.transport.landlines.domain.HoraireService;
import com.transport.landlines.repository.HoraireServiceRepository;
import com.transport.landlines.service.HoraireServiceService;
import com.transport.landlines.service.dto.HoraireServiceDTO;
import com.transport.landlines.service.mapper.HoraireServiceMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link HoraireServiceResource} REST controller.
 */
@SpringBootTest(classes = ReferentielApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class HoraireServiceResourceIT {

    private static final LocalDate DEFAULT_OUVERTURE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_OUVERTURE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_FERMETURE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_FERMETURE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private HoraireServiceRepository horaireServiceRepository;

    @Autowired
    private HoraireServiceMapper horaireServiceMapper;

    @Autowired
    private HoraireServiceService horaireServiceService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restHoraireServiceMockMvc;

    private HoraireService horaireService;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HoraireService createEntity(EntityManager em) {
        HoraireService horaireService = new HoraireService()
            .ouverture(DEFAULT_OUVERTURE)
            .fermeture(DEFAULT_FERMETURE);
        return horaireService;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HoraireService createUpdatedEntity(EntityManager em) {
        HoraireService horaireService = new HoraireService()
            .ouverture(UPDATED_OUVERTURE)
            .fermeture(UPDATED_FERMETURE);
        return horaireService;
    }

    @BeforeEach
    public void initTest() {
        horaireService = createEntity(em);
    }

    @Test
    @Transactional
    public void createHoraireService() throws Exception {
        int databaseSizeBeforeCreate = horaireServiceRepository.findAll().size();
        // Create the HoraireService
        HoraireServiceDTO horaireServiceDTO = horaireServiceMapper.toDto(horaireService);
        restHoraireServiceMockMvc.perform(post("/api/horaire-services")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(horaireServiceDTO)))
            .andExpect(status().isCreated());

        // Validate the HoraireService in the database
        List<HoraireService> horaireServiceList = horaireServiceRepository.findAll();
        assertThat(horaireServiceList).hasSize(databaseSizeBeforeCreate + 1);
        HoraireService testHoraireService = horaireServiceList.get(horaireServiceList.size() - 1);
        assertThat(testHoraireService.getOuverture()).isEqualTo(DEFAULT_OUVERTURE);
        assertThat(testHoraireService.getFermeture()).isEqualTo(DEFAULT_FERMETURE);
    }

    @Test
    @Transactional
    public void createHoraireServiceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = horaireServiceRepository.findAll().size();

        // Create the HoraireService with an existing ID
        horaireService.setId(1L);
        HoraireServiceDTO horaireServiceDTO = horaireServiceMapper.toDto(horaireService);

        // An entity with an existing ID cannot be created, so this API call must fail
        restHoraireServiceMockMvc.perform(post("/api/horaire-services")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(horaireServiceDTO)))
            .andExpect(status().isBadRequest());

        // Validate the HoraireService in the database
        List<HoraireService> horaireServiceList = horaireServiceRepository.findAll();
        assertThat(horaireServiceList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllHoraireServices() throws Exception {
        // Initialize the database
        horaireServiceRepository.saveAndFlush(horaireService);

        // Get all the horaireServiceList
        restHoraireServiceMockMvc.perform(get("/api/horaire-services?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(horaireService.getId().intValue())))
            .andExpect(jsonPath("$.[*].ouverture").value(hasItem(DEFAULT_OUVERTURE.toString())))
            .andExpect(jsonPath("$.[*].fermeture").value(hasItem(DEFAULT_FERMETURE.toString())));
    }
    
    @Test
    @Transactional
    public void getHoraireService() throws Exception {
        // Initialize the database
        horaireServiceRepository.saveAndFlush(horaireService);

        // Get the horaireService
        restHoraireServiceMockMvc.perform(get("/api/horaire-services/{id}", horaireService.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(horaireService.getId().intValue()))
            .andExpect(jsonPath("$.ouverture").value(DEFAULT_OUVERTURE.toString()))
            .andExpect(jsonPath("$.fermeture").value(DEFAULT_FERMETURE.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingHoraireService() throws Exception {
        // Get the horaireService
        restHoraireServiceMockMvc.perform(get("/api/horaire-services/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateHoraireService() throws Exception {
        // Initialize the database
        horaireServiceRepository.saveAndFlush(horaireService);

        int databaseSizeBeforeUpdate = horaireServiceRepository.findAll().size();

        // Update the horaireService
        HoraireService updatedHoraireService = horaireServiceRepository.findById(horaireService.getId()).get();
        // Disconnect from session so that the updates on updatedHoraireService are not directly saved in db
        em.detach(updatedHoraireService);
        updatedHoraireService
            .ouverture(UPDATED_OUVERTURE)
            .fermeture(UPDATED_FERMETURE);
        HoraireServiceDTO horaireServiceDTO = horaireServiceMapper.toDto(updatedHoraireService);

        restHoraireServiceMockMvc.perform(put("/api/horaire-services")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(horaireServiceDTO)))
            .andExpect(status().isOk());

        // Validate the HoraireService in the database
        List<HoraireService> horaireServiceList = horaireServiceRepository.findAll();
        assertThat(horaireServiceList).hasSize(databaseSizeBeforeUpdate);
        HoraireService testHoraireService = horaireServiceList.get(horaireServiceList.size() - 1);
        assertThat(testHoraireService.getOuverture()).isEqualTo(UPDATED_OUVERTURE);
        assertThat(testHoraireService.getFermeture()).isEqualTo(UPDATED_FERMETURE);
    }

    @Test
    @Transactional
    public void updateNonExistingHoraireService() throws Exception {
        int databaseSizeBeforeUpdate = horaireServiceRepository.findAll().size();

        // Create the HoraireService
        HoraireServiceDTO horaireServiceDTO = horaireServiceMapper.toDto(horaireService);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHoraireServiceMockMvc.perform(put("/api/horaire-services")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(horaireServiceDTO)))
            .andExpect(status().isBadRequest());

        // Validate the HoraireService in the database
        List<HoraireService> horaireServiceList = horaireServiceRepository.findAll();
        assertThat(horaireServiceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteHoraireService() throws Exception {
        // Initialize the database
        horaireServiceRepository.saveAndFlush(horaireService);

        int databaseSizeBeforeDelete = horaireServiceRepository.findAll().size();

        // Delete the horaireService
        restHoraireServiceMockMvc.perform(delete("/api/horaire-services/{id}", horaireService.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<HoraireService> horaireServiceList = horaireServiceRepository.findAll();
        assertThat(horaireServiceList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
