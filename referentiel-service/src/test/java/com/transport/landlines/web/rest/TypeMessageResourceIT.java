package com.transport.landlines.web.rest;

import com.transport.landlines.ReferentielApp;
import com.transport.landlines.domain.TypeMessage;
import com.transport.landlines.repository.TypeMessageRepository;
import com.transport.landlines.service.TypeMessageService;
import com.transport.landlines.service.dto.TypeMessageDTO;
import com.transport.landlines.service.mapper.TypeMessageMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TypeMessageResource} REST controller.
 */
@SpringBootTest(classes = ReferentielApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class TypeMessageResourceIT {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_TITRE = "AAAAAAAAAA";
    private static final String UPDATED_TITRE = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_CODE_FONCTION = "AAAAAAAAAA";
    private static final String UPDATED_CODE_FONCTION = "BBBBBBBBBB";

    @Autowired
    private TypeMessageRepository typeMessageRepository;

    @Autowired
    private TypeMessageMapper typeMessageMapper;

    @Autowired
    private TypeMessageService typeMessageService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTypeMessageMockMvc;

    private TypeMessage typeMessage;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TypeMessage createEntity(EntityManager em) {
        TypeMessage typeMessage = new TypeMessage()
            .description(DEFAULT_DESCRIPTION)
            .titre(DEFAULT_TITRE)
            .type(DEFAULT_TYPE)
            .codeFonction(DEFAULT_CODE_FONCTION);
        return typeMessage;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TypeMessage createUpdatedEntity(EntityManager em) {
        TypeMessage typeMessage = new TypeMessage()
            .description(UPDATED_DESCRIPTION)
            .titre(UPDATED_TITRE)
            .type(UPDATED_TYPE)
            .codeFonction(UPDATED_CODE_FONCTION);
        return typeMessage;
    }

    @BeforeEach
    public void initTest() {
        typeMessage = createEntity(em);
    }

    @Test
    @Transactional
    public void createTypeMessage() throws Exception {
        int databaseSizeBeforeCreate = typeMessageRepository.findAll().size();
        // Create the TypeMessage
        TypeMessageDTO typeMessageDTO = typeMessageMapper.toDto(typeMessage);
        restTypeMessageMockMvc.perform(post("/api/type-messages")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(typeMessageDTO)))
            .andExpect(status().isCreated());

        // Validate the TypeMessage in the database
        List<TypeMessage> typeMessageList = typeMessageRepository.findAll();
        assertThat(typeMessageList).hasSize(databaseSizeBeforeCreate + 1);
        TypeMessage testTypeMessage = typeMessageList.get(typeMessageList.size() - 1);
        assertThat(testTypeMessage.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testTypeMessage.getTitre()).isEqualTo(DEFAULT_TITRE);
        assertThat(testTypeMessage.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testTypeMessage.getCodeFonction()).isEqualTo(DEFAULT_CODE_FONCTION);
    }

    @Test
    @Transactional
    public void createTypeMessageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = typeMessageRepository.findAll().size();

        // Create the TypeMessage with an existing ID
        typeMessage.setId(1L);
        TypeMessageDTO typeMessageDTO = typeMessageMapper.toDto(typeMessage);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTypeMessageMockMvc.perform(post("/api/type-messages")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(typeMessageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TypeMessage in the database
        List<TypeMessage> typeMessageList = typeMessageRepository.findAll();
        assertThat(typeMessageList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTypeMessages() throws Exception {
        // Initialize the database
        typeMessageRepository.saveAndFlush(typeMessage);

        // Get all the typeMessageList
        restTypeMessageMockMvc.perform(get("/api/type-messages?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(typeMessage.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].titre").value(hasItem(DEFAULT_TITRE)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].codeFonction").value(hasItem(DEFAULT_CODE_FONCTION)));
    }
    
    @Test
    @Transactional
    public void getTypeMessage() throws Exception {
        // Initialize the database
        typeMessageRepository.saveAndFlush(typeMessage);

        // Get the typeMessage
        restTypeMessageMockMvc.perform(get("/api/type-messages/{id}", typeMessage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(typeMessage.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.titre").value(DEFAULT_TITRE))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.codeFonction").value(DEFAULT_CODE_FONCTION));
    }
    @Test
    @Transactional
    public void getNonExistingTypeMessage() throws Exception {
        // Get the typeMessage
        restTypeMessageMockMvc.perform(get("/api/type-messages/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTypeMessage() throws Exception {
        // Initialize the database
        typeMessageRepository.saveAndFlush(typeMessage);

        int databaseSizeBeforeUpdate = typeMessageRepository.findAll().size();

        // Update the typeMessage
        TypeMessage updatedTypeMessage = typeMessageRepository.findById(typeMessage.getId()).get();
        // Disconnect from session so that the updates on updatedTypeMessage are not directly saved in db
        em.detach(updatedTypeMessage);
        updatedTypeMessage
            .description(UPDATED_DESCRIPTION)
            .titre(UPDATED_TITRE)
            .type(UPDATED_TYPE)
            .codeFonction(UPDATED_CODE_FONCTION);
        TypeMessageDTO typeMessageDTO = typeMessageMapper.toDto(updatedTypeMessage);

        restTypeMessageMockMvc.perform(put("/api/type-messages")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(typeMessageDTO)))
            .andExpect(status().isOk());

        // Validate the TypeMessage in the database
        List<TypeMessage> typeMessageList = typeMessageRepository.findAll();
        assertThat(typeMessageList).hasSize(databaseSizeBeforeUpdate);
        TypeMessage testTypeMessage = typeMessageList.get(typeMessageList.size() - 1);
        assertThat(testTypeMessage.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testTypeMessage.getTitre()).isEqualTo(UPDATED_TITRE);
        assertThat(testTypeMessage.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testTypeMessage.getCodeFonction()).isEqualTo(UPDATED_CODE_FONCTION);
    }

    @Test
    @Transactional
    public void updateNonExistingTypeMessage() throws Exception {
        int databaseSizeBeforeUpdate = typeMessageRepository.findAll().size();

        // Create the TypeMessage
        TypeMessageDTO typeMessageDTO = typeMessageMapper.toDto(typeMessage);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTypeMessageMockMvc.perform(put("/api/type-messages")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(typeMessageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TypeMessage in the database
        List<TypeMessage> typeMessageList = typeMessageRepository.findAll();
        assertThat(typeMessageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTypeMessage() throws Exception {
        // Initialize the database
        typeMessageRepository.saveAndFlush(typeMessage);

        int databaseSizeBeforeDelete = typeMessageRepository.findAll().size();

        // Delete the typeMessage
        restTypeMessageMockMvc.perform(delete("/api/type-messages/{id}", typeMessage.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TypeMessage> typeMessageList = typeMessageRepository.findAll();
        assertThat(typeMessageList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
