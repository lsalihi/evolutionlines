package com.transport.landlines.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.transport.landlines.web.rest.TestUtil;

public class AutocarDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AutocarDTO.class);
        AutocarDTO autocarDTO1 = new AutocarDTO();
        autocarDTO1.setId(1L);
        AutocarDTO autocarDTO2 = new AutocarDTO();
        assertThat(autocarDTO1).isNotEqualTo(autocarDTO2);
        autocarDTO2.setId(autocarDTO1.getId());
        assertThat(autocarDTO1).isEqualTo(autocarDTO2);
        autocarDTO2.setId(2L);
        assertThat(autocarDTO1).isNotEqualTo(autocarDTO2);
        autocarDTO1.setId(null);
        assertThat(autocarDTO1).isNotEqualTo(autocarDTO2);
    }
}
