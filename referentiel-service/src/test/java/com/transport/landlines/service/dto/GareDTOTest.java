package com.transport.landlines.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.transport.landlines.web.rest.TestUtil;

public class GareDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(GareDTO.class);
        GareDTO gareDTO1 = new GareDTO();
        gareDTO1.setId(1L);
        GareDTO gareDTO2 = new GareDTO();
        assertThat(gareDTO1).isNotEqualTo(gareDTO2);
        gareDTO2.setId(gareDTO1.getId());
        assertThat(gareDTO1).isEqualTo(gareDTO2);
        gareDTO2.setId(2L);
        assertThat(gareDTO1).isNotEqualTo(gareDTO2);
        gareDTO1.setId(null);
        assertThat(gareDTO1).isNotEqualTo(gareDTO2);
    }
}
