package com.transport.landlines.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class TypeMessageMapperTest {

    private TypeMessageMapper typeMessageMapper;

    @BeforeEach
    public void setUp() {
        typeMessageMapper = new TypeMessageMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(typeMessageMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(typeMessageMapper.fromId(null)).isNull();
    }
}
