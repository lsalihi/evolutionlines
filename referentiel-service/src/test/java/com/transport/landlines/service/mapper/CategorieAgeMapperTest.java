package com.transport.landlines.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CategorieAgeMapperTest {

    private CategorieAgeMapper categorieAgeMapper;

    @BeforeEach
    public void setUp() {
        categorieAgeMapper = new CategorieAgeMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(categorieAgeMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(categorieAgeMapper.fromId(null)).isNull();
    }
}
