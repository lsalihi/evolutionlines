package com.transport.landlines.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.transport.landlines.web.rest.TestUtil;

public class CodificationDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CodificationDTO.class);
        CodificationDTO codificationDTO1 = new CodificationDTO();
        codificationDTO1.setId(1L);
        CodificationDTO codificationDTO2 = new CodificationDTO();
        assertThat(codificationDTO1).isNotEqualTo(codificationDTO2);
        codificationDTO2.setId(codificationDTO1.getId());
        assertThat(codificationDTO1).isEqualTo(codificationDTO2);
        codificationDTO2.setId(2L);
        assertThat(codificationDTO1).isNotEqualTo(codificationDTO2);
        codificationDTO1.setId(null);
        assertThat(codificationDTO1).isNotEqualTo(codificationDTO2);
    }
}
