package com.transport.landlines.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.transport.landlines.web.rest.TestUtil;

public class NationaliteDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(NationaliteDTO.class);
        NationaliteDTO nationaliteDTO1 = new NationaliteDTO();
        nationaliteDTO1.setId(1L);
        NationaliteDTO nationaliteDTO2 = new NationaliteDTO();
        assertThat(nationaliteDTO1).isNotEqualTo(nationaliteDTO2);
        nationaliteDTO2.setId(nationaliteDTO1.getId());
        assertThat(nationaliteDTO1).isEqualTo(nationaliteDTO2);
        nationaliteDTO2.setId(2L);
        assertThat(nationaliteDTO1).isNotEqualTo(nationaliteDTO2);
        nationaliteDTO1.setId(null);
        assertThat(nationaliteDTO1).isNotEqualTo(nationaliteDTO2);
    }
}
