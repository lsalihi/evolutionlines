package com.transport.landlines.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CompteBancaireMapperTest {

    private CompteBancaireMapper compteBancaireMapper;

    @BeforeEach
    public void setUp() {
        compteBancaireMapper = new CompteBancaireMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(compteBancaireMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(compteBancaireMapper.fromId(null)).isNull();
    }
}
