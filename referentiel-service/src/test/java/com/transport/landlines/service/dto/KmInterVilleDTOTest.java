package com.transport.landlines.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.transport.landlines.web.rest.TestUtil;

public class KmInterVilleDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(KmInterVilleDTO.class);
        KmInterVilleDTO kmInterVilleDTO1 = new KmInterVilleDTO();
        kmInterVilleDTO1.setId(1L);
        KmInterVilleDTO kmInterVilleDTO2 = new KmInterVilleDTO();
        assertThat(kmInterVilleDTO1).isNotEqualTo(kmInterVilleDTO2);
        kmInterVilleDTO2.setId(kmInterVilleDTO1.getId());
        assertThat(kmInterVilleDTO1).isEqualTo(kmInterVilleDTO2);
        kmInterVilleDTO2.setId(2L);
        assertThat(kmInterVilleDTO1).isNotEqualTo(kmInterVilleDTO2);
        kmInterVilleDTO1.setId(null);
        assertThat(kmInterVilleDTO1).isNotEqualTo(kmInterVilleDTO2);
    }
}
