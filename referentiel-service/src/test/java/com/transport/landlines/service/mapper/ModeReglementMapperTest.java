package com.transport.landlines.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ModeReglementMapperTest {

    private ModeReglementMapper modeReglementMapper;

    @BeforeEach
    public void setUp() {
        modeReglementMapper = new ModeReglementMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(modeReglementMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(modeReglementMapper.fromId(null)).isNull();
    }
}
