package com.transport.landlines.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class GareMapperTest {

    private GareMapper gareMapper;

    @BeforeEach
    public void setUp() {
        gareMapper = new GareMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(gareMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(gareMapper.fromId(null)).isNull();
    }
}
