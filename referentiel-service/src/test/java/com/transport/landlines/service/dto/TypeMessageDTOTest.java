package com.transport.landlines.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.transport.landlines.web.rest.TestUtil;

public class TypeMessageDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TypeMessageDTO.class);
        TypeMessageDTO typeMessageDTO1 = new TypeMessageDTO();
        typeMessageDTO1.setId(1L);
        TypeMessageDTO typeMessageDTO2 = new TypeMessageDTO();
        assertThat(typeMessageDTO1).isNotEqualTo(typeMessageDTO2);
        typeMessageDTO2.setId(typeMessageDTO1.getId());
        assertThat(typeMessageDTO1).isEqualTo(typeMessageDTO2);
        typeMessageDTO2.setId(2L);
        assertThat(typeMessageDTO1).isNotEqualTo(typeMessageDTO2);
        typeMessageDTO1.setId(null);
        assertThat(typeMessageDTO1).isNotEqualTo(typeMessageDTO2);
    }
}
