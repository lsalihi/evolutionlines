package com.transport.landlines.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class NationaliteMapperTest {

    private NationaliteMapper nationaliteMapper;

    @BeforeEach
    public void setUp() {
        nationaliteMapper = new NationaliteMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(nationaliteMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(nationaliteMapper.fromId(null)).isNull();
    }
}
