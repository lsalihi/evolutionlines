package com.transport.landlines.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.transport.landlines.web.rest.TestUtil;

public class HoraireServiceDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(HoraireServiceDTO.class);
        HoraireServiceDTO horaireServiceDTO1 = new HoraireServiceDTO();
        horaireServiceDTO1.setId(1L);
        HoraireServiceDTO horaireServiceDTO2 = new HoraireServiceDTO();
        assertThat(horaireServiceDTO1).isNotEqualTo(horaireServiceDTO2);
        horaireServiceDTO2.setId(horaireServiceDTO1.getId());
        assertThat(horaireServiceDTO1).isEqualTo(horaireServiceDTO2);
        horaireServiceDTO2.setId(2L);
        assertThat(horaireServiceDTO1).isNotEqualTo(horaireServiceDTO2);
        horaireServiceDTO1.setId(null);
        assertThat(horaireServiceDTO1).isNotEqualTo(horaireServiceDTO2);
    }
}
