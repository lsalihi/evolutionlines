package com.transport.landlines.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class AutocarMapperTest {

    private AutocarMapper autocarMapper;

    @BeforeEach
    public void setUp() {
        autocarMapper = new AutocarMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(autocarMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(autocarMapper.fromId(null)).isNull();
    }
}
