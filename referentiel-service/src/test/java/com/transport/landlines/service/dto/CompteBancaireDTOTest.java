package com.transport.landlines.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.transport.landlines.web.rest.TestUtil;

public class CompteBancaireDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompteBancaireDTO.class);
        CompteBancaireDTO compteBancaireDTO1 = new CompteBancaireDTO();
        compteBancaireDTO1.setId(1L);
        CompteBancaireDTO compteBancaireDTO2 = new CompteBancaireDTO();
        assertThat(compteBancaireDTO1).isNotEqualTo(compteBancaireDTO2);
        compteBancaireDTO2.setId(compteBancaireDTO1.getId());
        assertThat(compteBancaireDTO1).isEqualTo(compteBancaireDTO2);
        compteBancaireDTO2.setId(2L);
        assertThat(compteBancaireDTO1).isNotEqualTo(compteBancaireDTO2);
        compteBancaireDTO1.setId(null);
        assertThat(compteBancaireDTO1).isNotEqualTo(compteBancaireDTO2);
    }
}
