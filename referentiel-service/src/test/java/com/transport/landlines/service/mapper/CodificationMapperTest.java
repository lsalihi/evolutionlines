package com.transport.landlines.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CodificationMapperTest {

    private CodificationMapper codificationMapper;

    @BeforeEach
    public void setUp() {
        codificationMapper = new CodificationMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(codificationMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(codificationMapper.fromId(null)).isNull();
    }
}
