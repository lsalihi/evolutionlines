package com.transport.landlines.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class KmInterVilleMapperTest {

    private KmInterVilleMapper kmInterVilleMapper;

    @BeforeEach
    public void setUp() {
        kmInterVilleMapper = new KmInterVilleMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(kmInterVilleMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(kmInterVilleMapper.fromId(null)).isNull();
    }
}
