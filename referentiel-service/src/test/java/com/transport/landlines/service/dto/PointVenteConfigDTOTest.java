package com.transport.landlines.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.transport.landlines.web.rest.TestUtil;

public class PointVenteConfigDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PointVenteConfigDTO.class);
        PointVenteConfigDTO pointVenteConfigDTO1 = new PointVenteConfigDTO();
        pointVenteConfigDTO1.setId(1L);
        PointVenteConfigDTO pointVenteConfigDTO2 = new PointVenteConfigDTO();
        assertThat(pointVenteConfigDTO1).isNotEqualTo(pointVenteConfigDTO2);
        pointVenteConfigDTO2.setId(pointVenteConfigDTO1.getId());
        assertThat(pointVenteConfigDTO1).isEqualTo(pointVenteConfigDTO2);
        pointVenteConfigDTO2.setId(2L);
        assertThat(pointVenteConfigDTO1).isNotEqualTo(pointVenteConfigDTO2);
        pointVenteConfigDTO1.setId(null);
        assertThat(pointVenteConfigDTO1).isNotEqualTo(pointVenteConfigDTO2);
    }
}
