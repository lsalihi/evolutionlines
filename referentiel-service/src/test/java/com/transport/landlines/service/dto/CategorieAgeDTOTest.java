package com.transport.landlines.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.transport.landlines.web.rest.TestUtil;

public class CategorieAgeDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CategorieAgeDTO.class);
        CategorieAgeDTO categorieAgeDTO1 = new CategorieAgeDTO();
        categorieAgeDTO1.setId(1L);
        CategorieAgeDTO categorieAgeDTO2 = new CategorieAgeDTO();
        assertThat(categorieAgeDTO1).isNotEqualTo(categorieAgeDTO2);
        categorieAgeDTO2.setId(categorieAgeDTO1.getId());
        assertThat(categorieAgeDTO1).isEqualTo(categorieAgeDTO2);
        categorieAgeDTO2.setId(2L);
        assertThat(categorieAgeDTO1).isNotEqualTo(categorieAgeDTO2);
        categorieAgeDTO1.setId(null);
        assertThat(categorieAgeDTO1).isNotEqualTo(categorieAgeDTO2);
    }
}
