package com.transport.landlines.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ModeleMailMapperTest {

    private ModeleMailMapper modeleMailMapper;

    @BeforeEach
    public void setUp() {
        modeleMailMapper = new ModeleMailMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(modeleMailMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(modeleMailMapper.fromId(null)).isNull();
    }
}
