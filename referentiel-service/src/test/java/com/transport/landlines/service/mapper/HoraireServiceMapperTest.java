package com.transport.landlines.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class HoraireServiceMapperTest {

    private HoraireServiceMapper horaireServiceMapper;

    @BeforeEach
    public void setUp() {
        horaireServiceMapper = new HoraireServiceMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(horaireServiceMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(horaireServiceMapper.fromId(null)).isNull();
    }
}
