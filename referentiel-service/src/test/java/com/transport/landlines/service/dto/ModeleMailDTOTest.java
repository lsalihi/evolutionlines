package com.transport.landlines.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.transport.landlines.web.rest.TestUtil;

public class ModeleMailDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ModeleMailDTO.class);
        ModeleMailDTO modeleMailDTO1 = new ModeleMailDTO();
        modeleMailDTO1.setId(1L);
        ModeleMailDTO modeleMailDTO2 = new ModeleMailDTO();
        assertThat(modeleMailDTO1).isNotEqualTo(modeleMailDTO2);
        modeleMailDTO2.setId(modeleMailDTO1.getId());
        assertThat(modeleMailDTO1).isEqualTo(modeleMailDTO2);
        modeleMailDTO2.setId(2L);
        assertThat(modeleMailDTO1).isNotEqualTo(modeleMailDTO2);
        modeleMailDTO1.setId(null);
        assertThat(modeleMailDTO1).isNotEqualTo(modeleMailDTO2);
    }
}
