package com.transport.landlines.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class PointVenteConfigMapperTest {

    private PointVenteConfigMapper pointVenteConfigMapper;

    @BeforeEach
    public void setUp() {
        pointVenteConfigMapper = new PointVenteConfigMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(pointVenteConfigMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(pointVenteConfigMapper.fromId(null)).isNull();
    }
}
