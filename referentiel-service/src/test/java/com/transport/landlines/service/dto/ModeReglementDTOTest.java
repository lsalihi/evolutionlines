package com.transport.landlines.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.transport.landlines.web.rest.TestUtil;

public class ModeReglementDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ModeReglementDTO.class);
        ModeReglementDTO modeReglementDTO1 = new ModeReglementDTO();
        modeReglementDTO1.setId(1L);
        ModeReglementDTO modeReglementDTO2 = new ModeReglementDTO();
        assertThat(modeReglementDTO1).isNotEqualTo(modeReglementDTO2);
        modeReglementDTO2.setId(modeReglementDTO1.getId());
        assertThat(modeReglementDTO1).isEqualTo(modeReglementDTO2);
        modeReglementDTO2.setId(2L);
        assertThat(modeReglementDTO1).isNotEqualTo(modeReglementDTO2);
        modeReglementDTO1.setId(null);
        assertThat(modeReglementDTO1).isNotEqualTo(modeReglementDTO2);
    }
}
