package com.transport.landlines.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.transport.landlines.web.rest.TestUtil;

public class CompteBancaireTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompteBancaire.class);
        CompteBancaire compteBancaire1 = new CompteBancaire();
        compteBancaire1.setId(1L);
        CompteBancaire compteBancaire2 = new CompteBancaire();
        compteBancaire2.setId(compteBancaire1.getId());
        assertThat(compteBancaire1).isEqualTo(compteBancaire2);
        compteBancaire2.setId(2L);
        assertThat(compteBancaire1).isNotEqualTo(compteBancaire2);
        compteBancaire1.setId(null);
        assertThat(compteBancaire1).isNotEqualTo(compteBancaire2);
    }
}
