package com.transport.landlines.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.transport.landlines.web.rest.TestUtil;

public class CategorieAgeTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CategorieAge.class);
        CategorieAge categorieAge1 = new CategorieAge();
        categorieAge1.setId(1L);
        CategorieAge categorieAge2 = new CategorieAge();
        categorieAge2.setId(categorieAge1.getId());
        assertThat(categorieAge1).isEqualTo(categorieAge2);
        categorieAge2.setId(2L);
        assertThat(categorieAge1).isNotEqualTo(categorieAge2);
        categorieAge1.setId(null);
        assertThat(categorieAge1).isNotEqualTo(categorieAge2);
    }
}
