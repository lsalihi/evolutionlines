package com.transport.landlines.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.transport.landlines.web.rest.TestUtil;

public class TypeMessageTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TypeMessage.class);
        TypeMessage typeMessage1 = new TypeMessage();
        typeMessage1.setId(1L);
        TypeMessage typeMessage2 = new TypeMessage();
        typeMessage2.setId(typeMessage1.getId());
        assertThat(typeMessage1).isEqualTo(typeMessage2);
        typeMessage2.setId(2L);
        assertThat(typeMessage1).isNotEqualTo(typeMessage2);
        typeMessage1.setId(null);
        assertThat(typeMessage1).isNotEqualTo(typeMessage2);
    }
}
