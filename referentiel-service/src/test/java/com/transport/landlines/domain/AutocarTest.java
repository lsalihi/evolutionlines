package com.transport.landlines.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.transport.landlines.web.rest.TestUtil;

public class AutocarTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Autocar.class);
        Autocar autocar1 = new Autocar();
        autocar1.setId(1L);
        Autocar autocar2 = new Autocar();
        autocar2.setId(autocar1.getId());
        assertThat(autocar1).isEqualTo(autocar2);
        autocar2.setId(2L);
        assertThat(autocar1).isNotEqualTo(autocar2);
        autocar1.setId(null);
        assertThat(autocar1).isNotEqualTo(autocar2);
    }
}
