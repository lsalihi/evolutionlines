package com.transport.landlines.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.transport.landlines.web.rest.TestUtil;

public class PointVenteConfigTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PointVenteConfig.class);
        PointVenteConfig pointVenteConfig1 = new PointVenteConfig();
        pointVenteConfig1.setId(1L);
        PointVenteConfig pointVenteConfig2 = new PointVenteConfig();
        pointVenteConfig2.setId(pointVenteConfig1.getId());
        assertThat(pointVenteConfig1).isEqualTo(pointVenteConfig2);
        pointVenteConfig2.setId(2L);
        assertThat(pointVenteConfig1).isNotEqualTo(pointVenteConfig2);
        pointVenteConfig1.setId(null);
        assertThat(pointVenteConfig1).isNotEqualTo(pointVenteConfig2);
    }
}
