package com.transport.landlines.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.transport.landlines.web.rest.TestUtil;

public class HoraireServiceTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(HoraireService.class);
        HoraireService horaireService1 = new HoraireService();
        horaireService1.setId(1L);
        HoraireService horaireService2 = new HoraireService();
        horaireService2.setId(horaireService1.getId());
        assertThat(horaireService1).isEqualTo(horaireService2);
        horaireService2.setId(2L);
        assertThat(horaireService1).isNotEqualTo(horaireService2);
        horaireService1.setId(null);
        assertThat(horaireService1).isNotEqualTo(horaireService2);
    }
}
