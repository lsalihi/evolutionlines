package com.transport.landlines.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.transport.landlines.web.rest.TestUtil;

public class KmInterVilleTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(KmInterVille.class);
        KmInterVille kmInterVille1 = new KmInterVille();
        kmInterVille1.setId(1L);
        KmInterVille kmInterVille2 = new KmInterVille();
        kmInterVille2.setId(kmInterVille1.getId());
        assertThat(kmInterVille1).isEqualTo(kmInterVille2);
        kmInterVille2.setId(2L);
        assertThat(kmInterVille1).isNotEqualTo(kmInterVille2);
        kmInterVille1.setId(null);
        assertThat(kmInterVille1).isNotEqualTo(kmInterVille2);
    }
}
