package com.transport.landlines.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.transport.landlines.web.rest.TestUtil;

public class ModeReglementTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ModeReglement.class);
        ModeReglement modeReglement1 = new ModeReglement();
        modeReglement1.setId(1L);
        ModeReglement modeReglement2 = new ModeReglement();
        modeReglement2.setId(modeReglement1.getId());
        assertThat(modeReglement1).isEqualTo(modeReglement2);
        modeReglement2.setId(2L);
        assertThat(modeReglement1).isNotEqualTo(modeReglement2);
        modeReglement1.setId(null);
        assertThat(modeReglement1).isNotEqualTo(modeReglement2);
    }
}
