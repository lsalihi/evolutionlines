package com.transport.landlines.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.transport.landlines.web.rest.TestUtil;

public class CodificationTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Codification.class);
        Codification codification1 = new Codification();
        codification1.setId(1L);
        Codification codification2 = new Codification();
        codification2.setId(codification1.getId());
        assertThat(codification1).isEqualTo(codification2);
        codification2.setId(2L);
        assertThat(codification1).isNotEqualTo(codification2);
        codification1.setId(null);
        assertThat(codification1).isNotEqualTo(codification2);
    }
}
