package com.transport.landlines.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.transport.landlines.web.rest.TestUtil;

public class ModeleMailTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ModeleMail.class);
        ModeleMail modeleMail1 = new ModeleMail();
        modeleMail1.setId(1L);
        ModeleMail modeleMail2 = new ModeleMail();
        modeleMail2.setId(modeleMail1.getId());
        assertThat(modeleMail1).isEqualTo(modeleMail2);
        modeleMail2.setId(2L);
        assertThat(modeleMail1).isNotEqualTo(modeleMail2);
        modeleMail1.setId(null);
        assertThat(modeleMail1).isNotEqualTo(modeleMail2);
    }
}
