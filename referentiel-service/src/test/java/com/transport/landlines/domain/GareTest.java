package com.transport.landlines.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.transport.landlines.web.rest.TestUtil;

public class GareTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Gare.class);
        Gare gare1 = new Gare();
        gare1.setId(1L);
        Gare gare2 = new Gare();
        gare2.setId(gare1.getId());
        assertThat(gare1).isEqualTo(gare2);
        gare2.setId(2L);
        assertThat(gare1).isNotEqualTo(gare2);
        gare1.setId(null);
        assertThat(gare1).isNotEqualTo(gare2);
    }
}
