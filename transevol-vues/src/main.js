import { createApp } from 'vue'
import router from '@/router/router'
import App from './App.vue'

createApp(App).use(router).mount('#app')

/*

import Vue from 'vue';
import App from './App.vue'
import router from './router';
import store from './store';
import './registerServiceWorker';

import BootstrapVue from 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(BootstrapVue)

import DateFilter from './common/date.filter';
import ErrorFilter from './common/error.filter';

Vue.config.productionTip = false;
Vue.filter('date', DateFilter);
Vue.filter('error', ErrorFilter);


new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app');
*/